import React, {Component} from 'react';
import {Keyboard, Text, View} from 'react-native';
import {NavigationActions} from 'react-navigation';
import moment from 'moment';

// import {province, city} from '../constants/Area';
import FreshStyles from '../constants/FreshStyles';

import Button from '../components/Button';
import DialogDropdown from '../components/DialogDropdown';
import FormInput from '../components/FormInput';
import FreshScreen from '../components/FreshScreen';
import RadioButton from '../components/RadioButton';

import ApiConnector from '../helper/ApiConnector';

export default class RegisterScreen extends Component {
	constructor(props) {
		super(props);
		let yearsList = [];
		let thisYear = parseInt(moment().format('YYYY'))-16;
		for (let i = thisYear; i >= 1920; i--) {
			yearsList.push({key: i, value: i});
		}
		let monthList = [];
		for (let i = 0; i < 12; i++) {
			monthList.push({
				key: i,
				value: moment()
					.month(i)
					.format('MMMM'),
			});
		}
		this.state = {
			yearsList: yearsList,
			monthList: monthList,
			selectedYear: thisYear,
			selectedMonth: 0,
			selectedDay: 1,
			email: null,
			mobile: null,
			password: null,
			password2: null,
			nickname: null,
			gender: null,
			// province: null,
			// city: null,
			// district: null,
			error: {},
			isSubmitting: false,
		};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	completeRegisterValOTP = async (theData) => {
		this.props.navigation.reset(
			[NavigationActions.navigate({routeName: 'Home'})],
			0,
		);
	}
	submitRegister = async () => {
		let email = this.state.email;
		let mobile = this.state.mobile;
		let password = this.state.password;
		let password2 = this.state.password2;
		let nickname = this.state.nickname;
		let gender = this.state.gender;
		// let province = this.state.province;
		// let city = this.state.city;
		// let district = this.state.district;
		let birthday = moment(
				this.state.selectedYear +
					'-' +
					(this.state.selectedMonth + 1) +
					'-' +
					this.state.selectedDay,
				'YYYY-M-D',
			).format('X');
		if(this.mounted) {
			this.setState({
				isSubmitting: true,
			});
		}
		let registerObj = {
			scene: 1,
			email: email,
			mobile: mobile,
			password: password,
			password2: password2,
			nickname: nickname,
			sex: gender,
			// province: province,
			// city: city,
			// district: district,
			birthday: birthday,
		};
		let registerApi = new ApiConnector({
			apiLink: '/fresh/Login/register',
			body: JSON.stringify(registerObj),
			navigation: this.props.navigation,
			onSuccess: async jsonObj => {
				if (jsonObj.resultCode !== 0) {
					if (this.mounted) {
						this.setState({error: jsonObj.data});
					}
				} else {
					let valData = {user_id: jsonObj.data.user_id, scene: 5};
					this.props.navigation.push('ValidateOTP', {
						valParam: JSON.stringify(valData),
						email: jsonObj.data.email,
						mobile: jsonObj.data.mobile,
						onComplete: this.completeRegisterValOTP,
					});
				}
				if (this.mounted) {
					this.setState({isSubmitting: false});
				}
			},
		});
		Keyboard.dismiss();
		await registerApi.submit();
	};
	changeEmail = email => {
		if (this.mounted) {
			this.setState({email: email});
		}
		if (email !== undefined && email !== '') {
			let oldError = this.state.error;
			delete oldError.email;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};
	changeMobile = mobile => {
		if (this.mounted) {
			this.setState({mobile: mobile});
		}
		if (mobile !== undefined && mobile !== '') {
			let oldError = this.state.error;
			delete oldError.mobile;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};
	changePassword = password => {
		if (this.mounted) {
			this.setState({password: password});
		}
		if (password !== undefined && password !== '') {
			let oldError = this.state.error;
			delete oldError.password;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};
	changePassword2 = password2 => {
		if (this.mounted) {
			this.setState({password2: password2});
		}
		if (password2 !== undefined && password2 !== '') {
			let oldError = this.state.error;
			delete oldError.password2;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};
	changeNickname = nickname => {
		if (this.mounted) {
			this.setState({nickname: nickname});
		}
		if (nickname !== undefined && nickname !== '') {
			let oldError = this.state.error;
			delete oldError.nickname;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};
	changeGender = gender => {
		if(this.mounted) {
			this.setState({
				gender: gender,
			});
		}
	};
	changeYear = year => {
		let {selectedDay} = this.state;
		let test = moment(
			year.key + '-' + (this.state.selectedMonth + 1) + '-' + selectedDay,
			'YYYY-M-D',
			true,
		);
		if (!test.isValid()) {
			selectedDay = 1;
		}
		let oldError = this.state.error;
		delete oldError.birthday;
		if(this.mounted) {
			this.setState({
				error: oldError,
				selectedYear: year.key,
				selectedDay: selectedDay,
			});
		}
	};
	changeMonth = month => {
		let {selectedDay} = this.state;
		let test = moment(
			this.state.selectedYear + '-' + (month.key + 1) + '-' + selectedDay,
			'YYYY-M-D',
			true,
		);
		if (!test.isValid()) {
			selectedDay = 1;
		}
		let oldError = this.state.error;
		delete oldError.birthday;
		if(this.mounted) {
			this.setState({
				error: oldError,
				selectedYear: month.key,
				selectedDay: selectedDay,
			});
		}
	};
	changeDay = day => {
		let oldError = this.state.error;
		delete oldError.birthday;
		if(this.mounted) {
			this.setState({
				error: oldError,
				selectedDay: day.key,
			});
		}
	};
	// changeProvince = province => {
	// 	let oldError = this.state.error;
	// 	delete oldError.province;
	// 	if(this.mounted) {
	// 		this.setState({
	// 			error: oldError,
	// 			selectedDay: province.key,
	// 		});
	// 	}
	// };
	// changeCity = city => {
	// 	let oldError = this.state.error;
	// 	delete oldError.city;
	// 	if(this.mounted) {
	// 		this.setState({
	// 			error: oldError,
	// 			selectedDay: city.key,
	// 		});
	// 	}
	// };
	// changeDistrict = district => {
	// 	let oldError = this.state.error;
	// 	delete oldError.district;
	// 	if(this.mounted) {
	// 		this.setState({
	// 			error: oldError,
	// 			selectedDay: district.key,
	// 		});
	// 	}
	// };
	render() {
		let days = [];
		for (let i = 1; i <= 31; i++) {
			if (i <= 28) {
				days.push({key: i, value: i});
			} else {
				let test = moment(
					this.state.selectedYear +
						'-' +
						(this.state.selectedMonth + 1) +
						'-' +
						i,
					'YYYY-M-D',
					true,
				);
				if (test.isValid()) {
					days.push({key: i, value: i});
				}
			}
		}
		return (
			<FreshScreen navigation={this.props.navigation}>
				<View style={[
					FreshStyles.flex1,
					FreshStyles.marginTop2,
					FreshStyles.selfCenter,
					FreshStyles.screenTab,
					FreshStyles.marginVer7,
				]}>
					<FormInput
						placeholder="Email Address"
						onChangeText={this.changeEmail}
						error={this.state.error.email}
						value={this.state.email}
						keyboardType="email-address"
					/>
					<FormInput
						placeholder="Mobile Number"
						onChangeText={this.changeMobile}
						error={this.state.error.mobile}
						keyboardType="numeric"
						value={this.state.mobile}
					/>
					<FormInput
						placeholder="Password"
						onChangeText={this.changePassword}
						error={this.state.error.password}
						secureTextEntry={true}
						value={this.state.password}
					/>
					<FormInput
						placeholder="Confirm Password"
						onChangeText={this.changePassword2}
						error={this.state.error.password2}
						secureTextEntry={true}
						value={this.state.password2}
					/>
					<FormInput
						placeholder="Nickname"
						onChangeText={this.changeNickname}
						error={this.state.error.nickname}
						value={this.state.nickname}
					/>
					<Text
						style={[
							FreshStyles.text12,
							FreshStyles.padTop2,
							FreshStyles.fgColorFormTitle,
						]}>Gender</Text>
					<View style={[FreshStyles.flexRow, FreshStyles.contentSpaceBetween]}>
						<RadioButton
							text="Male"
							onChange={this.changeGender}
							value={this.state.gender}
							myValue="1"
						/>
						<RadioButton
							text="Female"
							onChange={this.changeGender}
							value={this.state.gender}
							myValue="2"
						/>
					</View>
					{
						this.state.error.sex != undefined &&
						this.state.error.sex != null &&
						this.state.error.sex != '' &&
						<Text style={[FreshStyles.errorText]}>{this.state.error.sex}</Text>
					}
					<View style={[FreshStyles.flexRow, FreshStyles.padVer2]}>
						<DialogDropdown
							title="Years"
							data={this.state.yearsList}
							selectedValue={this.state.selectedYear}
							change={this.changeYear}
							containerStyle={[FreshStyles.margin0, FreshStyles.flex2]}
						/>
						<DialogDropdown
							title="Months"
							data={this.state.monthList}
							selectedValue={this.state.selectedMonth}
							change={this.changeMonth}
							containerStyle={[
								FreshStyles.margin0,
								FreshStyles.flex3,
								FreshStyles.padHor2,
							]}
						/>
						<DialogDropdown
							title="Days"
							data={days}
							selectedValue={this.state.selectedDay}
							change={this.changeDay}
							containerStyle={[FreshStyles.margin0, FreshStyles.flex1]}
						/>
					</View>
					{
						this.state.error.birthday != undefined &&
						this.state.error.birthday != null &&
						this.state.error.birthday != '' &&
						<Text style={[FreshStyles.errorText]}>{this.state.error.birthday}</Text>
					}
					{/*
					<DialogDropdown
						title="Province"
						data={province}
						selectedValue={this.state.province}
						change={this.changeProvince}
						containerStyle={[FreshStyles.margin0, FreshStyles.padVer2]}
					/>
					<DialogDropdown
						title="City"
						data={city}
						selectedValue={this.state.city}
						change={this.changeCity}
						containerStyle={[FreshStyles.margin0, FreshStyles.padVer2]}
					/>
					<DialogDropdown
						title="District"
						data={district}
						selectedValue={this.state.district}
						change={this.changeDistrict}
						containerStyle={[FreshStyles.margin0, FreshStyles.padVer2]}
					/>
					*/}
					<Button
						disabled={this.state.isSubmitting ? true : false}
						title={this.state.isSubmitting ? 'Submitting' : 'Register'}
						onPress={() => this.submitRegister()}
					/>
				</View>
			</FreshScreen>
		);
	}
}
