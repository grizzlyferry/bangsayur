import React, {Component} from 'react';
import {View, Text, Keyboard, StyleSheet} from 'react-native';

import FreshStyles from '../constants/FreshStyles';
import {province, city} from '../constants/Area';

import Buttons from '../components/Buttons';
import DialogDropdown from '../components/DialogDropdown';
import FormInput from '../components/FormInput';
import FreshScreen from '../components/FreshScreen';
import RadioButton from '../components/RadioButton';

import ApiConnector from '../helper/ApiConnector';

export default class NewAddressScreen extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		this.state = {
			// province: null,
			// city: null,
			// district: null,
			consignee: null,
			mobile: null,
			address: null,
			zipcode: null,
			isSubmitting: false,
			is_default: null,
			error: {},
		};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	changeDefault = is_default => {
		this.setState({
			is_default: is_default,
		});
	};
	changeProvince = province => {
		this.setState({
			province: province,
		});
	};
	changeCity = city => {
		this.setState({
			city: city,
		});
	};
	changeDistrict = district => {
		this.setState({
			district: district,
		});
	};
	changeConsignee = consignee => {
		if (this.mounted) {
			this.setState({consignee: consignee});
		}
		if (consignee !== undefined && consignee !== '') {
			let oldError = this.state.error;
			delete oldError.consignee;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};
	changeMobile = mobile => {
		if (this.mounted) {
			this.setState({mobile: mobile});
		}
		if (mobile !== undefined && mobile !== '') {
			let oldError = this.state.error;
			delete oldError.mobile;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};
	changeAddress = address => {
		if (this.mounted) {
			this.setState({address: address});
		}
		if (address !== undefined && address !== '') {
			let oldError = this.state.error;
			delete oldError.address;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};
	changeZip = zipcode => {
		if (this.mounted) {
			this.setState({zipcode: zipcode});
		}
		if (zipcode !== undefined && zipcode !== '') {
			let oldError = this.state.error;
			delete oldError.zipcode;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};
	changePhone = phone => {
		if (this.mounted) {
			this.setState({phone: phone});
		}
		if (phone !== undefined && phone !== '') {
			let oldError = this.state.error;
			delete oldError.phone;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};

	submitAddress = async () => {
		let consignee = this.state.consignee;
		let mobile = this.state.mobile;
		let address = this.state.address;
		let zipcode = this.state.zipcode;
		let is_default = this.state.is_default;
		this.setState({
			isSubmitting: true,
		});
		let addressObj = {
			scene: 8,
			consignee: consignee,
			mobile: mobile,
			zipcode: zipcode,
			address: address,
			is_default: is_default,
		};
		let AddressBody = JSON.stringify(addressObj);
		let AddressApi = new ApiConnector({
			apiLink: '/fresh/UserAddress/addAddress',
			body: AddressBody,
			navigation: this.props.navigation,
			onSuccess: async jsonObj => {
				if (jsonObj.resultCode !== 0) {
					if (this.mounted) {
						this.setState({error: jsonObj.data});
					}
				} else if (jsonObj.resultCode === 0) {
					let newAddress = jsonObj.data;
					this.props.navigation.navigate('Checkout', {
						newAddress: newAddress,
					});
				}
				if (this.mounted) {
					this.setState({isSubmitting: false});
				}
			},
			onError: error => {
				if (this.mounted) {
					this.setState({isSubmitting: false});
				}
			},
		});
		Keyboard.dismiss();
		await AddressApi.submit();
	};

	render() {
		let district = [
			{key: 'Abenaho, Yalimo', value: 'Abenaho'},
			{key: 'Abepura, Jayapura', value: 'Abepura'},
		];
		return (
			<FreshScreen navigation={this.props.navigation}>
				<View>
					<View style={[FreshStyles.address]}>
						<Text style={[styles.title]}>Recipent's Identity</Text>
						<FormInput
							style={[styles.text]}
							placeholder="Nama Penerima"
							onChangeText={this.changeConsignee}
							value={this.state.consignee}
							error={this.state.error.consignee}
						/>
						<FormInput
							style={[styles.text]}
							placeholder="Mobile Phone"
							onChangeText={this.changeMobile}
							value={this.state.mobile}
							error={this.state.error.mobile}
							keyboardType="numeric"
						/>
					</View>
					{/* <View style={[FreshStyles.address]}>
						<Text style={[styles.title]}>Recipent's Address</Text>
						<DialogDropdown
							title="Province"
							data={province}
							selectedValue={this.state.province}
							change={this.changeProvince}
							containerStyle={[FreshStyles.margin0, FreshStyles.padVer2]}
						/>
						<DialogDropdown
							title="City"
							data={city}
							selectedValue={this.state.city}
							change={this.changeCity}
							containerStyle={[FreshStyles.margin0, FreshStyles.padVer2]}
						/>
						<DialogDropdown
							title="District"
							data={district}
							selectedValue={this.state.district}
							change={this.changeDistrict}
							containerStyle={[FreshStyles.margin0, FreshStyles.padVer2]}
						/>
					</View> */}
					<View style={[FreshStyles.address]}>
						<Text style={[styles.title]}>Address</Text>
						{/* <View style={[FreshStyles.flexRow]}>
							<RadioButton
								text="Home"
								value={this.state.gender}
								onChange={this.changeDefault}
								myValue="1"
							/>
							<RadioButton
								text="Pick-up Point"
								value={this.state.gender}
								onChange={this.onChange}
								myValue="2"
							/>
						</View> */}
						<FormInput
							style={[styles.text]}
							placeholder="Full Address"
							onChangeText={this.changeAddress}
							value={this.state.address}
							error={this.state.error.address}
						/>
						<FormInput
							style={[styles.text]}
							placeholder="Zipcode"
							onChangeText={this.changeZip}
							value={this.state.zipcode}
							error={this.state.error.zipcode}
							keyboardType="numeric"
						/>
					</View>
					<View
						style={[
							FreshStyles.address,
							FreshStyles.flexRow,
							FreshStyles.alignCenter,
						]}>
						<Text>Use as Primary? </Text>
						<RadioButton
							text="Yes"
							value={this.state.is_default}
							onChange={this.onChange}
							myValue="1"
						/>
						<RadioButton
							text="No"
							value={this.state.is_default}
							onChange={this.onChange}
							myValue="0"
						/>
					</View>
				</View>
				<Buttons
					viewStyle={[styles.btn]}
					style={[FreshStyles.width90]}
					textStyle={[FreshStyles.greenButton, {color: 'white'}]}
					disabled={this.state.isSubmitting ? true : false}
					title={this.state.isSubmitting ? 'Submitting' : 'Submit'}
					onPress={() => this.submitAddress()}
				/>
			</FreshScreen>
		);
	}
}

const styles = StyleSheet.create({
	text: {
		fontSize: 12,
	},
	title: {
		fontSize: 13,
	},
	btn: {
		alignItems: 'center',
		margin: 15,
	},
});
