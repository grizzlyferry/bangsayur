import React, {Component} from 'react';
import {Keyboard, Text, TouchableOpacity, View} from 'react-native';
import {NavigationActions} from 'react-navigation';

import FreshStyles from '../constants/FreshStyles';

import Button from '../components/Button';
import FormInput from '../components/FormInput';
import FreshScreen from '../components/FreshScreen';

import ApiConnector from '../helper/ApiConnector';
import Session from '../helper/Session';
import StringHelper from '../helper/StringHelper';

export default class LoginScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			account: null,
			password: null,
			error: {},
		};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	completeRegisterValOTP = async (theData) => {
		this.props.navigation.reset(
			[NavigationActions.navigate({routeName: 'Home'})],
			0,
		);
	}
	submitLogin = async () => {
		let {account, password} = this.state;
		if (this.mounted) {
			this.setState({isSubmitting: true});
		}
		let loginObj = {
			scene: 3,
			account,
			password,
		};
		let loginBody = JSON.stringify(loginObj);
		let loginApi = new ApiConnector({
			apiLink: '/fresh/Login/login',
			body: loginBody,
			navigation: this.props.navigation,
			onSuccess: async jsonObj => {
				if (jsonObj.resultCode !== 0) {
					if (this.mounted) {
						this.setState({error: jsonObj.data});
					}
					if (jsonObj.resultCode === -2) {
						if(jsonObj.data.lock_code === 1) {
							let valData = {
								user_id: jsonObj.data.user_id,
								scene: 5,
							};
							let accountType = StringHelper.getTextType(this.state.account);
							this.props.navigation.push('ValidateOTP', {
								valParam: JSON.stringify(valData),
								onComplete: this.completeValOTP,
								mobile: accountType===1?this.state.account:null,
								email: accountType===2?this.state.account:null,
							});
						} else if(jsonObj.data.lock_code === 2) {
							this.props.navigation.push('Error', {
								banned: true,
							});
						}
					}
				} else {
					if(jsonObj.data !== undefined && jsonObj.data.token !== undefined) {
						await Session.setValue('fresh_token', jsonObj.data.token);
					}
					this.completeRegisterValOTP(null);
				}
				if (this.mounted) {
					this.setState({isSubmitting: false});
				}
			},
		});
		Keyboard.dismiss();
		await loginApi.submit();
	};
	changeAccount = account => {
		if (this.mounted) {
			this.setState({account: account});
		}
		if (account !== undefined && account !== '') {
			let oldError = this.state.error;
			delete oldError.account;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};
	changePassword = password => {
		if (this.mounted) {
			this.setState({password: password});
		}
		if (password !== undefined && password !== '') {
			let oldError = this.state.error;
			delete oldError.password;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};
	render() {
		return (
			<FreshScreen navigation={this.props.navigation}>
				<View style={[
					FreshStyles.flex1,
					FreshStyles.marginTop2,
					FreshStyles.selfCenter,
					FreshStyles.screenTab,
					FreshStyles.marginVer7,
				]}>
					<FormInput
						placeholder="Username"
						onChangeText={this.changeAccount}
						value={this.state.account}
						error={this.state.error.account}
					/>
					<FormInput
						placeholder="Password"
						secureTextEntry={true}
						onChangeText={this.changePassword}
						value={this.state.password}
						error={this.state.error.password}
					/>
					<Button
						disabled={this.state.isSubmitting ? true : false}
						title={this.state.isSubmitting ? 'Logging in...' : 'Log in'}
						onPress={this.submitLogin}
					/>
					<View
						style={[FreshStyles.alignCenter]}>
						<TouchableOpacity
							style={FreshStyles.padVer2}
							onPress={() => this.props.navigation.push('Register')}>
							<Text>Register New Account</Text>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => this.props.navigation.push('Forget')}>
							<Text>Forget Password?</Text>
						</TouchableOpacity>
					</View>
				</View>
			</FreshScreen>
		);
	}
}
