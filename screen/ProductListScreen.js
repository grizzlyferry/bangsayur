import React, {Component} from 'react';
import {View} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import FreshScreen from '../components/FreshScreen';
import ProductList from '../components/ProductList';

import ApiConnector from '../helper/ApiConnector';

export default class ProductListScreen extends Component {
	constructor(props) {
		super(props);
		// let bodyParam = this.props.navigation.getParam('bodyParam', null); -> Ferry : Sementara diganti null karena bikin error
		let bodyParam = null;
		this.state = {
			bodyParam: bodyParam,
		};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	render() {
		return (
			<FreshScreen
				navigation={this.props.navigation}
				productData={this.state.productData}
				useScroll={false}
				isProductLoaded={this.state.isProductLoaded}>
				<ProductList
					type="two-column"
					apiLink="/fresh/Product/productList"
					bodyParam={JSON.stringify({sort: 'new'})}
					navigation={this.props.navigation}
					title2="Similar Products"
					headerLayout={() => {
						return null;
					}}
				/>
			</FreshScreen>
		);
		// return (
		//   <FreshScreen navigation={this.props.navigation}>
		//     <View style={FreshStyles.padTop2}>
		//       <ProductList
		//         type="two-column"
		//         apiLink="/fresh/Product/productList"
		//         bodyParam={JSON.stringify({sort: 'new'})}
		//         navigation={this.props.navigation}
		//       />
		//     </View>
		//   </FreshScreen>
		// );
	}
}
