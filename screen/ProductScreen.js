import React, {Component} from 'react';
import {View} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import FreshScreen from '../components/FreshScreen';
import ProductInfo from '../components/ProductInfo';
import ProductList from '../components/ProductList';

import ApiConnector from '../helper/ApiConnector';

export default class ProductScreen extends Component {
	constructor(props) {
		super(props);
		// let product_id = this.props.navigation.getParam('product_id', null); -> Ferry : Sementara ini diganti sama null
		let product_id = null;
		this.state = {
			product_id: product_id,
			isProductLoaded: false,
			productData: null,
		};
		this.loadProduct();
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	loadProduct = async () => {
		if (this.state.isProductLoaded) {
			return;
		}
		let product_id = this.state.product_id;
		let productBody = {
			goods_id: product_id,
		};
		let productBodyJson = JSON.stringify(productBody);
		let productApi = new ApiConnector({
			apiLink: '/fresh/Product/getProduct',
			body: productBodyJson,
			navigation: this.props.navigation,
			onSuccess: async jsonObj => {
				if (jsonObj.resultCode === 0) {
					if (this.mounted) {
						this.setState({
							isProductLoaded: true,
							productData: jsonObj.data,
						});
					}
				} else {
					alert('Something Bad Happened!\nPlease Call Our Developer!');
					console.log('Error Details: ', jsonObj);
				}
			},
			onError: error => {
				console.log(error);
				alert('Failed to Retrieve Data!\nPlease Call Our Developer!');
			},
		});
		await productApi.submit();
	};
	render() {
		return (
			<FreshScreen
				navigation={this.props.navigation}
				productData={this.state.productData}
				useScroll={false}
				isProductLoaded={this.state.isProductLoaded}>
				<ProductList
					type="two-column"
					apiLink="/fresh/Product/productList"
					bodyParam={JSON.stringify({sort: 'new'})}
					navigation={this.props.navigation}
					title2="Similar Products"
					headerLayout={() => {
						return (
							<View>
								{this.state.isProductLoaded && (
									<ProductInfo data={this.state.productData} />
								)}
								<ProductList
									type="horizontal"
									apiLink="/fresh/Product/productList"
									bodyParam={JSON.stringify({sort: 'new'})}
									navigation={this.props.navigation}
									title="Similar Products"
								/>
							</View>
						);
					}}
				/>
			</FreshScreen>
		);
		// return (
		//   <FreshScreen
		//     navigation={this.props.navigation}
		//     productData={this.state.productData}
		//     isProductLoaded={this.state.isProductLoaded}>
		//     {this.state.isProductLoaded && (
		//       <ProductInfo data={this.state.productData} />
		//     )}
		//     <View style={FreshStyles.padTop2}>
		//       <ProductList
		//         type="horizontal"
		//         apiLink="/fresh/Product/productList"
		//         bodyParam={JSON.stringify({sort: 'new'})}
		//         navigation={this.props.navigation}
		//         title="Similar Products"
		//       />
		//       <ProductList
		//         type="two-column"
		//         apiLink="/fresh/Product/productList"
		//         bodyParam={JSON.stringify({sort: 'new'})}
		//         navigation={this.props.navigation}
		//         title2="Similar Products"
		//       />
		//     </View>
		//   </FreshScreen>
		// );
	}
}
