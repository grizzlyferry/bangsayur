import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Keyboard} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import Button from '../components/Button';
import FormControl from '../components/FormControl';
import FreshScreen from '../components/FreshScreen';

import ApiConnector from '../helper/ApiConnector';
import Session from '../helper/Session';

export default class ValidateOTPScreen extends Component {
	constructor(props) {
		super(props);
		// let valParam = this.props.navigation.getParam('valParam', null); 
		// let email = this.props.navigation.getParam('email', null);
		// let mobile = this.props.navigation.getParam('mobile', null);
		// let onComplete = this.props.navigation.getParam('onComplete', null); -> Ferry : Ini sementara yang bikin error, sementara diganti sama null
		let valParam = null;
		let email = null;
		let mobile = null;
		let onComplete = null;
		if (onComplete !== null) {
			this.onComplete = onComplete;
		}
		this.state = {
			valParam: valParam,
			email: email,
			mobile: mobile,
			user_code: null,
			isSubmitting: false,
			error: {},
		};
	}
	onComplete = async jsonObj => {
		this.goToErrorCritical({devErrorCode: '-07'});
	};
	goToErrorCritical = (params = null) => {
		this.props.navigation.reset(
			[NavigationActions.navigate({routeName: 'Error', params: params})],
			0,
		);
	};
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	changeUserCode = user_code => {
		if (this.mounted) {
			this.setState({user_code: user_code});
		}
		if (user_code !== undefined && user_code !== '') {
			let oldError = this.state.error;
			delete oldError.user_code;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};
	validateOTP = async () => {
		let user_code = this.state.user_code;
		this.setState({
			isSubmitting: true,
		});
		let validateBody = JSON.stringify({
			user_code: user_code,
			...JSON.parse(this.state.valParam),
		});
		let validateApi = new ApiConnector({
			apiLink: '/fresh/Login/validateOTP',
			body: validateBody,
			navigation: this.props.navigation,
			onSuccess: async jsonObj => {
				if (jsonObj.resultCode !== 0) {
					if (this.mounted) {
						this.setState({error: jsonObj.data});
					}
				} else {
					if(jsonObj.data !== undefined && jsonObj.data.token !== undefined) {
						await Session.setValue('fresh_token', jsonObj.data.token);
					}
					let theData = JSON.stringify({
						user_code: user_code,
						...JSON.parse(this.state.valParam),
					});
					this.onComplete(theData);
				}
				if (this.mounted) {
					this.setState({isSubmitting: false});
				}
			},
		});
		Keyboard.dismiss();
		await validateApi.submit();
	};
	render() {
		return (
			<FreshScreen navigation={this.props.navigation}>
				<View style={[FreshStyles.Background, FreshStyles.margin20]}>
					<Text
						style={[
							FreshStyles.textBold,
							FreshStyles.textCenter,
							FreshStyles.marginBot3,
						]}>
						ENTER CONFIRMATION CODE
					</Text>
					<Text style={FreshStyles.textCenter}>
						Enter the 6-digit code we sent to
					</Text>
					<View style={[FreshStyles.flexColumn, FreshStyles.alignCenter]}>
						{this.state.email != null && <Text>{this.state.email}</Text>}
						{this.state.email != null && this.state.mobile != null && (
							<Text> or </Text>
						)}
						{this.state.mobile != null && <Text>{this.state.mobile}</Text>}
					</View>
					<TouchableOpacity style={FreshStyles.marginBot4}>
						<Text
							style={[
								FreshStyles.textBold,
								FreshStyles.fgColor4,
								FreshStyles.textCenter,
							]}>
							Request a new one
						</Text>
					</TouchableOpacity>
					<FormControl
						style={FreshStyles.textCenter}
						onChangeText={this.changeUserCode}
						placeholder="Confirmation Code"
						maxLength={6}
						keyboardType="numeric"
						error={this.state.error.user_code}
					/>
					<Button
						disabled={this.state.isSubmitting ? true : false}
						title={this.state.isSubmitting ? 'Verifying..' : 'Next'}
						onPress={() => this.validateOTP()}
					/>
				</View>
			</FreshScreen>
		);
	}
}
