import React, {Component} from 'react';

import FreshStyles from '../constants/FreshStyles';

import Banner from '../components/Banner';
import CategoryList from '../components/CategoryList';
import FreshScreen from '../components/FreshScreen';
import LoadingBox from '../components/LoadingBox';
import ProductList from '../components/ProductList';

import ApiConnector from '../helper/ApiConnector';

import {View} from 'react-native';
export default class CategoryScreen extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		// let category_id = this.props.navigation.getParam('category_id', null); -> Ferry : ini yang bikin error sementara jadi null 
		let category_id = null;
		this.state = {
			category_id: category_id,
		};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}

	// loadCategory = async (offset = 0) => {
	//   if (this.state.isLoaded) {
	//     return;
	//   }
	//   if (this.mounted) {
	//     this.setState({
	//       isLoaded: true,
	//     });
	//   }
	//   let categoryListBody = {
	//     category_id: this.state.category_id,
	//     level: 2,
	//     limit: 9,
	//   };
	//   let categoryListBodyJson = JSON.stringify(categoryListBody);
	//   let categoryListApi = new ApiConnector({
	//     apiLink: '/fresh/Category/categoryList',
	//     body: categoryListBodyJson,
	//     navigation: this.props.navigation,
	//     onSuccess: async jsonObj => {
	//       if (jsonObj.resultCode === 0) {
	//         let noMore = false;
	//         let categoryList = this.state.categoryList.concat(jsonObj.data);
	//         if (categoryList.length >= jsonObj.count) {
	//           noMore = true;
	//         }
	//         if (this.mounted) {
	//           this.setState({
	//             isLoaded: true,
	//             categoryList: categoryList,
	//             noMore: noMore,
	//             totalCount: jsonObj.count,
	//           });
	//         }
	//       } else {
	//         alert('Something Bad Happened!\nPlease Call Our Developer!');
	//         console.log('Error Details: ', jsonObj);
	//         if (this.mounted) {
	//           this.setState({
	//             isLoaded: false,
	//           });
	//         }
	//       }
	//     },
	//     onError: error => {
	//       console.log(error);
	//       alert('Failed to Retrieve Data!\nPlease Call Our Developer!');
	//     },
	//   });
	//   await categoryListApi.submit();
	// };
	// renderCatList = () => {
	//   let needShowAll = this.state.categoryList.length === 9;
	//   let i = 0;
	//   return (
	//     <View style={[FreshStyles.CatWrapper]}>
	//       {this.state.categoryList.map((item, key) => {
	//         i++;
	//         if (i === 8 && needShowAll) {
	//           return (
	//             <CategoryBox2
	//               key={'cat_' + key}
	//               navigation={this.props.navigation}
	//               category_id={this.state.category_id}
	//             />
	//           );
	//         } else if (i !== 9) {
	//           return (
	//             <CategoryBox2
	//               key={'cat_' + key}
	//               navigation={this.props.navigation}
	//               data={item}
	//             />
	//           );
	//         }
	//       })}
	//     </View>
	//   );
	// };
	render() {
		return (
			<FreshScreen navigation={this.props.navigation}>
				<Banner />
				<CategoryList
					apiLink={'/fresh/Category/categoryList'}
					category_id={this.state.category_id}
					level={2}
					navigation={this.props.navigation}
				/>
				<ProductList
					type="horizontal"
					apiLink="/fresh/Product/productList"
					bodyParam={JSON.stringify({sort: 'new'})}
					navigation={this.props.navigation}
					title="Hot Products"
				/>
				<ProductList
					type="two-column"
					apiLink="/fresh/Product/productList"
					bodyParam={JSON.stringify({sort: 'new'})}
					navigation={this.props.navigation}
					title2="Recommended for you"
				/>
			</FreshScreen>
		);
	}
}
