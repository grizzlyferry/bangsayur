import React, {Component} from 'react';
import {
	View,
	Text,
	StyleSheet,
	Image,
	TouchableOpacity,
	ActivityIndicator,
} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import Buttons from '../components/Buttons';
import FreshScreen from '../components/FreshScreen';

import ApiConnector from '../helper/ApiConnector';
import Session from '../helper/Session';

export default class ProfileScreen extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		this.state = {
			token: false,
			isChecking: false,
		};
	}
	componentDidMount() {
		this.mounted = true;
		this.checkToken();
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	checkToken = async () => {
		if (this.mounted) {
			this.setState({
				isChecking: true,
			});
		}
		let tokenSession = await Session.getValue('fresh_token');
		if (this.mounted) {
			this.setState({
				token: tokenSession,
				isChecking: false,
			});
		}
		if (this.state.token === null) {
			this.props.navigation.replace('Login');
		}
	};
	removeToken = async () => {
		await Session.removeValue('fresh_token');
		this.props.navigation.replace('Login');
	};
	render() {
		return (
			<FreshScreen navigation={this.props.navigation}>
				{this.state.isChecking && (
					<View style={[FreshStyles.screen, {justifyContent: 'center'}]}>
						<ActivityIndicator size="large" color="#0000ff" />
					</View>
				)}
				{!this.state.isChecking && (
					<View>
						<View style={[FreshStyles.address, FreshStyles.flexRow]}>
							<Image
								style={[
									FreshStyles.width15Percent,
									FreshStyles.height15Percent,
									FreshStyles.cornerRad20,
								]}
								source={{
									uri:
										'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Google_Chrome_icon_%28September_2014%29.svg/1200px-Google_Chrome_icon_%28September_2014%29.svg.png',
								}}
							/>
							<View style={[styles.col]}>
								<Text style={[FreshStyles.titleText]}>John Doe</Text>
								<Text style={[styles.text]}>Jodfsdsrewwrwesdfsdfs</Text>
								<Buttons
									title={'Edit'}
									viewStyle={[FreshStyles.width15Percent, {marginTop: 10}]}
									textStyle={styles.btn}
								/>
							</View>
						</View>

						<View style={[FreshStyles.address]}>
							<Text style={[FreshStyles.titleText]}>My order</Text>
							<View style={[styles.col]}>
								<TouchableOpacity>
									<Text style={[styles.text]}>Not yet paid</Text>
								</TouchableOpacity>
								<View style={[styles.ul]} />
								<TouchableOpacity>
									<Text style={[styles.text]}>Packed</Text>
								</TouchableOpacity>
								<View style={[styles.ul]} />
								<TouchableOpacity>
									<Text style={[styles.text]}>Shipped</Text>
								</TouchableOpacity>
								<View style={[styles.ul]} />
								<TouchableOpacity>
									<Text style={[styles.text]}>Done</Text>
								</TouchableOpacity>
								<View style={[styles.ul]} />
							</View>
						</View>

						<View style={[FreshStyles.address, FreshStyles.flexColumn]}>
							<TouchableOpacity>
								<Text style={[styles.title]}>Change Password</Text>
							</TouchableOpacity>
							<TouchableOpacity>
								<Text style={[styles.title2]}>My Favorite</Text>
							</TouchableOpacity>
						</View>

						<View style={[FreshStyles.address, FreshStyles.flexColumn]}>
							<TouchableOpacity>
								<Text style={[styles.title]}>Address</Text>
							</TouchableOpacity>
							<TouchableOpacity onPress={() => this.removeToken()}>
								<Text style={[styles.title2]}>Logout</Text>
							</TouchableOpacity>
						</View>
					</View>
				)}
			</FreshScreen>
		);
	}
}

const styles = StyleSheet.create({
	text: {
		fontSize: 12,
		paddingVertical: 8,
	},
	title: {
		fontSize: 13,
		fontWeight: 'bold',
		paddingBottom: 5,
	},
	title2: {
		fontSize: 13,
		fontWeight: 'bold',
	},
	ul: {
		borderBottomColor: 'rgba(0,0,0,0.3)',
		borderBottomWidth: 1,
	},
	btn: {
		borderRadius: 20,
		borderWidth: 1,
		borderColor: 'rgba(0,0,0,0.3)',
		backgroundColor: '#fff',
		color: 'rgba(0,0,0,0.3)',
		fontSize: 12,
		padding: 3,
	},
	col: {
		flexDirection: 'column',
		marginLeft: 15,
	},
});
