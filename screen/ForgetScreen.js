import React, {Component} from 'react';
import {View, Text, Keyboard} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import Buttons from '../components/Buttons';
import FormControl from '../components/FormControl';
import FreshScreen from '../components/FreshScreen';

import ApiConnector from '../helper/ApiConnector';

export default class ForgetScreen extends Component {
	constructor(props) {
		super(props)
		this.state = {
			account: null,
			isSubmitting: false,
			error: {},
		};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	completeValOTP = async theData => {
		this.props.navigation.push('ResetPassword', {
			data: JSON.parse(theData),
		});
	};
	changeAccount = account => {
		if (this.mounted) {
			this.setState({account: account});
		}
		if (account !== undefined && account !== '') {
			let oldError = this.state.error;
			delete oldError.account;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};
	checkAccount = async () => {
		let account = this.state.account;
		this.setState({
			isSubmitting: true,
		});
		let validateObj = {
			scene: 6,
			account: account,
		};
		let validateBody = JSON.stringify(validateObj);
		let validateApi = new ApiConnector({
			apiLink: '/fresh/Login/forgetPassword',
			body: validateBody,
			navigation: this.props.navigation,
			onSuccess: async jsonObj => {
				console.log('JSON OBJ', jsonObj);
				if (jsonObj.resultCode !== 0) {
					if (this.mounted) {
						this.setState({error: jsonObj.data});
					}
				} else {
					let valData = {user_id: jsonObj.data.user_id, scene: 6};
					this.props.navigation.push('ValidateOTP', {
						valParam: JSON.stringify(valData),
						onComplete: this.completeValOTP,
						email: JSON.stringify(jsonObj.data.email),
						mobile: JSON.stringify(jsonObj.data.mobile),
					});
				}
				if (this.mounted) {
				this.setState({isSubmitting: false});
				}
			},
		});
		Keyboard.dismiss();
		await validateApi.submit();
	};
	render() {
		return (
			<FreshScreen navigation={this.props.navigation}>
				<View style={[FreshStyles.Background, FreshStyles.margin20]}>
					<View
						style={[
							FreshStyles.flexRow,
							FreshStyles.centerRow,
							FreshStyles.padBot2,
						]}>
						<Text style={FreshStyles.textBold}>Forget Password</Text>
					</View>
					<FormControl
						style={FreshStyles.textCenter}
						onChangeText={this.changeAccount}
						error={this.state.error.account}
						placeholder="Email Address / Phone Number"
					/>
					<Buttons
						disabled={this.state.isSubmitting ? true : false}
						title={this.state.isSubmitting ? 'Please wait...' : 'Next'}
						onPress={this.checkAccount}
					/>
				</View>
			</FreshScreen>
		);
	}
}
