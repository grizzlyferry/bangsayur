import React, {Component} from 'react';
import {View, Text, Keyboard} from 'react-native';
import {NavigationActions} from 'react-navigation';

import FreshStyles from '../constants/FreshStyles';

import Buttons from '../components/Buttons';
import FormControl from '../components/FormControl';

import ApiConnector from '../helper/ApiConnector';

export default class ResetPasswordScreen extends Component {
	constructor(props) {
		super(props);
		let data = this.props.navigation.getParam('data', null);
		this.state = {
			user_code: data.user_code,
			user_id: data.user_id,
			isSubmitting: false,
			password: null,
			password2: null,
			error: {},
		};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	changePassword = password => {
		if (this.mounted) {
			this.setState({password: password});
		}
		if (password !== undefined && password !== '') {
			let oldError = this.state.error;
			delete oldError.password;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};
	changePassword2 = password2 => {
		if (this.mounted) {
			this.setState({password2: password2});
		}
		if (password2 !== undefined && password2 !== '') {
			let oldError = this.state.error;
			delete oldError.password2;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};
	resetPassword = async () => {
		let user_id = this.state.user_id;
		let user_code = this.state.user_code;
		let password = this.state.password;
		let password2 = this.state.password2;
		this.setState({
			isSubmitting: true,
		});
		let validateObj = {
			scene: 6,
			user_id: user_id,
			user_code: user_code,
			password: password,
			password2: password2,
		};
		let validateBody = JSON.stringify(validateObj);
		let validateApi = new ApiConnector({
			apiLink: '/fresh/login/resetPassword',
			body: validateBody,
			navigation: this.props.navigation,
			onSuccess: async jsonObj => {
				console.log('JSON OBJ', jsonObj);
				if (jsonObj.resultCode !== 0) {
					if (this.mounted) {
						this.setState({error: jsonObj.data});
					}
				} else {
					this.props.navigation.reset(
						[NavigationActions.navigate({routeName: 'Home'})],
						0,
					);
				}
				if (this.mounted) {
					this.setState({isSubmitting: false});
				}
			}
		});
		Keyboard.dismiss();
		await validateApi.submit();
	};
	render() {
		return (
			<View style={[FreshStyles.Background, FreshStyles.margin20]}>
				<View
					style={[
						FreshStyles.flexRow,
						FreshStyles.centerRow,
						FreshStyles.padBot2,
					]}>
					<Text style={FreshStyles.textBold}>Create a New Password</Text>
				</View>

				<FormControl
					style={FreshStyles.textCenter}
					onChangeText={this.changePassword}
					placeholder="New Password"
					error={this.state.error.password}
				/>
				<FormControl
					style={FreshStyles.textCenter}
					onChangeText={this.changePassword2}
					placeholder="Confirm Password"
					error={this.state.error.password2}
				/>
				<Buttons
					disabled={this.state.isSubmitting ? true : false}
					title={this.state.isSubmitting ? 'Saving...' : 'Change Password'}
					onPress={() => this.resetPassword()}
				/>
			</View>
		);
	}
}
