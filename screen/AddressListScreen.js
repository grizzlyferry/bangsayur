import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import FreshScreen from '../components/FreshScreen';
import RadioButton from '../components/RadioButton';

import ApiConnector from '../helper/ApiConnector';

export default class AddressListScreen extends Component {
	constructor(props) {
		super(props);
		// let address_id = this.props.navigation.getParam('address_id', null); -> Ferry : ini yang sementara bikin error
		let address_id = null;
		this.state = {
			addressList: [],
			address: address_id,
		};
	}
	componentDidMount() {
		this.mounted = true;
		this.loadAddress();
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	loadAddress = async () => {
		if (this.state.isLoaded) {
			return;
		}
		if (this.mounted) {
			this.setState({
				isLoaded: true,
			});
		}
		let addressListBody = {
			scene: 8,
		};
		let addressListBodyJson = JSON.stringify(addressListBody);
		let addressListApi = new ApiConnector({
			apiLink: '/fresh/UserAddress/addressList',
			body: addressListBodyJson,
			navigation: this.props.navigation,
			onSuccess: async jsonObj => {
				if (jsonObj.resultCode === 0) {
					if (this.mounted) {
						this.setState({
							addressList: jsonObj.data,
						});
					}
				} else {
					alert('Something Bad Happened!\nPlease Call Our Developer!');
					console.log('Error Details: ', jsonObj);
				}
			},
			onError: error => {
				console.log(error);
				alert('Failed to Retrieve Data!\nPlease Call Our Developer!');
			},
		});
		await addressListApi.submit();
	};
	changeAddress = address => {
		this.props.navigation.navigate('Checkout', {
			newAddress: address,
		});
	};
	render() {
		return (
			<FreshScreen navigation={this.props.navigation}>
				{this.state.addressList.map((address, key) => {
					return (
						<View
							style={[FreshStyles.address]}
							key={'Address_' + address.address_id}>
							{address.is_default == 1 && (
								<Text style={[FreshStyles.titleText]}>Alamat Utama</Text>
							)}
							<View
								style={[FreshStyles.flexRow, FreshStyles.contentSpaceBetween]}>
								<RadioButton
									text={address.is_pickup == 1 ? 'Pick-up Point' : 'Home'}
									textStyle={
										address.is_pickup == 1
											? FreshStyles.pillsPick
											: FreshStyles.pillsHome
									}
									style={[FreshStyles.pad1]}
									value={this.state.address}
									onChange={() => this.changeAddress(address)}
									myValue={address.address_id}
								/>
								<Text style={[styles.text, {color: '#46CF54', paddingTop: 5}]}>
									Edit
								</Text>
							</View>
							<View style={[styles.col]}>
								<Text style={[styles.title]}>{address.consignee}</Text>
								<Text style={[styles.text]}>{address.mobile}</Text>
								<Text style={[styles.text, {marginTop: 5}]}>
									{address.address}
								</Text>
								<Text style={[styles.text]}>{address.zipcode}</Text>
							</View>
						</View>
					);
				})}
				{/* <View style={[FreshStyles.address]}>
					<Text style={[FreshStyles.title]}>Alamat Utama</Text>
					<RadioButton
						text="Home"
						textStyle={[FreshStyles.pillsHome]}
						style={[FreshStyles.pad1]}
						value={this.state.address}
						onChange={this.changeAddress}
						myValue="1"
					/>
					<View style={[styles.col]}>
						<Text style={[styles.title]}>John Doe</Text>
						<Text style={[styles.text]}>+62 1234 5678</Text>
						<Text style={[styles.text, {marginTop: 5}]}>
							Bengkong Kolam, Jalan Cendrawasih Gang Mangga Blok B no 34a
						</Text>
						<Text style={[styles.text]}>
							Bengkong, Kota Batam, Kepulauan Riau 29458
						</Text>
					</View>
				</View>
				<View style={[FreshStyles.address]}>
					<RadioButton
						text="Pick Up Point"
						textStyle={[FreshStyles.pillsPick]}
						style={[FreshStyles.pad1]}
						value={this.state.address}
						onChange={this.changeAddress}
						myValue="2"
					/>
					<View style={[styles.col]}>
						<Text style={[styles.title]}>John Doe</Text>
						<Text style={[styles.text]}>+62 1234 5678</Text>
						<Text style={[styles.text, {marginTop: 5}]}>
							Bengkong Kolam, Jalan Cendrawasih Gang Mangga Blok B no 34a
						</Text>
						<Text style={[styles.text]}>
							Bengkong, Kota Batam, Kepulauan Riau 29458
						</Text>
					</View>
				</View> */}
			</FreshScreen>
		);
	}
}
const styles = StyleSheet.create({
	text: {
		fontSize: 12,
	},
	title: {
		fontSize: 13,
		fontWeight: 'bold',
	},
	col: {
		flexDirection: 'column',
		marginLeft: 40,
	},
	btn: {
		alignItems: 'center',
		margin: 15,
	},
});
