import React, {Component} from 'react';
import {View, TouchableOpacity, Image, Text} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import Buttons from '../components/Buttons';
import Cart from '../components/Cart';
import FreshScreen from '../components/FreshScreen';
import NumberForm from '../components/NumberForm';
import ProductList from '../components/ProductList';

import Session from '../helper/Session';

export default class CartScreen extends Component {
	constructor(props) {
		super(props);
		if (this.props.requestClose) {
			this.requestClose = this.props.requestClose;
		}
		this.state = {
			quantity: 1,
			token: false,
			isChecking: false,
		};
	}

	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}

	checkToken = async () => {
		if (this.mounted) {
			this.setState({
				isChecking: true,
			});
		}
		let tokenSession = await Session.getValue('fresh_token');
		if (this.mounted) {
			this.setState({
				token: tokenSession,
				isChecking: false,
			});
		}
		this.props.navigation.push(
			this.state.token === null ? 'Login' : 'Checkout',
		);
	};

	changeQuantity = targetValue => {
		this.setState({
			quantity: targetValue,
		});
	};
	render() {
		return (
			<FreshScreen navigation={this.props.navigation}>
				<Cart navigation={this.props.navigation} />
				{/* <ProductList
					type="three-column"
					apiLink="/fresh/Product/productList"
					bodyParam={JSON.stringify({sort: 'new'})}
					navigation={this.props.navigation}
					title="Check out the hot deals"
				/> */}
				<View
					style={[
						FreshStyles.flexRow,
						FreshStyles.pad2,
						{borderBottomWidth: 1, borderBottomColor: '#dedede'},
					]}>
					<Image
						style={[
							FreshStyles.width30Percent,
							FreshStyles.height30Percent,
							FreshStyles.cornerRad5,
						]}
						source={require('../assets/nHBfsgAADAAAABEAGxmc-AADyMU.jpg')}
						onError={this.imageError}
						resizeMode="cover"
					/>
					<View style={[FreshStyles.cartText]}>
						<Text style={[FreshStyles.textBold, FreshStyles.padVer1]}>
							Bak cang
						</Text>
						<Text style={[FreshStyles.text12]}>1 pcs (800g)</Text>
						<Text style={[FreshStyles.text12]}>Stock : 200</Text>
						<View style={[FreshStyles.flex1]} />
						<Text style={[FreshStyles.textBold]}>$ 200</Text>
					</View>
					<NumberForm
						style={[FreshStyles.selfEnd]}
						value={this.state.quantity}
						change={this.changeQuantity}
					/>
				</View>
				<View
					style={{
						flex: 1,
						justifyContent: 'flex-end',
						flexDirection: 'column',
					}}>
					<View style={[FreshStyles.cartBottom]}>
						<View style={[FreshStyles.flexColumn]}>
							<Text style={[FreshStyles.text11, FreshStyles.color1]}>
								SUB TOTAL :
							</Text>
							<Text style={[FreshStyles.textBold, FreshStyles.navTextGreen]}>
								$ 250
							</Text>
							<Text style={[FreshStyles.text10, FreshStyles.color1]}>
								Does not include postage
							</Text>
						</View>
						<Buttons
							title={this.state.isChecking ? 'Checking' : 'Checkout'}
							viewStyle={[FreshStyles.width30]}
							textStyle={[FreshStyles.greenButton, {color: 'white'}]}
							onPress={() => this.checkToken()}
						/>
					</View>
				</View>
			</FreshScreen>
		);
	}
}
