import React, {Component} from 'react';
import {
	View,
	Text,
	Image,
	TouchableOpacity,
	StyleSheet,
	ActivityIndicator,
} from 'react-native';

import {currencySymbol} from '../constants/Config';
import FreshStyles from '../constants/FreshStyles';

import Buttons from '../components/Buttons';
import FreshScreen from '../components/FreshScreen';
import RadioButton from '../components/RadioButton';

import ApiConnector from '../helper/ApiConnector';

export default class CheckoutScreen extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		// let newAddress = this.props.navigation.getParam('newAddress', null); -> Ferry : Ini yang sementara ini bikin error saya ganti pakai null
		let newAddress = null;
		this.state = {
			address: null,
			isChecking: false,
			isAddressLoaded: false,
			isShippingLoaded: false,
			isPaymentLoaded: false,
			defaultAddress: newAddress,
			shippingList: [],
			paymentList: [],
			error: null,
		};
	}
	componentDidMount() {
		this.mounted = true;
		if (this.state.defaultAddress == null) {
			this.loadAddress();
			//   this.loadShippingList();
			//   this.loadPaymentList();
		} else {
			this.setState({
				isAddressLoaded: true,
			});
		}
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	componentDidUpdate(prevProps) {
		let oldAddress = prevProps.navigation.getParam('newAddress', null);
		let newAddress = this.props.navigation.getParam('newAddress', null);
		if (oldAddress !== newAddress) {
			if (this.mounted) {
				this.setState({
					defaultAddress: newAddress,
				});
			}
		}
	}
	loadAddress = async () => {
		if (this.state.isAddressLoaded) {
			return;
		}
		let defaultAddressBody = {
			scene: 8,
		};
		let defaultAddressApi = new ApiConnector({
			apiLink: '/fresh/UserAddress/getDefaultAddress',
			body: JSON.stringify(defaultAddressBody),
			navigation: this.props.navigation,
			onSuccess: async jsonObj => {
				if (jsonObj.resultCode === 0) {
					if (this.mounted) {
						this.setState({
							isAddressLoaded: true,
							defaultAddress: jsonObj.data,
						});
					}
				} else {
					console.log('Error Details: ', jsonObj);
					if (this.mounted) {
						this.setState({
							isAddressLoaded: true,
							error:
								"You don't have a default address and or you haven't registered an address.",
						});
					}
				}
			},
		});
		await defaultAddressApi.submit();
	};
	// loadShippingList = async () => {
	// 	console.log('payment method start');
	// 	if (this.state.isShippingLoaded) {
	// 		return;
	// 	}
	// 	let shippingListApi = new ApiConnector({
	// 		apiLink: '/fresh/Shipping/listShipping',
	// 		navigation: this.props.navigation,
	// 		onSuccess: async jsonObj => {
	// 			if (jsonObj.resultCode === 0) {
	// 				if (this.mounted) {
	// 					this.setState({
	// 						isShippingLoaded: true,
	// 						shippingList: jsonObj.data,
	// 					});
	// 				}
	// 			} else {
	// 				console.log('Error Details: ', jsonObj);
	// 				if (this.mounted) {
	// 					this.setState({
	// 						isShippingLoaded: true,
	// 						error: 'There is no shipping method for this product.',
	// 					});
	// 				}
	// 			}
	// 		},
	// 	});
	// 	console.log('payment method end');
	// 	await shippingListApi.submit();
	// };

	// loadPaymentList = async () => {
	// 	if (this.state.isPaymentLoaded) {
	// 		return;
	// 	}
	// 	let paymentListApi = new ApiConnector({
	// 		apiLink: '/fresh/Payment/listPayment',
	// 		navigation: this.props.navigation,
	// 		onSuccess: async jsonObj => {
	// 			if (jsonObj.resultCode === 0) {
	// 				if (this.mounted) {
	// 					this.setState({
	// 						isPaymentLoaded: true,
	// 						paymentList: jsonObj.data,
	// 					});
	// 				}
	// 			} else {
	// 				console.log('Error Details: ', jsonObj);
	// 				if (this.mounted) {
	// 					this.setState({
	// 						isPaymentLoaded: true,
	// 						error: 'There is no payment method for this product.',
	// 					});
	// 				}
	// 			}
	// 		},
	// 	});
	// 	await paymentListApi.submit();
	// };

	order = () => {
		return (
			<View style={[FreshStyles.cartBottom]}>
				<View style={[FreshStyles.flexColumn]}>
					<Text style={[FreshStyles.text11, FreshStyles.color1]}>TOTAL :</Text>
					<Text style={[FreshStyles.textBold, FreshStyles.navTextGreen]}>
						$ 250
					</Text>
				</View>
				<Buttons
					title={this.state.isChecking ? 'Processing...' : 'Place Order'}
					viewStyle={[FreshStyles.width30]}
					textStyle={[FreshStyles.greenButton, {color: 'white'}]}
				/>
			</View>
		);
	};

	changeAddress = address => {
		this.setState({
			address: address,
		});
	};
	changeDefaultAddress = dataAddress => {
		this.setState({
			defaultAddress: dataAddress,
		});
	};
	render() {
		console.log(this.state.shippingList);
		return (
			<FreshScreen navigation={this.props.navigation} bottom={this.order()}>
				<View style={[FreshStyles.address]}>
					{this.state.error !== null && (
						<View style={[FreshStyles.flexRowWrap]}>
							<Text>{this.state.error}</Text>
							<TouchableOpacity
								onPress={() => this.props.navigation.push('NewAddress')}>
								<Text style={[FreshStyles.fgColor5]}>Add address</Text>
							</TouchableOpacity>
						</View>
					)}
					{this.state.defaultAddress !== null &&
						this.state.defaultAddress !== {} && (
							<View>
								<View
									style={[
										FreshStyles.flexRow,
										FreshStyles.contentSpaceBetween,
									]}>
									<Text style={[FreshStyles.titleText]}>Shipping Address</Text>
									<TouchableOpacity
										onPress={() =>
											this.props.navigation.push('AddressList', {
												address_id: this.state.defaultAddress.address_id,
												changeAddress: this.changeDefaultAddress,
											})
										}>
										<Text
											style={[styles.text, {color: '#46CF54', paddingTop: 5}]}>
											Select another Address
										</Text>
									</TouchableOpacity>
								</View>
								<View style={[FreshStyles.flexColumn, FreshStyles.marginTop2]}>
									<Text style={[styles.title]}>
										{this.state.defaultAddress.consignee}
									</Text>
									<Text style={[styles.text]}>
										{this.state.defaultAddress.mobile}
									</Text>
									<Text style={[styles.text, {marginTop: 5}]}>
										{this.state.defaultAddress.address}
									</Text>
									<Text style={[styles.text]}>
										{this.state.defaultAddress.zipcode}
									</Text>
								</View>
							</View>
						)}
					{!this.state.isAddressLoaded && (
						<ActivityIndicator
							size="large"
							style={{margin: 33, alignItems: 'center'}}
							color="#00ff00"
						/>
					)}
				</View>

				<View style={[FreshStyles.address]}>
					<Text style={[FreshStyles.titleText]}>Shipping Method</Text>
					{this.state.shippingList.map((shipping, key) => {
						return (
							<View
								style={[
									FreshStyles.flexRow,
									FreshStyles.contentSpaceBetween,
									styles.border,
								]}
								key={'shipping_' + shipping.shipping_id}>
								<RadioButton
									text={shipping.shipping_name}
									textStyle={[styles.title]}
									style={[[FreshStyles.pad1]]}
									value={this.state.address}
									onChange={this.changeAddress}
									myValue={shipping.shipping_id}
								/>
								<Text style={[styles.title, {paddingTop: 5}]}>
									{currencySymbol}20
								</Text>
							</View>
						);
					})}
				</View>

				<View style={[FreshStyles.address]}>
					<Text style={[FreshStyles.titleText]}>List Pesanan</Text>
					<View style={[FreshStyles.flexRow, FreshStyles.pad2, styles.border]}>
						<Image
							style={[FreshStyles.width25Percent, FreshStyles.height25Percent]}
							source={require('../assets/nHBfsgAADAAAABEAGxmc-AADyMU.jpg')}
							onError={this.imageError}
							resizeMode="cover"
						/>
						<View style={[FreshStyles.cartText]}>
							<View style={[FreshStyles.start]}>
								<Text style={[styles.title]}>Bak cang</Text>
								<Text style={[FreshStyles.text12]}>1 pcs (800g)</Text>
								<Text style={[FreshStyles.text12]}>Stock : 200</Text>
							</View>
							<View style={[FreshStyles.end]}>
								<Text style={[styles.title]}>{currencySymbol}200</Text>
							</View>
						</View>
					</View>
					<View style={[FreshStyles.flexRow, FreshStyles.pad2, styles.border]}>
						<Image
							style={[FreshStyles.width25Percent, FreshStyles.height25Percent]}
							source={require('../assets/nHBfsgAADAAAABEAGxmc-AADyMU.jpg')}
							onError={this.imageError}
							resizeMode="cover"
						/>
						<View style={[FreshStyles.cartText]}>
							<View style={[FreshStyles.start]}>
								<Text style={[styles.title]}>Bak cang</Text>
								<Text style={[FreshStyles.text12]}>1 pcs (800g)</Text>
								<Text style={[FreshStyles.text12]}>Stock : 200</Text>
							</View>
							<View style={[FreshStyles.end]}>
								<Text style={[styles.title]}>{currencySymbol}200</Text>
							</View>
						</View>
					</View>
				</View>

				<View
					style={[
						{
							backgroundColor: 'white',
							marginTop: 15,
							paddingVertical: 5,
							paddingHorizontal: 20,
						},
					]}>
					<View
						style={[
							FreshStyles.flexRow,
							FreshStyles.contentSpaceBetween,
							styles.border,
						]}>
						<Text style={[FreshStyles.titleText]}>Payment Method</Text>
						<TouchableOpacity>
							<Text style={[styles.text, {color: '#46CF54', paddingTop: 2.5}]}>
								Select Payment method
							</Text>
						</TouchableOpacity>
					</View>
					<View style={[styles.col, FreshStyles.marginVer5]}>
						<View
							style={[FreshStyles.flexRow, FreshStyles.contentSpaceBetween]}>
							<Text style={[styles.text]}>Subtotal Product</Text>
							<Text style={[styles.text]}>{currencySymbol}20</Text>
						</View>
						<View
							style={[FreshStyles.flexRow, FreshStyles.contentSpaceBetween]}>
							<Text style={[styles.text]}>Subtotal Delivery</Text>
							<Text style={[styles.text]}>{currencySymbol}20</Text>
						</View>
						<View
							style={[FreshStyles.flexRow, FreshStyles.contentSpaceBetween]}>
							<Text style={[styles.title]}>Total Pembayaran</Text>
							<Text style={[styles.title, {color: '#46Cf54'}]}>
								{currencySymbol}20
							</Text>
						</View>
					</View>
					{this.state.paymentList.map((pay, key) => {
						return (
							<View
								style={[
									FreshStyles.flexRow,
									FreshStyles.contentSpaceBetween,
									styles.border,
								]}
								key={'pay_' + pay.pay_id}>
								<RadioButton
									text={pay.pay_name}
									textStyle={[styles.title]}
									style={[[FreshStyles.pad1]]}
									value={this.state.address}
									onChange={this.onChange}
									myValue={pay.pay_id}
								/>
								<Text style={[styles.title, {paddingTop: 5}]}>
									{currencySymbol}20
								</Text>
							</View>
						);
					})}
				</View>
			</FreshScreen>
		);
	}
}

const styles = StyleSheet.create({
	text: {
		fontSize: 12,
	},
	title: {
		fontSize: 13,
		fontWeight: 'bold',
	},
	col: {
		flexDirection: 'column',
		marginLeft: 40,
	},
	btn: {
		alignItems: 'center',
		margin: 15,
	},
	border: {
		paddingVertical: 10,
		borderBottomWidth: 1,
		borderBottomColor: '#F5F6F8',
	},
});
