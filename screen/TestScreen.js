import React, {Component} from 'react';
import {Text,TouchableOpacity} from 'react-native';

import AddProductToCart from '../components/AddProductToCart';
import FreshScreen from '../components/FreshScreen';

import FreshLayoutHelper from '../helper/FreshLayoutHelper';

export default class TestScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showOverlay: false,
		};
	}
	openOverlay = () => {
		this.setState({showOverlay: true});
	}
	closeOverlay = () => {
		this.setState({showOverlay: false});
	}
	render() {
		// return (
		// 	<FreshScreen navigation={this.props.navigation}>
		// 		<TouchableOpacity onPress={this.openOverlay}><Text>Click to {this.state.showOverlay?"Hide":"Show"} Overlay</Text></TouchableOpacity>
		// 		<AddProductToCart visible={this.state.showOverlay} requestClose={this.closeOverlay} />
		// 	</FreshScreen>
		// );
		return (
			<FreshLayoutHelper
				layoutId={1}
				useScroll={true}
				navigation={this.props.navigation} />
		);
	}
}
