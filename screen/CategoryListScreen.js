import React, {Component} from 'react';
import {View} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import CategoryBox from '../components/CategoryBox';
import CategoryBox2 from '../components/CategoryBox2';
import FreshScreen from '../components/FreshScreen';
import LoadingBox from '../components/LoadingBox';

import ApiConnector from '../helper/ApiConnector';

export default class CategoryListScreen extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		console.log(this.props.navigation.state);
		// let category_id = this.props.navigation.getParam('category_id', null); -> Ferry : ini yang bikin error sementara jadi null 
		let category_id = null;
		this.state = {
			category_id: category_id,
			isLoaded: false,
			categoryList: [],
			noMore: true,
			totalCount: null,
		};
		this.loadCategory();
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}

	loadCategory = async (offset = 0) => {
		if (this.state.isLoaded) {
			return;
		}
		if (this.mounted) {
			this.setState({
				isLoaded: true,
			});
		}
		let categoryListBody = {
			// level: this.state.level === null ? 1 : this.state.level,
			// category_id: this.state.category_id, //Only for level 2
		};
		if (this.state.level) {
			categoryListBody.level = this.state.level;
		}
		if (this.state.category_id) {
			categoryListBody.category_id = this.state.category_id;
		}
		let categoryListBodyJson = JSON.stringify(categoryListBody);
		let categoryListApi = new ApiConnector({
			apiLink: '/fresh/Category/categoryList',
			body: categoryListBodyJson,
			navigation: this.props.navigation,
			onSuccess: async jsonObj => {
				if (jsonObj.resultCode === 0) {
					let noMore = false;
					let categoryList = this.state.categoryList.concat(jsonObj.data);
					if (categoryList.length >= jsonObj.count) {
						noMore = true;
					}
					if (this.mounted) {
						this.setState({
							isLoaded: true,
							categoryList: categoryList,
							noMore: noMore,
							totalCount: jsonObj.count,
						});
					}
				} else {
					alert('Something Bad Happened!\nPlease Call Our Developer!');
					console.log('Error Details: ', jsonObj);
					if (this.mounted) {
						this.setState({
							isLoaded: false,
						});
					}
				}
			},
			onError: error => {
				console.log(error);
				alert('Failed to Retrieve Data!\nPlease Call Our Developer!');
			},
		});
		await categoryListApi.submit();
	};
	render() {
		return (
			<FreshScreen navigation={this.props.navigation}>
				<View style={[FreshStyles.white]}>
					{this.state.level != 2 && (
						<View>
							{!this.state.isLoaded && (
								<LoadingBox
									style={[FreshStyles.flexRowWrap, FreshStyles.pad2]}
									count={15}
									cart="false"
									viewStyle={[
										FreshStyles.categoryBoxWrapper2,
										FreshStyles.margin5,
									]}
									imageStyle={[
										FreshStyles.width15Percent,
										FreshStyles.height25Percent,
									]}
								/>
							)}
							{this.state.isLoaded && (
								<View style={[FreshStyles.flexRowWrap, FreshStyles.pad2]}>
									{this.state.categoryList.map((category, key) => {
										return (
											<CategoryBox
												key={'category ' + category.id}
												data={category}
												navigation={this.props.navigation}
											/>
										);
									})}
								</View>
							)}
						</View>
					)}
				</View>
				{this.state.level == 2 && (
					<View>
						{!this.state.isLoaded && (
							<View style={[FreshStyles.CatWrapper, FreshStyles.padHor1]}>
								<LoadingBox
									style={[FreshStyles.flexRowWrap, FreshStyles.centerRow]}
									count={16}
									cart="false"
									viewStyle={[
										FreshStyles.boxNoShadow,
										FreshStyles.width225Percent,
									]}
									imageStyle={[
										FreshStyles.width225Percent,
										FreshStyles.height20Percent,
									]}
								/>
							</View>
						)}
						{this.state.isLoaded && (
							<View style={[FreshStyles.CatWrapper]}>
								{this.state.categoryList.map((item, key) => {
									return (
										<CategoryBox2
											key={'cat_' + key}
											data={item}
											navigation={this.props.navigation}
										/>
									);
								})}
							</View>
						)}
					</View>
				)}
			</FreshScreen>
		);
	}
}
