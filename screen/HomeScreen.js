import React, {Component} from 'react';
import { Button, View, Text } from 'react-native';

// import FreshLayoutHelper from '../helper/FreshLayoutHelper';

export default class HomeScreen extends Component {
	render() {
		return (
			

			<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
				<Text>Lompat Halaman Dari Sini ! {"\n"}</Text>
				<Button
				title="Jump To"
				onPress={() => this.props.navigation.push('Profile')}
				/>
			</View>
			
			// <FreshLayoutHelper
			// 	layoutId={1}
			// 	navigation={this.props.navigation}
			// 	useScreen={true}
			// 	useScroll={false}
			// />
		);
	}
}