import React, {Component} from 'react';
import {View, Dimensions, Text} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import Buttons from '../components/Buttons';

export default class ErrorScreen extends Component {
	constructor(props) {
		super(props);
		let banned = null;
		let devErrorCode = null;
		if(this.props.navigation) {
			// banned = this.props.navigation.getParam('banned', null); -> Ferry : Sementara ini yang bikin error, sementara diganti null
			// devErrorCode = this.props.navigation.getParam('devErrorCode', null); -> Ferry : Sementara ini yang bikin error
			banned = null;
			devErrorCode = null;
		} else {
			if(this.props.banned) {
				banned = this.props.banned;
			}
			if(this.props.devErrorCode) {
				devErrorCode = this.props.devErrorCode;
			}
		}
		this.state = {
			banned: banned,
			devErrorCode: devErrorCode,
		};
	}
	errorNoNavigation = () => {
		alert("Something is totally wrong with the Developer!");
	}
	updateApp = () => {
		alert("Not handled yet!");
	}
	render() {
		return (
			<View
				style={[
					FreshStyles.Background,
					FreshStyles.margin20,
					FreshStyles.allCenter,
					FreshStyles.width100,
				]}>
				{
					this.state.banned == null &&
					<View>
						<Text style={FreshStyles.textCenter}>
							Your app version is Broken/Invalid/Outdated!{'\n'}
							Please Update or Re-install the App!
						</Text>
						<Text style={[FreshStyles.errorText, FreshStyles.textCenter]}>
							Error Code : {this.state.devErrorCode}
						</Text>
						<Buttons style={FreshStyles.width100} title="Update" onPress={this.updateApp} />
					</View>
				}
				{
					this.state.banned != null &&
					<View>
						<Text style={FreshStyles.textCenter}>
							Your Account is Banned!{'\n'}
							Please Contact Us!!!
						</Text>
						<Buttons
							style={FreshStyles.width100}
							title="Back"
							onPress={
								(this.props.navigation === null) ?
								this.errorNoNavigation:
								this.props.navigation.pop.bind(this)
							} />
					</View>
				}
			</View>
		);
	}
}
