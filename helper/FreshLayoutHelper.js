import React, {PureComponent} from 'react';
import {
	ActivityIndicator,
	ScrollView,
	View,
} from 'react-native';
import FreshStyles from '../constants/FreshStyles';

import Banner from '../components/Banner';
import CategoryList from '../components/CategoryList';
import FreshScreen from '../components/FreshScreen';
import ProductList from '../components/ProductList';

import ApiConnector from '../helper/ApiConnector';

import ErrorScreen from '../screen/ErrorScreen';

export default class FreshLayoutHelper extends PureComponent {
	static defaultProps = {
		layoutId: 1,
		useScreen: false,
		useScroll: true,
	};
	constructor(props) {
		super(props);
		this.state = {
			isLoaded: false,
			layoutDetail: null,
		};
	}
	componentDidMount() {
		this.mounted = true;
		this.loadLayout();
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	loadLayout = async () => {
		if(this.state.isLoaded || !this.props.navigation) {
			return;
		}
		let layoutApi = new ApiConnector({
			apiLink: '/fresh/Layout/getLayout',
			body: JSON.stringify({layout_id: this.props.layoutId}),
			navigation: this.props.navigation,
			onSuccess: async jsonObj => {
				if(jsonObj.resultCode===0) {
					if(this.mounted) {
						this.setState({
							isLoaded: true,
							layoutDetail: jsonObj.data,
						});
					}
				} else {
					this.props.navigation.reset(
						[
							NavigationActions.navigate({
								routeName: 'Error',
								params: {devErrorCode: '-06'},
							}),
						],
						0,
					);
				}
			},
		});
		await layoutApi.submit();
	};
	renderItem = (item, key) => {
		let theParams = null;
		let viewParams = null;
		if(item.params) {
			theParams = JSON.parse(item.params);
		}
		if(item.viewParams) {
			viewParams = JSON.parse(item.viewParams);
		}
		if(item.type==='ProductList') {
			return (
				<ProductList
					key={'layout_' + this.props.layoutId + '_item_' + key}
					apiLink={item.api}
					params={theParams}
					{...viewParams}
					title={item.title}
					navigation={this.props.navigation}
				/>
			);
		} else if(item.type==='FullBanner') {
			return (
				<Banner
					key={'layout_' + this.props.layoutId + '_item_' + key}
					apiLink={item.api}
					params={theParams}
					{...viewParams}
					title={item.title}
					navigation={this.props.navigation}
				/>
			);
		} else if(item.type==='CategoryList') {
			return (
				<CategoryList
					key={'layout_' + this.props.layoutId + '_item_' + key}
					apiLink={item.api}
					params={theParams}
					{...viewParams}
					title={item.title}
					navigation={this.props.navigation}
				/>
			);
		} // TODO, add more else if for newer type.
	};
	render() {
		console.log("FreshLayoutHelper render isLoaded? ", this.state.isLoaded, " layoutId? ", this.props.layoutId);
		if(!this.props.navigation) {
			return <ErrorScreen devErrorCode="-05" />;
		}
		if(!this.state.isLoaded) {
			return (
				<ActivityIndicator
					style={FreshStyles.layoutLoading}
					size="large"
					color="#0000ff"
				/>
			);
		}
		if(this.props.useScreen) {
			return (
				<FreshScreen
					navigation={this.props.navigation}
					useScroll={this.props.useScroll}>
					{this.state.layoutDetail.map(this.renderItem)}
				</FreshScreen>
			);
		} else if(this.props.useScroll) {
			return (
				<ScrollView>
					{this.state.layoutDetail.map(this.renderItem)}
				</ScrollView>
			);
		} else {
			return (
				<View>
					{this.state.layoutDetail.map(this.renderItem)}
				</View>
			);
		}
	}
}
