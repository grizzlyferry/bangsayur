import {Platform} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import {NavigationActions} from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import md5 from 'md5';

import {serverAddress, serverSecret} from '../constants/FreshServer';

export default class ApiConnector {
	#baseApi = serverAddress;
	props;
	constructor(props) {
		this.props = props;
		if (this.props.method === undefined) {
			this.props.method = 'POST';
		}
		this.onSuccess = this.props.onSuccess.bind(this);
		if (this.props.onError !== undefined) {
			this.onError = this.props.onError.bind(this);
		}
	}
	onError = async error => {
		this.goToErrorCritical({devErrorCode: '-01'});
	};
	submit = async () => {
		let headers = {
			'Content-Type': 'application/json',
			Accept: 'application/json',
		};
		try {
			let deviceString =
				'Device ' + DeviceInfo.getUniqueId() + ' Platform ' + Platform.OS;
			headers.Authorization = deviceString;
			let token = await AsyncStorage.getItem('fresh_token');
			if (token !== null) {
				headers.Authorization += ' Bearer ' + token;
			}
		} catch (error) {
			this.goToErrorCritical({devErrorCode: '-02'});
		}

		let bodyObj = {};
		if(this.props.body !== null &&
			this.props.body !== undefined &&
			this.props.body !== "") {
			bodyObj = JSON.parse(this.props.body);
		}
		bodyObj.client = 'fresh-react-native';
		let keys = Object.keys(bodyObj).sort();
		let dataString = '';
		for (let key in keys) {
			dataString += keys[key] + '=' + bodyObj[keys[key]] + '&';
		}
		dataString = dataString.replace(/^&+|&+$/g, '') + serverSecret;
		let signature = md5(dataString);
		bodyObj.sign = signature;

		let objToSend = JSON.stringify(bodyObj);

		fetch(this.#baseApi + this.props.apiLink, {
			method: this.props.method,
			headers: headers,
			body: objToSend,
		})
			.then(response => {
				if (response.status === 200) {
					return response.json();
				} else {
					this.goToErrorCritical({devErrorCode: '-03'});
					return;
				}
			})
			.then(async jsonObject => {
				if (jsonObject.resultCode === -4) {
					this.goToErrorCritical({devErrorCode: jsonObject.data});
					return;
				} else if (
					jsonObject.status != 0 &&
					jsonObject.data !== undefined &&
					jsonObject.data !== null &&
					typeof jsonObject.data === 'object'
				) {
					let keys = Object.keys(jsonObject.data);
					if (keys.indexOf('scene') >= 0) {
						this.goToErrorCritical({devErrorCode: '-04'});
						return;
					}
				}
				await this.props.onSuccess(jsonObject);
			})
			.catch(error => {
				this.onError(error);
			});
	};
	goToErrorCritical = (params = null) => {
		this.props.navigation.reset(
			[NavigationActions.navigate({routeName: 'Error', params: params})],
			0,
		);
	};
}
