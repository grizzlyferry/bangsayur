import {serverAddress} from '../constants/FreshServer';
// import DummyProfilePicture from './DummyProfilePicture';

export default class ImageHelper {
	static returnSource(imageString, type = 'product', user_id = 0) {
		if (
			imageString === undefined ||
			imageString === null ||
			imageString === ''
		) {
			switch (type) {
				case 'product':
					return require('../assets/default_product_1.png');
				case 'products':
					return require('../assets/default_product_2.png');
				case 'category':
					return require('../assets/default_category.png');
				// case "profile" :
				// 	return DummyProfilePicture.get(user_id);
				default:
					return require('../assets/default_default.png');
			}
		} else {
			if (!/^(f|ht)tps?:\/\//i.test(imageString)) {
				return {uri: serverAddress + imageString};
			} else {
				return {uri: imageString};
			}
		}
	}
}
