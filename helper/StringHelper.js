export default class StringHelper {
	static isMobile(stringToCheck) {
		return /\+?\d/g.test(stringToCheck);
	}
	static isEmail(stringToCheck) {
		return /[a-zA-Z0-9._]+@[a-zA-Z0-9].[a-zA-Z]/g.test(stringToCheck);
	}
	// get String Text Type of the stringToCheck
	// return value :
	// => 1 = is mobile number
	// => 2 = is email address
	// => otherwise 0
	static getTextType(stringToCheck) {
		if(StringHelper.isMobile(stringToCheck)) {
			return 1;
		} else if(StringHelper.isEmail(stringToCheck)) {
			return 2;
		} else {
			return 0;
		}
	}
}