import React, {Component} from 'react';
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native';

export default class Button extends Component {
	static defaultProps = {
		disabled: false,
	};
	constructor(props) {
		super(props);
		this.state = {};
	}
	render() {
		return (
			<View style={(Buttonstyles.my, this.props.viewStyle)}>
				<TouchableOpacity
					style={this.props.style}
					disabled={this.props.disabled}
					onPress={this.props.onPress}>
					<Text
						style={[
							Buttonstyles.button,
							Buttonstyles.center,
							this.props.textStyle,
						]}>
						{this.props.title}
					</Text>
				</TouchableOpacity>
			</View>
		);
	}
}
const Buttonstyles = StyleSheet.create({
	button: {
		alignItems: 'center',
		backgroundColor: '#DDDDDD',
		padding: 10,
	},
	my: {
		marginVertical: 10,
	},
	center: {
		alignContent: 'center',
		textAlign: 'center',
	},
});
