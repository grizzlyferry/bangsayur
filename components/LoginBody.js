import React, {Component} from 'react';
import {View, TouchableOpacity, Text, Keyboard} from 'react-native';
import Buttons from '../components/Buttons';
import FormInput from '../components/FormInput';
import ApiConnector from '../helper/ApiConnector';
import Session from '../helper/Session';
import FreshStyles from '../constants/FreshStyles';

export default class LoginBody extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		this.state = {
			username: null,
			password: null,
			error: {},
		};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	completeValOTP = async theData => {
		this.props.navigation.push('Home', {
			data: JSON.parse(theData),
		});
		console.log('complete val otp ', theData);
	};
	submitLogin = async () => {
		let username = this.state.username;
		let password = this.state.password;
		if (this.mounted) {
			this.setState({
				isSubmitting: true,
			});
		}
		let loginObj = {
			scene: 3,
			username: username,
			password: password,
		};
		let loginBody = JSON.stringify(loginObj);
		let loginApi = new ApiConnector({
			apiLink: '/fresh/Login/login',
			body: loginBody,
			navigation: this.props.navigation,
			onSuccess: async jsonObj => {
				console.log(jsonObj);
				if (jsonObj.resultCode !== 0) {
					if (this.mounted) {
						this.setState({error: jsonObj.data});
					}
					if (jsonObj.resultCode === -2 && jsonObj.data.lock_code == 1) {
						console.log('Result', jsonObj);
						let valData = {
							user_id: jsonObj.data.user_id,
							scene: 5,
						};
						this.props.navigation.push('ValidateOTP', {
							valParam: JSON.stringify(valData),
							onComplete: this.completeValOTP,
							email: JSON.stringify(jsonObj.data.email),
							mobile: JSON.stringify(jsonObj.data.mobile),
						});
					}
					if (jsonObj.resultCode === -2 && jsonObj.data.lock_code == 2) {
						console.log('Result', jsonObj);
						this.props.navigation.push('Error', {
							banned: true,
						});
					}
				} else if (jsonObj.resultCode === 0) {
					//SetSession
					let token = jsonObj.data.token;
					Session.setValue('fresh_token', token);
					this.props.navigation.push('Home');
				} else {
					// this.handleLoginError(
					//   'Error!\nError Code: -8',
					//   'Something wrong Logining your account!\nIf this problem persist, please contact our developers with the error code above!\nThank you!',
					// );
				}
				if (this.mounted) {
					this.setState({isSubmitting: false});
				}
			},
			onError: error => {
				// this.handleLoginError(
				//   'No Internet Connection!',
				//   'You need to be connected to the Internet to do that!',
				// );
				if (this.mounted) {
					this.setState({isSubmitting: false});
				}
			},
		});
		Keyboard.dismiss();
		await loginApi.submit();
	};
	changeUsername = username => {
		if (this.mounted) {
			this.setState({username: username});
		}
		if (username !== undefined && username !== '') {
			let oldError = this.state.error;
			delete oldError.username;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};
	changePassword = password => {
		if (this.mounted) {
			this.setState({password: password});
		}
		if (password !== undefined && password !== '') {
			let oldError = this.state.error;
			delete oldError.password;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};
	render() {
		let {username, password} = this.state;
		return (
			<View style={[FreshStyles.flex1, FreshStyles.marginTop2]}>
				<View style={[FreshStyles.selfCenter, FreshStyles.width80]}>
					<FormInput
						placeholder="Username"
						onChangeText={this.changeUsername}
						value={this.state.username}
						error={this.state.error.username}
					/>
					<FormInput
						placeholder="Password"
						secureTextEntry={true}
						onChangeText={this.changePassword}
						value={this.state.password}
						error={this.state.error.password}
					/>
					<Buttons
						disabled={this.state.isSubmitting ? true : false}
						title={this.state.isSubmitting ? 'Logging in...' : 'Log in'}
						onPress={this.submitLogin}
					/>
				</View>
				<View style={[FreshStyles.alignCenter]}>
					<TouchableOpacity
						style={FreshStyles.padVer2}
						onPress={() => this.props.navigation.push('Register')}>
						<Text>Quick Register</Text>
					</TouchableOpacity>
					<TouchableOpacity
						onPress={() =>
							this.props.navigation.push('Forget', {page: 'ForgetPassword'})
						}>
						<Text>Forget Password?</Text>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}
