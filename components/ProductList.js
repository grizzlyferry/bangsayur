import React, {PureComponent} from 'react';
import {
	View,
	Text,
	FlatList,
	TouchableOpacity,
	Image,
} from 'react-native';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faAngleRight} from '@fortawesome/free-solid-svg-icons';

import FreshStyles, {screenWidth, screenHeight} from '../constants/FreshStyles';

import Banner from './Banner';
import LoadingBox from './LoadingBox';
import ProductBox from './ProductBox';

import ApiConnector from '../helper/ApiConnector';
import FreshLayoutHelper from '../helper/FreshLayoutHelper';

export default class ProductList extends PureComponent {
	static defaultProps = {
		apiLink: '/fresh/Product/productList',
		infiniteScroll: true,
		pageSize: 20,
		type: 'two-column',
	};
	constructor(props) {
		super(props);
		this.state = {
			isLoading: false,
			isLoaded: false,
			productList: [],
			noMore: false,
		};
	}
	componentDidMount() {
		this.mounted = true;
		this.loadProductData();
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	loadMoreProduct = () => {
		this.loadProductData(this.state.productList.length, false);
	}
	loadProductData = async (offset = 0, reload = true) => {
		if(
			this.state.isLoading ||
			(!this.props.infiniteScroll && this.state.isLoaded)
			) {
			return;
		}
		if(this.mounted) {
			this.setState({isLoading: true});
		}
		let theParams = {
			limit: this.props.pageSize,
			offset: offset,
		};
		theParams = {...this.props.params, ...theParams};
		let productListApi = new ApiConnector({
			apiLink: this.props.apiLink,
			body: JSON.stringify(theParams),
			navigation: this.props.navigation,
			onSuccess: async jsonObj => {
				if (jsonObj.resultCode === 0) {
					let productList = jsonObj.data;
					let noMore = false;
					if (!reload) {
						if (offset !== this.state.productList.length) {
							if (this.mounted) {
								this.setState({isLoading: false});
							}
							return;
						}
						productList = this.state.productList.concat(productList);
					}
					if (
						productList.length >= jsonObj.count &&
						productList.length < this.state.pageSize
						) {
						noMore = true;
					}
					if (this.mounted) {
						this.setState({
							isLoading: false,
							isLoaded: true,
							productList: productList,
							noMore: noMore,
						});
					}
				}
			},
		});
		await productListApi.submit();
	}
	renderEachItem = ({item, index}) => {
		return (
			<ProductBox
				key={'product_' + item.goods_id}
				type={this.props.type}
				data={item}
				navigation={this.props.navigation}
			/>
		);
	}
	renderEmpty = () => {
		return <LoadingBox type={this.props.type} />;
	}
	renderedItem = ({item, index}) => {
		return (
			<ProductBox
				key={'product_' + item.goods_id}
				type={this.props.type}
				data={item}
				navigation={this.props.navigation}
			/>
		);
	}
	renderPropsHeader = () => {
		if (typeof this.props.headerLayout === 'function') {
			return this.props.headerLayout();
		} else {
			return (
				<FreshLayoutHelper
					layoutId={this.props.headerLayout}
					navigation={this.props.navigation}
					useScroll={false}
				/>
			);
		}
	}
	render() {
		return (
			<View>
				{
					this.props.type === 'horizontal' &&
					this.props.headerLayout &&
					this.renderPropsHeader()
				}
				{
					this.props.type === 'horizontal' &&
					<View
						style={[
							FreshStyles.padTop2,
							FreshStyles.flexRow,
							FreshStyles.padHor2,
							FreshStyles.contentSpaceBetween,
						]}>
						{
							this.props.title !== null &&
							this.props.title !== '' &&
							<Text style={[FreshStyles.titleText2]}>{this.props.title}</Text>
						}
						{
							this.props.useShowAll &&
							<TouchableOpacity
								onPress={() =>
									this.props.navigation.push('ProductList')
								}>
								<Text style={[FreshStyles.padBot2, FreshStyles.padHor2]}>View All</Text>
							</TouchableOpacity>
						}
					</View>
				}
				{
					this.props.type === 'horizontal' &&
					this.props.bannerId &&
					<Banner bannerId={this.props.bannerId} />
				}
				<FlatList
					style={FreshStyles.white}
					data={this.state.productList}
					{
						...(
							this.props.type === 'horizontal'
							?{horizontal: true}
							:{numColumns:this.props.type==='two-column' ? 2 : 3}
						)
					}
					keyExtractor={(item, goods_id) => 'product_' + goods_id}
					renderItem={this.renderEachItem}
					onRefresh={this.loadProductData}
					refreshing={this.state.isLoading}
					onEndReached={this.props.infiniteScroll?this.loadMoreProduct:()=>{}}
					ListEmptyComponent={this.renderEmpty}
					onEndReachedThreshold={0.1}
					ListHeaderComponent={() => {
						if (this.props.type !== 'horizontal') {
							return (
								<View style={[FreshStyles.flexColumn]}>
									{this.props.headerLayout && this.renderPropsHeader()}
									<View
										style={[
											FreshStyles.padTop2,
											FreshStyles.flexRow,
											FreshStyles.padHor2,
											FreshStyles.contentSpaceBetween,
										]}>
										{
											this.props.title !== null &&
											this.props.title !== '' &&
											<Text style={[FreshStyles.titleText2]}>
												{this.props.title}
											</Text>
										}
										{
											this.props.useShowAll &&
											<TouchableOpacity
												onPress={()=>{
													this.props.navigation.push('ProductList');
												}}>
												<Text
													style={[FreshStyles.padBot2, FreshStyles.padHor2]}>
													View All
												</Text>
											</TouchableOpacity>
										}
									</View>
									{
										this.props.bannerId &&
										<Banner bannerId={this.props.bannerId} />
									}
								</View>
							);
						} else {
							return null;
						}
					}}
				/>
			</View>
		);
	}
}
