import React, {Component} from 'react';
import {View, Image, Text, TouchableOpacity} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import ImageHelper from '../helper/ImageHelper';

export default class CategoryBox extends Component {
	static defaultProps = {
		category: null,
	};
	constructor(props) {
		super(props);
		this.state = {
			imageOkay: true,
		};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	imageError = e => {
		this.setState({
			imageOkay: false,
		});
	};
	render() {
		return (
			<TouchableOpacity
				onPress={() =>
					this.props.navigation.push('Category', {
						category_id: this.props.data.id,
					})
				}
				style={[FreshStyles.categoryBoxWrapper]}
				activeOpacity={0.8}>
				<Image
					style={[FreshStyles.width30Percent, FreshStyles.height30Percent]}
					source={
						this.state.imageOkay
							? ImageHelper.returnSource(this.props.data.image, 'category')
							: ImageHelper.returnSource('')
					}
					onError={this.imageError}
					resizeMode="contain"
				/>
				<Text style={[FreshStyles.textCenter, {height: 20}]}>
					{this.props.data.name}
				</Text>
			</TouchableOpacity>
		);
	}
}
