import React, {Component} from 'react';
import {View, Image, Text} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import Buttons from './Buttons';

export default class Menu extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		this.state = {};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	render() {
		return (
			<View style={[FreshStyles.cartContainer]}>
				<Text style={[FreshStyles.titleText, FreshStyles.textCenter]}>
					Empty Shopping Cart
				</Text>
				<Text style={[FreshStyles.margin5, FreshStyles.textCenter]}>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit.
				</Text>
				<Buttons
					title="Shopping Now"
					viewStyle={[FreshStyles.width60, FreshStyles.padTop2]}
					textStyle={[FreshStyles.greenButton, {color: 'white'}]}
					onPress={() => this.props.navigation.push('Home')}
				/>
			</View>
		);
	}
}
