import React, {PureComponent} from 'react';
import {View, TouchableOpacity, Image, Text} from 'react-native';

import FreshStyles, {screen30PercDown50Min50} from '../constants/FreshStyles';

export default class LoadingBox extends PureComponent {
	static defaultProps = {
		itemType: "products",
		layoutType: "horizontal",
	};
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	// textBoxes = () => {
	// 	<View style={[FreshStyles.padTop1, FreshStyles.flexRow]}>
	// 		<View style={[FreshStyles.flex1Column]}>
	// 			<View
	// 				style={{
	// 					height: 20,
	// 					backgroundColor: '#C5C5C5',
	// 					marginBottom: 4,
	// 				}}
	// 			/>
	// 			<View style={[FreshStyles.flexRow, FreshStyles.contentSpaceBetween]}>
	// 				<View
	// 					style={{
	// 						height: 15,
	// 						width: '75%',
	// 						backgroundColor: '#C5C5C5',
	// 					}}
	// 				/>
	// 				<View
	// 					style={{
	// 						height: 15,
	// 						width: '22%',
	// 						backgroundColor: '#C5C5C5',
	// 					}}
	// 				/>
	// 			</View>
	// 		</View>
	// 	</View>;
	// };
	// listLoading = () => {
	// 	let a = [];
	// 	for (let i = 1; i <= 12; i++) {
	// 		a.push(
	// 			<View key={'LoadingBox' + i}>
	// 				{this.props.type == 'horizontal' && (
	// 					<View style={[FreshStyles.flex1Row]}>
	// 						<TouchableOpacity
	// 							style={[FreshStyles.boxShadow, FreshStyles.width35Percent]}
	// 							activeOpacity={1}>
	// 							<View
	// 								style={[
	// 									FreshStyles.width30PercentDown50,
	// 									FreshStyles.height30PercentDown50,
	// 									{backgroundColor: '#C5C5C5', width: '100%'},
	// 								]}
	// 							/>
	// 							{this.textBoxes()}
	// 						</TouchableOpacity>
	// 					</View>
	// 				)}
	// 				{this.props.type == 'two-column' && (
	// 					<View style={[FreshStyles.flexRowWrap]}>
	// 						<TouchableOpacity
	// 							style={[FreshStyles.boxShadow, FreshStyles.productBoxWrapper2]}
	// 							activeOpacity={1}>
	// 							<View
	// 								style={[
	// 									FreshStyles.width100,
	// 									FreshStyles.height40Percent,
	// 									{backgroundColor: '#C5C5C5', width: '100%'},
	// 								]}
	// 							/>
	// 							{this.textBoxes()}
	// 						</TouchableOpacity>
	// 					</View>
	// 				)}
	// 				{this.props.type == 'three-column' && (
	// 					<View style={[FreshStyles.flexRowWrap]}>
	// 						<TouchableOpacity
	// 							style={[FreshStyles.boxShadow, FreshStyles.productBoxWrapper3]}
	// 							activeOpacity={1}>
	// 							<View
	// 								style={[
	// 									FreshStyles.width25Percent,
	// 									FreshStyles.height25Percent,
	// 									FreshStyles.justifyCenter,
	// 									{backgroundColor: '#C5C5C5', width: '100%'},
	// 								]}
	// 							/>
	// 							{this.textBoxes()}
	// 						</TouchableOpacity>
	// 					</View>
	// 				)}
	// 				{this.props.type == null && this.props.type == undefined && (
	// 					<TouchableOpacity style={[this.props.viewStyle]} activeOpacity={1}>
	// 						<View
	// 							style={[
	// 								this.props.imageStyle,
	// 								{backgroundColor: '#C5C5C5', width: '100%'},
	// 							]}
	// 						/>
	// 						<View style={[FreshStyles.padTop1, FreshStyles.flexRow]}>
	// 							<View style={[FreshStyles.flex1Column]}>
	// 								<View
	// 									style={{
	// 										height: 20,
	// 										backgroundColor: '#C5C5C5',
	// 										marginBottom: 4,
	// 									}}
	// 								/>
	// 								{!this.props.cart && (
	// 									<View
	// 										style={[
	// 											FreshStyles.flexRow,
	// 											FreshStyles.contentSpaceBetween,
	// 										]}>
	// 										<View
	// 											style={{
	// 												height: 15,
	// 												width: '75%',
	// 												backgroundColor: '#C5C5C5',
	// 											}}
	// 										/>
	// 										<View
	// 											style={{
	// 												height: 15,
	// 												width: '22%',
	// 												backgroundColor: '#C5C5C5',
	// 											}}
	// 										/>
	// 									</View>
	// 								)}
	// 							</View>
	// 						</View>
	// 					</TouchableOpacity>
	// 				)}
	// 			</View>,
	// 		);
	// 	}
	// 	return a;
	// };
	renderProductText = () => {
		return (
			<View style={[FreshStyles.padTop1, FreshStyles.flex1Column]} >
				<View style={[FreshStyles.LBTextHeight, FreshStyles.LBBackground, FreshStyles.LBMargin]} />
				<View style={[FreshStyles.flexRow, FreshStyles.justifyBetween]}>
					<View style={[FreshStyles.LBTextHeight, FreshStyles.LBTextW3, FreshStyles.LBBackground]} />
					<View style={[FreshStyles.LBTextHeight, FreshStyles.LBTextW1, FreshStyles.LBBackground]} />
				</View>
			</View>
		);
	}
	renderCategoryText = () => {
		return (
			<View style={[FreshStyles.LBTextHeight, FreshStyles.width100, FreshStyles.LBBackground, FreshStyles.margin5]} />
		);
	}
	renderProductBox = (idx) => {
		if(this.props.type === "two-column") {
			return (
				<View
					key={"Product_Loading_"+idx}
					style={[
						FreshStyles.boxShadow,
						FreshStyles.LBWidth50Perc,
					]}
					>
					<View
						style={[
							FreshStyles.LBWidth50PercInside,
							FreshStyles.LBHeight50PercInside,
							FreshStyles.LBBackground,
						]}
						/>
					{this.renderProductText()}
				</View>
			);
		} else if(this.props.type === "three-column") {
			return (
				<View
					key={"Product_Loading_"+idx}
					style={[FreshStyles.boxShadow, FreshStyles.productBoxWrapper2]}>
					{this.renderProductText()}
				</View>
			);
		} else {
			return (
				<View
					key={"Product_Loading_"+idx}
					style={[
						FreshStyles.boxShadow,
						FreshStyles.LBWidth35Perc,
					]}
					>
					<View
						style={[
							FreshStyles.LBWidth35PercInside,
							FreshStyles.LBHeight35PercInside,
							FreshStyles.LBBackground,
						]}
						/>
					{this.renderProductText()}
				</View>
			);
		}
	}
	renderCategoryBox = (idx) => {
		if(this.props.type === "four-column") {
			return (
				<View
					key={"Category_Loading_"+idx}
					style={[
						FreshStyles.padHor1,
						FreshStyles.alignCenter,
						FreshStyles.LBWidth4Col,
					]}
					>
					<View
						style={[
							FreshStyles.LBWidth4ColInside,
							FreshStyles.LBHeight4ColInside,
							FreshStyles.LBBackground,
						]}
						/>
					{this.renderCategoryText()}
				</View>
			);
		} else if(this.props.type === "three-column") {
			return (
				<View
					key={"Category_Loading_"+idx}
					style={[
						FreshStyles.padHor1,
						FreshStyles.alignCenter,
						FreshStyles.LBWidth3Col,
					]}
					>
					<View
						style={[
							FreshStyles.LBWidth3ColInside,
							FreshStyles.LBHeight3ColInside,
							FreshStyles.LBBackground,
						]}
						/>
					{this.renderCategoryText()}
				</View>
			);
		} else {
			return (
				<View key={"Category_Loading_"+idx}>
					<Text>Category Loading Box</Text>
				</View>
			);
		}
	}
	renderBox = (idx) => {
		if(this.props.itemType === "products") {
			return this.renderProductBox(idx);
		}
		if(this.props.itemType === "categories") {
			return this.renderCategoryBox(idx);
		}
		return (
			<View key={"LoadingBox_"+idx}>
				<Text>Loading...</Text>
			</View>
		);
	}
	getBoxArray = () => {
		let arr = [];
		let size = 12;
		if(this.props.itemType === "products") {
			if(this.props.type === "horizontal") {
				size = 5;
			} else if(this.props.type === "two-column") {
				size = 10;
			}
		} else if(this.props.itemType === "categories") {
			if(this.props.type === "four-column") {
				size = 8;
			} else if(this.props.type === "three-column") {
				size = 3;
			}
		}
		for(let i = 0;i<size;i++) {
			arr.push(i);
		}
		return arr;
	}
	getWrapperStyle = () => {
		if(this.props.type === "horizontal") {
			return [FreshStyles.flex1Row, ...(this.props.style?this.props.style:[])];
		} else {
			return [FreshStyles.flexRowWrap, ...(this.props.style?this.props.style:[])];
		}
	}
	render() {
		return (
			<View style={this.getWrapperStyle()}>
				{
					this.getBoxArray().map((a, b) => {
						return this.renderBox(a);
					})
				}
			</View>
		);
	}
}
