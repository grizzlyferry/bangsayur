import React, {PureComponent} from 'react';
import {View, Text} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import Banner from '../components/Banner';
import CategoryBox2 from '../components/CategoryBox2';
import FreshScreen from '../components/FreshScreen';
import LoadingBox from '../components/LoadingBox';
import ProductList from '../components/ProductList';
import ProductList2 from '../components/ProductList2';

import ApiConnector from '../helper/ApiConnector';

export default class CategoryList extends PureComponent {
	static defaultProps = {
		apiLink: '/fresh/Product/productList',
		infiniteScroll: true,
		pageSize: 15,
		type: 'three-column',
		rowCount: 2,
	};
	constructor(props) {
		super(props);
		this.state = {
			isLoading: false,
			isLoaded: false,
			categoryList: [],
			noMore: false,
		};
	}
	componentDidMount() {
		this.mounted = true;
		this.loadCategory();
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	loadCategory = async (offset = 0, reload = true) => {
		if(
			this.state.isLoading ||
			(!this.props.infiniteScroll && this.state.isLoaded)
			) {
			return;
		}
		if(this.mounted) {
			this.setState({isLoading: true});
		}
		let theParams = {
			limit: this.props.pageSize,
			offset: offset,
		};
		theParams = {...this.props.params, ...theParams};
		let categoryListApi = new ApiConnector({
			apiLink: this.props.apiLink,
			body: JSON.stringify(theParams),
			navigation: this.props.navigation,
			onSuccess: async jsonObj => {
				if(jsonObj.resultCode === 0) {
					let categoryList = jsonObj.data;
					let noMore = false;
					if(!reload) {
						if(offset !== this.state.categoryList.length) {
							if(this.mounted) {
								this.setState({isLoading: false});
							}
							return;
						}
						categoryList = this.state.categoryList.concat(categoryList);
					}
					if(
						categoryList.length >= jsonObj.count &&
						categoryList.length < this.state.pageSize
						) {
						noMore = true;
					}
					if(this.mounted) {
						this.setState({
							isLoading: false,
							isLoaded: true,
							categoryList: categoryList,
							noMore: noMore,
						});
					}
				}
			},
		});
		await categoryListApi.submit();
	// 	if (this.state.isLoaded) {
	// 		return;
	// 	}
	// 	if (this.mounted) {
	// 		this.setState({
	// 			isLoaded: true,
	// 		});
	// 	}
	// 	let categoryListBody = {
	// 		category_id: this.props.category_id,
	// 		level: this.props.level,
	// 		limit: 9,
	// 	};
	// 	let categoryListBodyJson = JSON.stringify(categoryListBody);
	// 	let categoryListApi = new ApiConnector({
	// 		apiLink: this.props.apiLink,
	// 		body: categoryListBodyJson,
	// 		navigation: this.props.navigation,
	// 		onSuccess: async jsonObj => {
	// 			if (jsonObj.resultCode === 0) {
	// 				let noMore = false;
	// 				let categoryList = this.state.categoryList.concat(jsonObj.data);
	// 				if (categoryList.length >= jsonObj.count) {
	// 					noMore = true;
	// 				}
	// 				if (this.mounted) {
	// 					this.setState({
	// 						isLoaded: true,
	// 						categoryList: categoryList,
	// 						noMore: noMore,
	// 						totalCount: jsonObj.count,
	// 					});
	// 				}
	// 			} else {
	// 				alert('Something Bad Happened!\nPlease Call Our Developer!');
	// 				console.log('Error Details: ', jsonObj);
	// 				if (this.mounted) {
	// 					this.setState({
	// 						isLoaded: false,
	// 					});
	// 				}
	// 			}
	// 		},
	// 		onError: error => {
	// 			console.log(error);
	// 			alert('Failed to Retrieve Data!\nPlease Call Our Developer!');
	// 		},
	// 	});
	// 	await categoryListApi.submit();
	}

	// threeColumn = () => {
	// 	let needShowAll = this.state.categoryList.length === 9;
	// 	let i = 0;
	// 	return (
	// 		<View>
	// 			{!this.state.isLoaded && (
	// 				<View style={[FreshStyles.CatWrapper, FreshStyles.padHor1]}>
	// 					<LoadingBox
	// 						style={[FreshStyles.flexRowWrap, FreshStyles.centerRow]}
	// 						count={8}
	// 						cart="false"
	// 						viewStyle={[FreshStyles.boxNoShadow, FreshStyles.width30Percent]}
	// 						imageStyle={[
	// 							FreshStyles.width30Percent,
	// 							FreshStyles.height25Percent,
	// 						]}
	// 					/>
	// 				</View>
	// 			)}
	// 			{this.state.isLoaded && this.state.totalCount > 0 && (
	// 				<View style={[FreshStyles.CatWrapper, FreshStyles.width100]}>
	// 					{this.state.categoryList.map((item, key) => {
	// 						i++;
	// 						if (i === 9 && needShowAll) {
	// 							return (
	// 								<CategoryBox2
	// 									key={'cat_' + key}
	// 									type={this.props.type}
	// 									navigation={this.props.navigation}
	// 									category_id={this.state.category_id}
	// 								/>
	// 							);
	// 						} else if (i !== 10) {
	// 							return (
	// 								<CategoryBox2
	// 									key={'cat_' + key}
	// 									type={this.props.type}
	// 									navigation={this.props.navigation}
	// 									data={item}
	// 								/>
	// 							);
	// 						}
	// 					})}
	// 				</View>
	// 			)}
	// 		</View>
	// 	);
	// };

	// fourColumn = () => {
	// 	let needShowAll = this.state.categoryList.length === 9;
	// 	let i = 0;
	// 	return (
	// 		<View>
	// 			{!this.state.isLoaded && (
	// 				<View style={[FreshStyles.CatWrapper, FreshStyles.padHor1]}>
	// 					<LoadingBox
	// 						style={[FreshStyles.flexRowWrap, FreshStyles.centerRow]}
	// 						count={8}
	// 						cart="false"
	// 						viewStyle={[FreshStyles.boxNoShadow, FreshStyles.width225Percent]}
	// 						imageStyle={[
	// 							FreshStyles.width225Percent,
	// 							FreshStyles.height20Percent,
	// 						]}
	// 					/>
	// 				</View>
	// 			)}
	// 			{this.state.isLoaded && this.state.totalCount > 0 && (
	// 				<View style={[FreshStyles.CatWrapper]}>
	// 					{this.state.categoryList.map((item, key) => {
	// 						i++;
	// 						if (i === 8 && needShowAll) {
	// 							return (
	// 								<CategoryBox2
	// 									key={'cat_' + key}
	// 									type={'four-column'}
	// 									navigation={this.props.navigation}
	// 									category_id={this.state.category_id}
	// 								/>
	// 							);
	// 						} else if (i !== 9) {
	// 							return (
	// 								<CategoryBox2
	// 									key={'cat_' + key}
	// 									type={'four-column'}
	// 									navigation={this.props.navigation}
	// 									data={item}
	// 								/>
	// 							);
	// 						}
	// 					})}
	// 				</View>
	// 			)}
	// 		</View>
	// 	);
	// };
	render() {
		return (
			<View style={FreshStyles.flexRowWrap}>
				{
					!this.state.isLoaded &&
					<LoadingBox itemType="categories" type={this.props.type} />
				}
				{
					this.state.isLoaded &&
					<View>
						<Text>is loaded!{JSON.stringify(this.state.categoryList)}</Text>
					</View>
				}
			</View>
		);
	// 			{this.props.type == 'three-column' && this.threeColumn}
	// 			{this.fourColumn()}
	}
}
