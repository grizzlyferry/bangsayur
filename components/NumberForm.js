import React, {Component} from 'react';
import {
	View,
	Text,
	TextInput,
	StyleSheet,
	TouchableOpacity,
} from 'react-native';

export default class NumberForm extends Component {
	static defaultProps = {
		disabled: false,
		placeholder: '',
	};
	constructor(props) {
		super(props);
		if (this.props.change !== undefined && this.props.change !== null) {
			this.changeValue = this.props.change;
		}
		this.state = {
			error: null,
		};
	}
	addValue = () => {
		this.changeValue(this.props.value + 1);
	};
	changeValue = targetValue => {
		console.log('ChangeValue Not Available' + targetValue);
	};
	minusValue = () => {
		this.changeValue(this.props.value - 1);
	};
	render() {
		return (
			<View style={[this.props.style, {flexDirection: 'row'}]}>
				<TouchableOpacity style={[Formstyles.button]} onPress={this.minusValue}>
					<Text style={[Formstyles.textCenter]}>-</Text>
				</TouchableOpacity>
				<TextInput
					style={[Formstyles.Form]}
					placeholder={this.props.placeholder}
					onChangeText={this.changeValue}
					value={'' + this.props.value}
					keyboardType="number-pad"
				/>
				<TouchableOpacity style={[Formstyles.button]} onPress={this.addValue}>
					<Text style={[Formstyles.textCenter]}>+</Text>
				</TouchableOpacity>
			</View>
		);
	}
}
const Formstyles = StyleSheet.create({
	Form: {
		borderWidth: 1,
		borderColor: 'grey',
		width: 40,
		height: 30,
		padding: 0,
		textAlign: 'center',
	},
	button: {
		alignItems: 'center',
		justifyContent: 'center',
		borderColor: 'grey',
		borderWidth: 1,
		height: 30,
		width: 30,
	},
	textCenter: {
		textAlign: 'center',
		justifyContent: 'center',
		color: 'grey',
		fontWeight: 'bold',
	},
	errorText: {
		color: 'red',
		fontSize: 11,
		alignSelf: 'stretch',
		paddingTop: 3,
		paddingVertical: 0,
		paddingLeft: 5,
		marginVertical: 0,
		padding: 0,
		margin: 0,
	},
});
