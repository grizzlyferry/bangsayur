import React, {Component} from 'react';
import {View, TouchableOpacity, Text} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faComment} from '@fortawesome/free-solid-svg-icons';

import AddProductToCart from './AddProductToCart';

export default class ProductBottomBar extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		this.state = {
			addToCart: false,
		};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}

	changeAppear = () => {
		if (this.mounted) {
			this.setState({
				addToCart: true,
			});
		}
	};
	changeDisappear = () => {
		if (this.mounted) {
			this.setState({
				addToCart: false,
			});
		}
	};
	closeOverlay = () => {
		this.setState({addToCart: false});
	};
	render() {
		let currRoute = this.props.navigation.state.routeName;
		// Start Variables
		let menuList = [
			{
				id: 1,
				icon: faComment,
				navigate: '',
				color: [FreshStyles.buttonOutline, FreshStyles.halfFlex],
				onClicking: null,
			},
			{
				id: 2,
				text: 'Buy now',
				navigate: 'Cart',
				color: FreshStyles.greenButtonOutline,
				textcolor: FreshStyles.navTextGreen,
				onClicking: () => this.props.navigation.push('Cart'),
			},
			{
				id: 3,
				text: 'Add to cart',
				navigate: 'Product',
				color: FreshStyles.greenButton,
				textcolor: FreshStyles.navTextWhite,
				onClicking: this.changeAppear,
			},
		];
		// End Variables
		return (
			<View style={[FreshStyles.bottomMenu, FreshStyles.pad2]}>
				<AddProductToCart
					visible={this.state.addToCart}
					requestClose={this.closeOverlay}
					productData={this.props.productData}
					isProductLoaded={this.props.isProductLoaded}
					navigation={this.props.navigation}
					closeCart={this.changeDisappear}
				/>
				{menuList.map((menuItem, key) => {
					return (
						<TouchableOpacity
							onPress={menuItem.onClicking}
							key={'Menu ' + menuItem.id}
							style={[
								FreshStyles.subBottomMenu,
								FreshStyles.centerRow,
								FreshStyles.marginHor2,
								menuItem.color,
							]}>
							{menuItem.icon !== null &&
								menuItem.icon !== '' &&
								menuItem.icon !== undefined && (
									<FontAwesomeIcon
										icon={menuItem.icon}
										style={
											currRoute == menuItem.navigate
												? FreshStyles.navOn
												: FreshStyles.navOff
										}
										size={20}
									/>
								)}
							{menuItem.text !== null &&
								menuItem.text !== '' &&
								menuItem.text !== undefined && (
									<Text style={menuItem.textcolor}>{menuItem.text}</Text>
								)}
						</TouchableOpacity>
					);
				})}
				{/* End icon & text */}
			</View>
		);
	}
}
