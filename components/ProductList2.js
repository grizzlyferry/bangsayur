import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faAngleRight} from '@fortawesome/free-solid-svg-icons';

import ProductBox from './ProductBox';
import LoadingBox from './LoadingBox';

import ApiConnector from '../helper/ApiConnector';

export default class ProductList2 extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		this.state = {
			isLoadingProduct: false,
			isLoaded: false,
			productList: [],
			noMore: true,
			totalCount: null,
		};
		this.loadProduct();
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}

	loadProduct = async (offset = 0) => {
		if (this.state.isLoadingProduct) {
			return;
		}
		if (this.mounted) {
			this.setState({
				isLoadingProduct: true,
			});
		}
		let productListBody = JSON.parse(this.props.bodyParam);
		let productListBodyJson = JSON.stringify(productListBody);
		let productListApi = new ApiConnector({
			apiLink: this.props.apiLink,
			body: productListBodyJson,
			navigation: this.props.navigation,
			onSuccess: async jsonObj => {
				if (jsonObj.resultCode === 0) {
					let noMore = false;
					let productList = this.state.productList.concat(jsonObj.data);
					if (productList.length >= jsonObj.count) {
						noMore = true;
					}
					if (this.mounted) {
						this.setState({
							isLoadingProduct: false,
							productList: productList,
							isLoaded: true,
							noMore: noMore,
							totalCount: jsonObj.count,
						});
					}
				} else {
					alert('Something Bad Happened!\nPlease Call Our Developer!');
					console.log('Error Details: ', jsonObj);
					if (this.mounted) {
						this.setState({
							isLoadingProduct: false,
							isLoaded: true,
						});
					}
				}
			},
			onError: error => {
				console.log(error);
				alert('Failed to Retrieve Data!\nPlease Call Our Developer!');
			},
		});
		await productListApi.submit();
	};
	render() {
		return (
			<View>
				{this.props.banner != null && (
					<View>
						<View
							style={[
								FreshStyles.white,
								FreshStyles.flexRow,
								FreshStyles.pad2,
								FreshStyles.margin15,
								FreshStyles.contentSpaceBetween,
							]}>
							{this.props.title != null && this.props.title != undefined && (
								<Text style={[FreshStyles.titleText, FreshStyles.textCenter]}>
									{this.props.title}
								</Text>
							)}
							<TouchableOpacity style={[FreshStyles.flexRow]}>
								<Text style={FreshStyles.padRight1}>More</Text>
								<FontAwesomeIcon
									style={FreshStyles.selfCenter}
									icon={faAngleRight}
								/>
							</TouchableOpacity>
						</View>
						<TouchableOpacity
							style={[FreshStyles.padHor3, FreshStyles.marginBot3]}
							activeOpacity={0.9}>
							<Image
								style={{width: '100%', height: 300}}
								source={require('../assets/nHBfsgAADAAAABEAGxmc-AADyMU.jpg')}
							/>
						</TouchableOpacity>
					</View>
				)}
				<View style={[FreshStyles.productList2Wrapper]}>
					{this.props.title == null &&
						this.props.title == undefined &&
						this.props.title == '' && (
							<Text style={[FreshStyles.titleText2]}>{this.props.title}</Text>
						)}
					<View style={[FreshStyles.flexRowWrap]}>
						{!this.state.isLoaded && (
							<LoadingBox
								style={[FreshStyles.flexRowWrap]}
								count={6}
								viewStyle={[
									FreshStyles.boxShadow,
									FreshStyles.productBoxWrapper2,
								]}
								imageStyle={[FreshStyles.width100, FreshStyles.height40Percent]}
							/>
						)}
						{this.state.isLoaded &&
							this.state.productList.map((product, key) => {
								return (
									<ProductBox
										style={[
											FreshStyles.productBoxWrapper2,
											FreshStyles.boxShadow,
										]}
										imageStyle={[
											FreshStyles.width100,
											FreshStyles.height40Percent,
										]}
										size={20}
										key={'product ' + product.goods_id}
										data={product}
										navigation={this.props.navigation}
									/>
								);
							})}
					</View>
				</View>
			</View>
		);
	}
}
