import React, {Component} from 'react';
import {View, TouchableOpacity, Image, Text, TextInput} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faMapMarkerAlt} from '@fortawesome/free-solid-svg-icons';
import {faSearch} from '@fortawesome/free-solid-svg-icons';

export default class TopBar extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		this.state = {};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	render() {
		return (
			<View style={FreshStyles.topBar}>
				<View style={[FreshStyles.flexRow, FreshStyles.justifyCenter]}>
					<View
						style={[
							FreshStyles.flexRow,
							FreshStyles.alignCenter,
							FreshStyles.width20,
						]}>
						<FontAwesomeIcon
							icon={faMapMarkerAlt}
							size={25}
							style={FreshStyles.textHalfOpacity}
						/>
						<Text style={FreshStyles.text11}> Batam</Text>
					</View>
					<View style={FreshStyles.searchBar}>
						<TouchableOpacity>
							<Image
								source={require('../assets/app.png')}
								style={{
									height: 30,
									width: 30,
									margin: 5,
								}}
							/>
						</TouchableOpacity>
						<View style={[FreshStyles.flexRow, {flex: 6}]}>
							<TextInput style={FreshStyles.pad0} placeholder="Search..." />
						</View>
						<TouchableOpacity style={[FreshStyles.flex1]}>
							<FontAwesomeIcon icon={faSearch} size={18} />
						</TouchableOpacity>
					</View>
				</View>
			</View>
		);
	}
}
