import React, {Component} from 'react';
import {
	Dimensions,
	Alert,
	findNodeHandle,
	UIManager,
	View,
	Text,
	Platform,
	TouchableOpacity,
	Image,
	Picker,
	Modal,
	ScrollView,
	StyleSheet,
} from 'react-native';

const screenHeight = Dimensions.get('window').height;

export default class DialogDropdown extends Component {
	static defaultProps = {
		disabled: false,
	};
	constructor(props) {
		super(props);
		var selectedVal = null;
		if (
			this.props.selectedValue !== undefined &&
			this.props.selectedValue !== null
		) {
			selectedVal = this.props.selectedValue;
		}
		if (this.props.change !== undefined && this.props.change !== null) {
			this.changeCallback = this.props.change;
		}
		this.state = {
			selectedValue: selectedVal,
			modalVisible: false,
			iOSCurrentKey: 0,
			androidCurrentKey: 0,
			androidModalX: 0,
			androidModalY: 0,
			androidModalWidth: 0,
			androidModalMaxHeight: 0,
		};
	}
	changeValue = async (itemValue, itemIndex) => {
		var selectedObj = null;
		for (var i in this.props.data) {
			if (this.props.data[i].key == itemValue) {
				selectedObj = this.props.data[i];
				break;
			}
		}
		if (selectedObj !== null) {
			this.setState({
				selectedValue: selectedObj.key,
			});
			this.changeCallback(selectedObj);
		} else {
			Alert.alert(
				'Error!\nError Code: -16',
				'Something wrong!\nThis dialog changed to an unknown value!\nPlease blame our developer with the error code above!\nThank you!',
			);
		}
	};
	changeCallback = async selectedObj => {
		Alert.alert(
			'Error!\nError Code: -15',
			'Something wrong!\nThis dialog dropdown is not set to do anything when it change!\nPlease blame our developer with the error code above!\nThank you!',
		);
	};
	iosClick = async () => {
		this.setState({
			modalVisible: true,
			iOSCurrentKey: this.state.selectedValue,
		});
	};
	androidClick = async picker => {
		let theFaker = this._androidFakePicker;
		let theFakerHandle = findNodeHandle(theFaker);
		UIManager.measure(theFakerHandle, (a, b, c, d, e, f) => {
			this.setState({
				androidModalX: e,
				androidModalY: f + d,
				androidModalWidth: c,
				androidModalMaxHeight: screenHeight - (f + d + 30),
			});
		});
		this.setState({
			modalVisible: true,
			androidCurrentKey: this.state.selectedValue,
		});
	};
	closeModal = async () => {
		this.setState({
			modalVisible: false,
		});
	};
	androidPickOkay = async selectedValue => {
		this.setState({
			selectedValue: selectedValue,
			modalVisible: false,
		});
		var selectedObj = null;
		for (var i in this.props.data) {
			if (this.props.data[i].key == selectedValue) {
				selectedObj = this.props.data[i];
				break;
			}
		}
		this.changeCallback(selectedObj);
	};
	iOSPickOkay = async () => {
		this.setState({
			selectedValue: this.state.iOSCurrentKey,
			modalVisible: false,
		});
		var selectedObj = null;
		for (var i in this.props.data) {
			if (this.props.data[i].key == this.state.iOSCurrentKey) {
				selectedObj = this.props.data[i];
				break;
			}
		}
		this.changeCallback(selectedObj);
	};
	disabled = async () => {};
	render() {
		var currentText = '';
		let {selectedValue} = this.state;
		if (selectedValue === null) {
			currentText = this.props.data[0].value;
		} else {
			let found = false;
			for (var i in this.props.data) {
				if (this.props.data[i].key === selectedValue) {
					currentText = this.props.data[i].value;
					found = true;
					break;
				}
			}
			if (!found) {
				currentText = this.props.data[0].value;
				setTimeout(() => {
					this.changeValue(1, null);
				}, 0);
			}
		}
		return (
			<View style={[styles.mainContainer, this.props.containerStyle]}>
				{(this.props.data === undefined ||
					this.props.data === null ||
					this.props.data.length === 0) && (
					<Text>Error! No Data Available!</Text>
				)}
				{this.props.data !== undefined &&
					this.props.data !== null &&
					this.props.data.length !== 0 && (
						<View style={styles.pickerHolder}>
							{this.props.title !== undefined && this.props.title !== '' && (
								<Text style={styles.titleText}>{this.props.title}</Text>
							)}
							<TouchableOpacity
								activeOpacity={1.0}
								style={[
									styles.dropdownBox,
									styles.fakePickerHolder,
									this.props.disabled ? styles.disabled : null,
								]}
								ref={comp => (this._androidFakePicker = comp)}
								onPress={
									this.props.disabled
										? this.disabled
										: Platform.OS === 'ios'
										? this.iosClick
										: this.androidClick
								}>
								<Text style={styles.dropdownText}>{currentText}</Text>
								<Image
									source={require('../assets/up.png')}
									style={styles.dropdownChevron}
								/>
							</TouchableOpacity>
							{this.props.errorMsg && (
								<Text style={styles.errorText}>{this.props.errorMsg}</Text>
							)}
						</View>
					)}
				<Modal
					animationType="fade"
					transparent={true}
					visible={this.state.modalVisible}
					onRequestClose={this.closeModal}>
					<TouchableOpacity
						style={styles.modalRootTouchable}
						onPress={this.closeModal}>
						{Platform.OS === 'android' && (
							<ScrollView
								containerStyle={styles.androidModalContainerStyle}
								style={[
									styles.androidModalView,
									{
										top: this.state.androidModalY,
										left: this.state.androidModalX,
										width: this.state.androidModalWidth,
										maxHeight: this.state.androidModalMaxHeight,
									},
								]}>
								{this.props.data.map((item, key) => {
									return (
										<TouchableOpacity
											activeOpacity={1.0}
											onPress={this.androidPickOkay.bind(this, item.key)}
											key={'androidItem' + key}>
											<Text
												style={[
													styles.androidModalItem,
													key !== this.props.data.length - 1
														? styles.androidModalItemBorder
														: styles.androidModalItemLast,
													item.key === selectedValue ||
													(selectedValue === null && key === 0)
														? styles.androidModalItemSelected
														: null,
												]}>
												{item.value}
											</Text>
										</TouchableOpacity>
									);
								})}
							</ScrollView>
						)}
						{Platform.OS === 'ios' && (
							<View style={styles.modalView}>
								{this.props.modalTitle !== undefined && (
									<Text style={[styles.modalTitle, styles.modalItems]}>
										{this.props.modalTitle}
									</Text>
								)}
								{this.props.modalTitle === undefined &&
									this.props.title !== undefined && (
										<Text style={[styles.modalTitle, styles.modalItems]}>
											{this.props.title}
										</Text>
									)}
								<Picker
									selectedValue={this.state.iOSCurrentKey}
									style={[styles.iOSPicker, styles.modalItems]}
									mode="dropdown"
									onValueChange={async (itemValue, itemIndex) => {
										this.setState({
											iOSCurrentKey: itemValue,
										});
									}}>
									{this.props.data.map((item, key) => {
										return (
											<Picker.Item
												color="#707070"
												label={item.value}
												value={item.key}
												key={key}
											/>
										);
									})}
								</Picker>
								<View style={styles.modalButtonHolder}>
									<TouchableOpacity
										activeOpacity={1.0}
										style={styles.modalCancelButton}
										onPress={this.closeModal}>
										<Text style={styles.modalCancelText}>Cancel</Text>
									</TouchableOpacity>
									<TouchableOpacity
										activeOpacity={1.0}
										style={styles.modalOkayButton}
										onPress={this.iOSPickOkay}>
										<Text style={styles.modalOkayText}>Okay</Text>
									</TouchableOpacity>
								</View>
							</View>
						)}
					</TouchableOpacity>
				</Modal>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	mainContainer: {
		alignSelf: 'stretch',
		alignItems: 'flex-start',
		marginVertical: 5,
		marginHorizontal: 20,
	},
	pickerHolder: {
		alignSelf: 'stretch',
	},
	titleText: {
		textAlign: 'left',
		color: '#707070',
		alignSelf: 'stretch',
		marginBottom: 5,
	},
	dropdownBox: {
		alignSelf: 'stretch',
		borderWidth: 1,
		borderColor: '#DCDCDC',
		borderRadius: 10,
	},
	fakePickerHolder: {
		flexDirection: 'row',
	},
	disabled: {
		backgroundColor: '#BBBBBB',
	},
	dropdownText: {
		flex: 1,
		padding: 10,
	},
	dropdownChevron: {
		height: 15,
		width: 15,
		marginVertical: 5,
		marginRight: 5,
		alignSelf: 'center',
	},
	errorText: {
		textAlign: 'center',
		color: 'red',
		alignSelf: 'stretch',
	},
	modalRootTouchable: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 2,
		shadowRadius: 2,
		shadowColor: 'rgba(0, 0, 0, 1.0)',
		shadowOpacity: 0.5,
		shadowOffset: {width: 0, height: 2},
		...Platform.select({
			ios: {
				backgroundColor: 'rgba(0, 0, 0, 0.4)',
			},
			android: {
				backgroundColor: 'rgba(0, 0, 0, 0.1)',
			},
		}),
	},
	androidModalContainerStyle: {
		alignItems: 'stretch',
		flexDirection: 'column',
	},
	androidModalView: {
		minHeight: 30,
		minWidth: 50,
		position: 'absolute',
		backgroundColor: 'white',
		padding: 10,
		borderWidth: 1,
		borderColor: 'white',
		borderRadius: 10,
	},
	androidModalItem: {
		fontSize: 14,
		paddingHorizontal: 10,
		paddingVertical: 10,
	},
	androidModalItemBorder: {
		borderBottomWidth: 1,
		borderColor: '#BBBBBB',
	},
	androidModalItemLast: {
		marginBottom: 20,
		// paddingBottom: 20,
	},
	androidModalItemSelected: {
		backgroundColor: '#BBBBBB',
	},
	modalView: {
		backgroundColor: 'white',
		alignSelf: 'stretch',
		alignItems: 'stretch',
		borderRadius: 10,
		marginHorizontal: 30,
		paddingHorizontal: 0,
		paddingTop: 15,
	},
	modalTitle: {
		fontSize: 16,
		fontWeight: 'bold',
		color: 'black',
		textAlign: 'center',
	},
	modalItems: {
		marginHorizontal: 10,
	},
	iOSPicker: {
		alignSelf: 'stretch',
	},
	modalButtonHolder: {
		alignSelf: 'stretch',
		borderBottomLeftRadius: 10,
		borderBottomRightRadius: 10,
		padding: 0,
		margin: 0,
		flexDirection: 'row',
	},
	modalCancelButton: {
		backgroundColor: 'white',
		borderBottomLeftRadius: 10,
		width: '50%',
		margin: 0,
		paddingVertical: 10,
		alignItems: 'center',
		justifyContent: 'center',
		shadowRadius: 2,
		shadowColor: 'rgba(0, 0, 0, 1.0)',
		shadowOpacity: 0.5,
		shadowOffset: {width: 0, height: 0},
	},
	modalCancelText: {
		color: 'red',
	},
	modalOkayButton: {
		backgroundColor: 'white',
		borderBottomRightRadius: 10,
		width: '50%',
		margin: 0,
		paddingVertical: 10,
		alignItems: 'center',
		justifyContent: 'center',
		shadowRadius: 2,
		shadowColor: 'rgba(0, 0, 0, 1.0)',
		shadowOpacity: 0.5,
		shadowOffset: {width: 0, height: 0},
	},
	modalOkayText: {
		fontWeight: 'bold',
	},
});
