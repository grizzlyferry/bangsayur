import React, {Component} from 'react';
import {View, TouchableOpacity, Text} from 'react-native';

import FreshStyles, {textColor2} from '../constants/FreshStyles';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faDotCircle} from '@fortawesome/free-regular-svg-icons';
import {faCircle} from '@fortawesome/free-regular-svg-icons';

export default class RadioButton extends Component {
	static defaultProps = {
		text: 'Radio Button text',
	};
	constructor(props) {
		super(props);
		this.state = {
			// selected: false,
		};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	onSelect = () => {
		// if (this.state.selected) {
		// 	this.setState({
		// 		selected: false,
		// 	});
		// }
		this.props.onChange(this.props.myValue);
	};
	render() {
		let radioStyle = {...FreshStyles.marginRight3, ...FreshStyles.fgColor4};
		return (
			<View style={this.props.viewStyle}>
				<TouchableOpacity
					style={[
						FreshStyles.flexRow,
						FreshStyles.alignCenter,
						this.props.style ? this.props.style : FreshStyles.pad3,
					]}
					activeOpacity={1}
					onPress={this.onSelect}>
					<FontAwesomeIcon
						icon={
							this.props.value !== this.props.myValue ? faCircle : faDotCircle
						}
						style={radioStyle}
					/>
					<Text style={[this.props.textStyle]}>{this.props.text}</Text>
				</TouchableOpacity>
			</View>
		);
	}
}
