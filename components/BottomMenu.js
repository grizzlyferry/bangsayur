import React, {Component} from 'react';
import {View, TouchableOpacity, Text} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faHome} from '@fortawesome/free-solid-svg-icons';
import {faShoppingCart} from '@fortawesome/free-solid-svg-icons';
import {faThLarge} from '@fortawesome/free-solid-svg-icons';
import {faUser} from '@fortawesome/free-solid-svg-icons';

export default class BottomMenu extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		this.state = {};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	render() {
		let currRoute = this.props.navigation.state.routeName;
		// Start Variables
		let menuList = [
			{
				id: 1,
				text: 'Home',
				icon: faHome,
				navigate: 'Home',
			},
			{
				id: 2,
				text: 'Category List',
				icon: faThLarge,
				navigate: 'CategoryList',
				navigateParam: {level: 1},
			},
			{
				id: 3,
				text: 'Cart',
				icon: faShoppingCart,
				navigate: 'Cart',
			},
			{
				id: 4,
				text: 'Me',
				icon: faUser,
				navigate: 'Profile',
			},
		];
		// End Variables
		return (
			<View style={[FreshStyles.bottomMenu]}>
				{/* Start icon & text */}
				{menuList.map((menuItem, key) => {
					return (
						<TouchableOpacity
							onPress={() =>
								this.props.navigation.push(
									menuItem.navigate,
									menuItem.navigateParam,
								)
							}
							key={'Menu ' + menuItem.id}
							style={[FreshStyles.subBottomMenu, FreshStyles.marginTop1]}>
							<FontAwesomeIcon
								icon={menuItem.icon}
								style={
									currRoute == menuItem.navigate
										? FreshStyles.navOn
										: FreshStyles.navOff
								}
								size={20}
							/>
							<Text
								style={
									currRoute == menuItem.navigate
										? FreshStyles.navTextOn
										: FreshStyles.navTextOff
								}>
								{menuItem.text}
							</Text>
						</TouchableOpacity>
					);
				})}
				{/* End icon & text */}
			</View>
		);
	}
}
