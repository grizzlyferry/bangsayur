import React, {Component} from 'react';
import {View, TouchableOpacity, Image, Text} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import Menu from '../components/Menu';

export default class MenuList extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		this.state = {};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	render() {
		return (
			<View
				style={[FreshStyles.centerColumn, FreshStyles.pad3, FreshStyles.white]}>
				<View style={[FreshStyles.menuListWrapper]}>
					<Menu />
					<Menu />
					<Menu />
					<Menu />
					<Menu />
					<Menu />
				</View>
			</View>
		);
	}
}
