import React, {Component} from 'react';
import {View, TouchableOpacity, Image, Text} from 'react-native';

import {currencySymbol} from '../constants/Config';
import FreshStyles from '../constants/FreshStyles';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCartPlus} from '@fortawesome/free-solid-svg-icons';

import ImageHelper from '../helper/ImageHelper';

export default class ProductBox2 extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		this.state = {
			imageOkay: true,
		};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	imageError = e => {
		this.setState({
			imageOkay: false,
		});
	};
	render() {
		return (
			<TouchableOpacity
				style={[FreshStyles.productBoxWrapper2, FreshStyles.boxShadow]}
				onPress={() =>
					this.props.navigation.push('Product', {
						product_id: this.props.data.goods_id,
					})
				}
				activeOpacity={0.9}>
				<Image
					style={[FreshStyles.width100, FreshStyles.height40Percent]}
					source={
						this.state.imageOkay
							? ImageHelper.returnSource(this.props.data.original_img)
							: ImageHelper.returnSource('')
					}
					onError={this.imageError}
					resizeMode="cover"
				/>
				<View style={[FreshStyles.padTop1, FreshStyles.flexRow]}>
					<View style={[FreshStyles.flex1Column]}>
						<Text style={[FreshStyles.text14, {height: 20}]}>
							{this.props.data.goods_name}
						</Text>
						<Text style={FreshStyles.textBold}>
							{currencySymbol}
							{this.props.data.market_price}
						</Text>
					</View>
					<TouchableOpacity style={[FreshStyles.selfEnd]}>
						<FontAwesomeIcon
							icon={faCartPlus}
							size={24}
							style={FreshStyles.navGreen}
						/>
					</TouchableOpacity>
				</View>
			</TouchableOpacity>
		);
	}
}
