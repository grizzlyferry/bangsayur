import React, {Component} from 'react';
import {View, Text, ScrollView} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import ProductBox from './ProductBox';
import LoadingBox from './LoadingBox';

import ApiConnector from '../helper/ApiConnector';

export default class ProductList2 extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		this.state = {
			isLoadingProduct: false,
			isLoaded: false,
			productList: [],
			noMore: true,
			totalCount: null,
		};
		this.loadProduct();
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}

	loadProduct = async (offset = 0) => {
		if (this.state.isLoadingProduct) {
			return;
		}
		if (this.mounted) {
			this.setState({
				isLoadingProduct: true,
			});
		}
		let productListBody = {
			old_id: 'aaa',
		};
		let productListBodyJson = JSON.stringify(productListBody);
		let productListApi = new ApiConnector({
			apiLink: '/index.php?m=fresh&c=Product&a=productList',
			body: productListBodyJson,
			navigation: this.props.navigation,
			onSuccess: async jsonObj => {
				if (jsonObj.resultCode === 0) {
					let noMore = false;
					let productList = this.state.productList.concat(jsonObj.data);
					if (productList.length >= jsonObj.count) {
						noMore = true;
					}
					if (this.mounted) {
						this.setState({
							isLoadingProduct: false,
							isLoaded: true,
							productList: productList,
							noMore: noMore,
							totalCount: jsonObj.count,
						});
					}
				} else {
					alert('Something Bad Happened!\nPlease Call Our Developer!');
					console.log('Error Details: ', jsonObj);
					if (this.mounted) {
						this.setState({
							isLoadingProduct: false,
						});
					}
				}
			},
			onError: error => {
				console.log(error);
				alert('Failed to Retrieve Data!\nPlease Call Our Developer!');
			},
		});
		await productListApi.submit();
	};
	render() {
		return (
			<View style={[FreshStyles.productList2Wrapper, FreshStyles.white]}>
				{this.props.title != null && this.props.title != undefined && (
					<Text style={[FreshStyles.titleText, FreshStyles.padHor1]}>
						{this.props.title}
					</Text>
				)}
				<View style={[FreshStyles.flexRowWrap, FreshStyles.selfCenter]}>
					{!this.state.isLoaded && (
						<LoadingBox
							style={[FreshStyles.flexRowWrap]}
							count={6}
							viewStyle={[
								FreshStyles.boxShadow,
								FreshStyles.productBoxWrapper3,
							]}
							imageStyle={[
								FreshStyles.width25Percent,
								FreshStyles.height25Percent,
								FreshStyles.justifyCenter,
							]}
						/>
					)}
					{this.state.isLoaded &&
						this.state.productList.map((product, key) => {
							return (
								<ProductBox
									style={[
										FreshStyles.boxShadow,
										FreshStyles.productBoxWrapper3,
									]}
									imageStyle={[
										FreshStyles.width25Percent,
										FreshStyles.height25Percent,
										FreshStyles.justifyCenter,
									]}
									size={20}
									key={'product ' + product.goods_id}
									data={product}
									navigation={this.props.navigation}
								/>
							);
						})}
				</View>
			</View>

			// End Padding
		);
	}
}
