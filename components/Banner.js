import React, {Component} from 'react';
import {View, Image, ScrollView, TouchableOpacity} from 'react-native';

import FreshStyles, {screenWidth} from '../constants/FreshStyles';

export default class Banner extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		this.state = {};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}

	render() {
		return (
			<ScrollView
				horizontal={true}
				style={[FreshStyles.marginBot1]}
				snapToInterval={screenWidth}
				showsHorizontalScrollIndicator={false}>
				<TouchableOpacity activeOpacity={1}>
					<Image
						style={{width: screenWidth, height: 200}}
						source={require('../assets/ali-tarhini-1272704-unsplash.jpg')}
					/>
				</TouchableOpacity>
				<TouchableOpacity activeOpacity={1}>
					<Image
						style={{width: screenWidth, height: 200}}
						source={require('../assets/nasi-goreng-seafood.jpg')}
					/>
				</TouchableOpacity>
			</ScrollView>
		);
	}
}
