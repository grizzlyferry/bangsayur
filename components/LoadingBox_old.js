import React, {Component} from 'react';
import {View, TouchableOpacity, Image, Text} from 'react-native';
import FreshStyles, {screen30PercDown50Min50} from '../constants/FreshStyles';
export default class LoadingBox extends Component {
  static defaultProps = {
    product: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      imageOkay: true,
    };
  }
  componentDidMount() {
    this.mounted = true;
  }
  componentWillUnmount() {
    this.mounted = false;
  }
  imageError = e => {
    this.setState({
      imageOkay: false,
    });
  };
  listLoading = () => {
    let a = [];
    for (let i = 1; i <= this.props.count; i++) {
      a.push(
        <TouchableOpacity
          key={i}
          style={[this.props.viewStyle]}
          activeOpacity={1}>
          <View
            style={[
              this.props.imageStyle,
              {backgroundColor: '#C5C5C5', width: '100%'},
            ]}
          />
          <View style={[FreshStyles.padTop1, FreshStyles.flexRow]}>
            <View style={[FreshStyles.flex1Column]}>
              <View
                style={{
                  height: 20,
                  backgroundColor: '#C5C5C5',
                  marginBottom: 4,
                }}
              />
              {!this.props.cart && (
                <View
                  style={[
                    FreshStyles.flexRow,
                    FreshStyles.contentSpaceBetween,
                  ]}>
                  <View
                    style={{
                      height: 15,
                      width: '75%',
                      backgroundColor: '#C5C5C5',
                    }}
                  />
                  <View
                    style={{
                      height: 15,
                      width: '22%',
                      backgroundColor: '#C5C5C5',
                    }}
                  />
                </View>
              )}
            </View>
          </View>
        </TouchableOpacity>,
      );
    }
    return a;
  };
  render() {
    return (
      <View style={this.props.style}>
        {this.listLoading().map((a, b) => {
          return a;
        })}
      </View>
    );
  }
}
