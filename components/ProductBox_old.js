import React, {Component} from 'react';
import {View, TouchableOpacity, Image, Text} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCartPlus} from '@fortawesome/free-solid-svg-icons';
import FreshStyles from '../constants/FreshStyles';
import {currencySymbol} from '../constants/Config';
import ImageHelper from '../helper/ImageHelper';
export default class ProductBox extends Component {
  static defaultProps = {
    style: FreshStyles.boxShadow,
    imageStyle: [
      FreshStyles.width30PercentDown50,
      FreshStyles.height30PercentDown50,
    ],
    size: 15,
  };
  constructor(props) {
    super(props);
    this.state = {
      imageOkay: true,
    };
  }
  componentDidMount() {
    this.mounted = true;
  }
  componentWillUnmount() {
    this.mounted = false;
  }
  imageError = e => {
    this.setState({
      imageOkay: false,
    });
  };
  render() {
    return (
      <TouchableOpacity
        style={[this.props.style, {position: 'relative'}]}
        onPress={() =>
          this.props.navigation.push('Product', {
            product_id: this.props.data.goods_id,
          })
        }
        activeOpacity={0.9}>
        <Image
          style={this.props.imageStyle}
          source={
            this.state.imageOkay
              ? ImageHelper.returnSource(this.props.data.original_img)
              : ImageHelper.returnSource('')
          }
          onError={this.imageError}
          resizeMode="cover"
        />
        <View style={[FreshStyles.pillsContainer]}>
          {this.props.data.is_hot == 1 && (
            <Text style={[FreshStyles.pillsRed]}>HOT</Text>
          )}
        </View>
        <View style={[FreshStyles.padTop1, FreshStyles.flexRow]}>
          <View style={[FreshStyles.flex1Column, FreshStyles.width20Percent]}>
            <Text style={[FreshStyles.text13, {height: 20}]}>
              {this.props.data.goods_name}
            </Text>
            <Text style={FreshStyles.textBold}>
              {currencySymbol}
              {this.props.data.market_price}
            </Text>
          </View>
          <TouchableOpacity style={[FreshStyles.selfEnd]}>
            <FontAwesomeIcon
              icon={faCartPlus}
              size={this.props.size}
              style={FreshStyles.navGreen}
            />
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    );
  }
}
