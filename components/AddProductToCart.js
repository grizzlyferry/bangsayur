import React, {Component} from 'react';
import {Modal, TouchableOpacity, View, Text, Image} from 'react-native';

import {currencySymbol} from '../constants/Config';
import FreshStyles from '../constants/FreshStyles';
import ImageHelper from '../helper/ImageHelper';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faTimes} from '@fortawesome/free-solid-svg-icons';

import Buttons from '../components/Buttons';
import NumberForm from '../components/NumberForm';

export default class AddProductToCart extends Component {
	static defaultProps = {
		visible: false,
		imageOkay: true,
	};
	constructor(props) {
		super(props);
		if (this.props.requestClose) {
			this.requestClose = this.props.requestClose;
		}
		this.state = {
			quantity: 1,
		};
		if (
			this.props.closeCart !== undefined &&
			this.props.closeCart !== null &&
			this.props.closeCart !== ''
		) {
			this.closeCart = this.props.closeCart;
		}
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	changeQuantity = targetValue => {
		this.setState({
			quantity: targetValue,
		});
	};
	closeCart = () => {};
	imageError = e => {
		this.setState({
			imageOkay: false,
		});
	};
	requestClose = () => {
		console.log('Fake request close function!');
	};
	render() {
		return (
			<Modal
				animationType="none"
				transparent={true}
				visible={this.props.visible}
				onRequestClose={this.requestClose}>
				<TouchableOpacity
					activeOpacity={1}
					onPress={this.requestClose}
					style={{
						flex: 1,
						flexDirection: 'column',
						alignItems: 'stretch',
						justifyContent: 'flex-end',
						backgroundColor: 'rgba(0, 0, 0, 0.4)',
					}}>
					<TouchableOpacity
						activeOpacity={1}
						onPress={() => {}}
						style={[FreshStyles.white, FreshStyles.flexColumn]}>
						{this.props.isProductLoaded && (
							<View>
								<View style={[FreshStyles.flexRow, FreshStyles.pad2]}>
									<Image
										style={[
											FreshStyles.width30Percent,
											FreshStyles.height30Percent,
											FreshStyles.cornerRad5,
										]}
										source={
											this.state.imageOkay
												? ImageHelper.returnSource(
														this.props.productData.original_img,
													)
												: ImageHelper.returnSource('')
										}
										onError={this.imageError}
										resizeMode="cover"
									/>
									<View style={[FreshStyles.cartText]}>
										<Text style={[FreshStyles.textBold, FreshStyles.padVer1]}>
											{this.props.productData.goods_name}
										</Text>
										<Text style={[FreshStyles.text12]}>
											1 pcs ({this.props.productData.weight})
										</Text>
										<Text style={[FreshStyles.text12]}>
											Stock : {this.props.productData.store_count}
										</Text>
										<View style={[FreshStyles.flex1]} />
										<Text style={[FreshStyles.textBold]}>
											{currencySymbol}
											{this.props.productData.market_price}
										</Text>
									</View>
									<TouchableOpacity
										style={[FreshStyles.alignEnd]}
										onPress={this.closeCart}>
										<FontAwesomeIcon icon={faTimes} size={20} />
									</TouchableOpacity>
								</View>

								{/* <View style={[FreshStyles.variation]}>
									<Text>Variation :</Text>
									<View style={[FreshStyles.flexRowWrap]}>
										<Buttons
											title="Blue"
											style={[FreshStyles.pad1]}
											textStyle={[FreshStyles.padHor4, FreshStyles.text12]}
										/>
									</View>
								</View> */}
								<View style={[FreshStyles.variation]}>
									<View
										style={[
											FreshStyles.flexRow,
											FreshStyles.contentSpaceBetween,
											FreshStyles.padBot3,
										]}>
										<Text>Jumlah :</Text>
										<NumberForm
											value={this.state.quantity}
											change={this.changeQuantity}
										/>
									</View>
									<View style={[FreshStyles.centerRow, FreshStyles.margin5]}>
										<TouchableOpacity
											style={[
												FreshStyles.greenButtonOutline,
												FreshStyles.alignCenter,
												FreshStyles.pad2,
												FreshStyles.flex1,
											]}>
											<Text style={[FreshStyles.navTextGreen]}>Buy now</Text>
										</TouchableOpacity>
										<View style={[FreshStyles.marginHor2]} />
										<TouchableOpacity
											style={[
												FreshStyles.greenButton,
												FreshStyles.alignCenter,
												FreshStyles.pad2,
												FreshStyles.flex1,
											]}>
											<Text style={[FreshStyles.navTextWhite]}>
												Add to Cart
											</Text>
										</TouchableOpacity>
									</View>
								</View>
							</View>
						)}
					</TouchableOpacity>
				</TouchableOpacity>
			</Modal>
		);
	}
}
