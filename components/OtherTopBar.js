import React, {Component} from 'react';
import {View, TouchableOpacity, Text} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faPlus, faArrowLeft} from '@fortawesome/free-solid-svg-icons';

export default class OtherTopBar extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		this.state = {};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	render() {
		let currRoute = this.props.navigation.state.routeName;
		return (
			<View style={[FreshStyles.topBar]}>
				<TouchableOpacity
					style={[FreshStyles.padRight2]}
					onPress={() => this.props.navigation.pop()}>
					<FontAwesomeIcon icon={faArrowLeft} color={'white'} size={20} />
				</TouchableOpacity>
				<Text style={[FreshStyles.topBarText, FreshStyles.flex1]}>
					{this.props.title}
				</Text>
				{currRoute == 'AddressList' && (
					<View style={{flexDirection: 'row-reverse', flex: 1}}>
						<TouchableOpacity
							onPress={() => this.props.navigation.push('NewAddress')}>
							<FontAwesomeIcon icon={faPlus} color={'white'} size={20} />
						</TouchableOpacity>
					</View>
				)}
			</View>
		);
	}
}
