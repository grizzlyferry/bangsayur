import React, {Component} from 'react';
import {
	SafeAreaView,
	KeyboardAvoidingView,
	ScrollView,
	View,
} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import BottomMenu from './BottomMenu';
import OtherTopBar from './OtherTopBar';
import ProductBottomBar from './ProductBottomBar';
import ProductTopBar from './ProductTopBar';
import TopBar from './TopBar';

export default class FreshScreen extends Component {
	static defaultProps = {
		useScroll: true,
	};
	constructor(props) {
		super(props);
		this.state = {
			currentPage: 0,
		};
		// if(this.props.changeHomePage !== undefined && this.props.changeHomePage !== null) {
		// 	this.postChangePage = this.props.changeHomePage;
		// }
	}
	render() {
		
		// let currRoute = this.props.navigation;
		// let currRoute = this.props.navigation.state.routeName; -> Ferry : ini yang sementara ini bikin error

		let currRoute = (state: NavigationState) => (
			state.index !== undefined ? getCurrentRoute(state.routes[state.index]) : state.routeName
		);
		return (
			<SafeAreaView style={[FreshStyles.flex1]}>
				<KeyboardAvoidingView
					style={[
						FreshStyles.flexColumn,
						FreshStyles.justifyChildCenter,
						FreshStyles.flexGrow,
					]}>
					{currRoute == 'Home' && <TopBar navigation={this.props.navigation} />}
					{currRoute != 'Home' &&
						currRoute != 'Product' &&
						currRoute != 'Category' &&
						currRoute != 'ProductList' &&
						this.state.level === null && (
							<OtherTopBar
								navigation={this.props.navigation}
								title={currRoute}
							/>
						)}
					{(currRoute == 'Product' ||
						currRoute == 'Category' ||
						currRoute == 'ProductList' ||
						(currRoute == 'CategoryList' && this.state.level !== null)) && (
						<ProductTopBar
							navigation={this.props.navigation}
							title={currRoute}
						/>
					)}
					{this.props.useScroll && (
						<ScrollView
							keyboardShouldPersistTaps="always"
							style={[FreshStyles.flex1]}
							contentContainerStyle={[FreshStyles.flexGrow]}
							showsVerticalScrollIndicator={false}>
							{this.props.children}
						</ScrollView>
					)}
					{!this.props.useScroll && (
						<View style={FreshStyles.flex1}>{this.props.children}</View>
					)}
					{currRoute == 'Product' && (
						<ProductBottomBar
							data={this.props.data}
							productData={this.props.productData}
							isProductLoaded={this.props.isProductLoaded}
							navigation={this.props.navigation}
						/>
					)}
					{(currRoute == 'Home' ||
						currRoute == 'Category' ||
						currRoute == 'Cart' ||
						currRoute == 'Login' ||
						currRoute == 'Profile') && (
						<BottomMenu navigation={this.props.navigation} />
					)}

					{this.props.bottom}
				</KeyboardAvoidingView>
			</SafeAreaView>
		);
	}
}
