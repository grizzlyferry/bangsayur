import React, {Component} from 'react';
import {View, TouchableOpacity, Image, Text} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faTh} from '@fortawesome/free-solid-svg-icons';

import ImageHelper from '../helper/ImageHelper';

export default class CategoryBox extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		this.state = {
			imageOkay: true,
		};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	imageError = e => {
		this.setState({
			imageOkay: false,
		});
	};
	render() {
		return (
			<View style={[FreshStyles.padHor1]}>
				{this.props.type == 'three-column' && (
					<View>
						{this.props.data && (
							<TouchableOpacity
								onPress={() =>
									this.props.navigation.push('ProductList', {
										bodyParam: JSON.stringify({
											category_id: this.props.data.id,
										}),
									})
								}
								style={[FreshStyles.width30Percent]}>
								<Image
									style={[
										FreshStyles.width30Percent,
										FreshStyles.height25Percent,
									]}
									source={
										this.state.imageOkay
											? ImageHelper.returnSource(
													this.props.data.image,
													'category',
												)
											: ImageHelper.returnSource('')
									}
									onError={this.imageError}
									resizeMode="contain"
								/>
								<Text style={[FreshStyles.textCenter, {height: 20}]}>
									{this.props.data.mobile_name}
								</Text>
							</TouchableOpacity>
						)}
						{!this.props.data && (
							<TouchableOpacity
								onPress={() =>
									this.props.navigation.push('CategoryList', {
										level: '2',
										category_id: this.props.category_id,
									})
								}>
								<View
									style={[
										FreshStyles.width225Percent,
										FreshStyles.alignCenter,
									]}>
									<FontAwesomeIcon
										icon={faTh}
										size={45}
										style={{color: 'grey'}}
									/>
									<Text style={[FreshStyles.textCenter]}>Show All</Text>
								</View>
							</TouchableOpacity>
						)}
					</View>
				)}
				{this.props.type == 'four-column' && (
					<View>
						{this.props.data && (
							<TouchableOpacity
								onPress={() =>
									this.props.navigation.push('ProductList', {
										bodyParam: JSON.stringify({
											category_id: this.props.data.id,
										}),
									})
								}
								style={[FreshStyles.width20Percent, FreshStyles.marginHor1]}>
								<Image
									style={[
										FreshStyles.width225Percent,
										FreshStyles.height25Percent,
									]}
									source={
										this.state.imageOkay
											? ImageHelper.returnSource(
													this.props.data.image,
													'category',
												)
											: ImageHelper.returnSource('')
									}
									onError={this.imageError}
									resizeMode="contain"
								/>
								<Text style={[FreshStyles.textCenter, {height: 20}]}>
									{this.props.data.mobile_name}
								</Text>
							</TouchableOpacity>
						)}
						{!this.props.data && (
							<TouchableOpacity
								onPress={() =>
									this.props.navigation.push('CategoryList', {
										level: '2',
										category_id: this.props.category_id,
									})
								}>
								<View
									style={[
										FreshStyles.width225Percent,
										FreshStyles.alignCenter,
									]}>
									<FontAwesomeIcon
										icon={faTh}
										size={45}
										style={{color: 'grey'}}
									/>
									<Text style={[FreshStyles.textCenter]}>Show All</Text>
								</View>
							</TouchableOpacity>
						)}
					</View>
				)}
			</View>
		);
	}
}
