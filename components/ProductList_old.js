import React, {Component} from 'react';
import {View, Text, ScrollView, TouchableOpacity, Image} from 'react-native';
import ApiConnector from '../helper/ApiConnector';
import ProductBox from './ProductBox';
import FreshStyles from '../constants/FreshStyles';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faAngleRight} from '@fortawesome/free-solid-svg-icons';
import LoadingBox from './LoadingBox';

export default class ProductList extends Component {
  static defaultProps = {};
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      productList: [],
      noMore: true,
      totalCount: null,
    };
    this.loadProduct();
  }
  componentDidMount() {
    this.mounted = true;
  }
  componentWillUnmount() {
    this.mounted = false;
  }

  loadProduct = async () => {
    if (this.state.isLoaded) {
      return;
    }
    let productListBody = JSON.parse(this.props.bodyParam);
    let productListBodyJson = JSON.stringify(productListBody);
    let productListApi = new ApiConnector({
      apiLink: this.props.apiLink,
      body: productListBodyJson,
      navigation: this.props.navigation,
      onSuccess: async jsonObj => {
        if (jsonObj.resultCode === 0) {
          let noMore = false;
          let productList = this.state.productList.concat(jsonObj.data);
          if (productList.length >= jsonObj.count) {
            noMore = true;
          }
          if (this.mounted) {
            this.setState({
              productList: productList,
              noMore: noMore,
              totalCount: jsonObj.count,
              isLoaded: true,
            });
          }
        } else {
          alert('Something Bad Happened!\nPlease Call Our Developer!');
          console.log('Error Details: ', jsonObj);
          if (this.mounted) {
            this.setState({
              isLoaded: true,
            });
          }
        }
      },
      onError: error => {
        console.log(error);
        alert('Failed to Retrieve Data!\nPlease Call Our Developer!');
      },
    });
    await productListApi.submit();
  };

  horizontal = () => {
    return (
      <View style={[FreshStyles.white, FreshStyles.pad2]}>
        <View style={[FreshStyles.flexRow, FreshStyles.contentSpaceBetween]}>
          <Text style={[FreshStyles.titleText2]}>{this.props.title}</Text>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.push('ProductList', {
                bodyParam: this.props.bodyParam,
              })
            }>
            <Text style={[FreshStyles.padBot2]}>View All</Text>
          </TouchableOpacity>
        </View>
        {!this.state.isLoaded && (
          <LoadingBox
            style={[FreshStyles.flex1Row]}
            count={3}
            viewStyle={[FreshStyles.boxShadow, FreshStyles.width35Percent]}
            imageStyle={[
              FreshStyles.width30PercentDown50,
              FreshStyles.height30PercentDown50,
            ]}
          />
        )}
        {this.state.isLoaded && (
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            {this.state.productList.map((product, key) => {
              return (
                <ProductBox
                  key={'product ' + product.goods_id}
                  data={product}
                  navigation={this.props.navigation}
                />
              );
            })}
          </ScrollView>
        )}
      </View>
    );
  };

  twoColumn = () => {
    return (
      <View>
        {this.props.title != null && (
          <View>
            <View
              style={[
                FreshStyles.white,
                FreshStyles.flexRow,
                FreshStyles.pad2,
                FreshStyles.margin15,
                FreshStyles.contentSpaceBetween,
              ]}>
              {this.props.title != null && this.props.title != undefined && (
                <Text style={[FreshStyles.titleText, FreshStyles.textCenter]}>
                  {this.props.title}
                </Text>
              )}
              <TouchableOpacity style={[FreshStyles.flexRow]}>
                <Text style={FreshStyles.padRight1}>More</Text>
                <FontAwesomeIcon
                  style={FreshStyles.selfCenter}
                  icon={faAngleRight}
                />
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={[FreshStyles.padHor3, FreshStyles.marginBot3]}
              activeOpacity={0.9}>
              <Image
                style={{width: '100%', height: 300}}
                source={require('../assets/nHBfsgAADAAAABEAGxmc-AADyMU.jpg')}
              />
            </TouchableOpacity>
          </View>
        )}
        <View style={[FreshStyles.productList2Wrapper]}>
          {this.props.title2 != null &&
            this.props.title2 != undefined &&
            this.props.title2 != '' && (
              <Text style={[FreshStyles.titleText2]}>{this.props.title2}</Text>
            )}
          <View style={[FreshStyles.flexRowWrap]}>
            {!this.state.isLoaded && (
              <LoadingBox
                style={[FreshStyles.flexRowWrap]}
                count={6}
                viewStyle={[
                  FreshStyles.boxShadow,
                  FreshStyles.productBoxWrapper2,
                ]}
                imageStyle={[FreshStyles.width100, FreshStyles.height40Percent]}
              />
            )}
            {this.state.isLoaded &&
              this.state.productList.map((product, key) => {
                return (
                  <ProductBox
                    style={[
                      FreshStyles.productBoxWrapper2,
                      FreshStyles.boxShadow,
                    ]}
                    imageStyle={[
                      FreshStyles.width100,
                      FreshStyles.height40Percent,
                    ]}
                    size={20}
                    key={'product ' + product.goods_id}
                    data={product}
                    navigation={this.props.navigation}
                  />
                );
              })}
          </View>
        </View>
      </View>
    );
  };

  threeColumn = () => {
    return (
      <View style={[FreshStyles.productList2Wrapper, FreshStyles.white]}>
        {this.props.title != null && this.props.title != undefined && (
          <Text style={[FreshStyles.titleText, FreshStyles.padHor1]}>
            {this.props.title}
          </Text>
        )}
        <View style={[FreshStyles.flexRowWrap, FreshStyles.selfCenter]}>
          {!this.state.isLoaded && (
            <LoadingBox
              style={[FreshStyles.flexRowWrap]}
              count={6}
              viewStyle={[
                FreshStyles.boxShadow,
                FreshStyles.productBoxWrapper3,
              ]}
              imageStyle={[
                FreshStyles.width25Percent,
                FreshStyles.height25Percent,
                FreshStyles.justifyCenter,
              ]}
            />
          )}
          {this.state.isLoaded &&
            this.state.productList.map((product, key) => {
              return (
                <ProductBox
                  style={[
                    FreshStyles.boxShadow,
                    FreshStyles.productBoxWrapper3,
                  ]}
                  imageStyle={[
                    FreshStyles.width25Percent,
                    FreshStyles.height25Percent,
                    FreshStyles.justifyCenter,
                  ]}
                  size={20}
                  key={'product ' + product.goods_id}
                  data={product}
                  navigation={this.props.navigation}
                />
              );
            })}
        </View>
      </View>
    );
  };
  render() {
    return (
      <View>
        {this.props.type == 'horizontal' && this.horizontal()}
        {this.props.type == 'two-column' && this.twoColumn()}
        {this.props.type == 'three-column' && this.threeColumn()}
      </View>
    );
  }
}
