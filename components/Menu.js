import React, {Component} from 'react';
import {View, TouchableOpacity, Image, Text} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

export default class Menu extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		this.state = {};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	render() {
		return (
			<View style={[FreshStyles.menuWrapper]}>
				<TouchableOpacity style={FreshStyles.width100} activeOpacity={0.5}>
					<Image
						style={{width: '100%', height: 50}}
						source={require('../assets/app.png')}
					/>
				</TouchableOpacity>
				<Text style={FreshStyles.textCenter}>Menu</Text>
			</View>
		);
	}
}
