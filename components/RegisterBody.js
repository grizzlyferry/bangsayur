import React, {Component} from 'react';
import {View, Text, Keyboard} from 'react-native';
import moment from 'moment';

import FreshStyles from '../constants/FreshStyles';
import {province, city} from '../constants/Area';

import Buttons from '../components/Buttons';
import DialogDropdown from '../components/DialogDropdown';
import FormInput from '../components/FormInput';
import RadioButton from '../components/RadioButton';

import ApiConnector from '../helper/ApiConnector';

export default class RegisterBody extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		let yearsList = [];
		let thisYear = parseInt(moment().format('YYYY'));
		for (let i = thisYear; i >= 1920; i--) {
			yearsList.push({key: i, value: i});
		}

		let monthList = [];
		for (let i = 0; i < 12; i++) {
			monthList.push({
				key: i,
				value: moment()
					.month(i)
					.format('MMMM'),
			});
		}
		this.state = {
			yearsList: yearsList,
			monthList: monthList,
			selectedYear: thisYear,
			selectedMonth: 0,
			selectedDay: 1,
			email: null,
			mobile: null,
			password: null,
			password2: null,
			nickname: null,
			gender: null,
			// province: null,
			// city: null,
			// district: null,
			error: {},
			isSubmitting: false,
		};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}

	submitRegister = async () => {
		let email = this.state.email;
		let mobile = this.state.mobile;
		let password = this.state.password;
		let password2 = this.state.password2;
		let nickname = this.state.nickname;
		let gender = this.state.gender;
		// let province = this.state.province;
		// let district = this.state.district;
		let birthday = moment(
			this.state.selectedYear +
				'-' +
				(this.state.selectedMonth + 1) +
				'-' +
				this.state.selectedDay,
			'YYYY-M-D',
		).format('X');
		this.setState({
			isSubmitting: true,
		});
		let registerObj = {
			scene: 1,
			email: email,
			mobile: mobile,
			password: password,
			password2: password2,
			nickname: nickname,
			sex: gender,
			// province: province,
			// district: district,
			birthday: birthday,
		};
		let registerBody = JSON.stringify(registerObj);
		let registerApi = new ApiConnector({
			apiLink: '/fresh/Login/register',
			body: registerBody,
			navigation: this.props.navigation,
			onSuccess: async jsonObj => {
				if (jsonObj.resultCode !== 0) {
					if (this.mounted) {
						this.setState({error: jsonObj.data});
					}
				} else if (jsonObj.resultCode === 0) {
					//SetSession
					console.log('JSON OBJ', jsonObj.data);
					let valData = {user_id: jsonObj.data.user_id, scene: 5};
					this.props.navigation.push('Verification', {
						valParam: JSON.stringify(valData),
						email: JSON.stringify(jsonObj.data.email),
						mobile: JSON.stringify(jsonObj.data.mobile)
					});
				} else {
					// this.handleRegisterError(
					//   'Error!\nError Code: -8',
					//   'Something wrong registering your account!\nIf this problem persist, please contact our developers with the error code above!\nThank you!',
					// );
				}
				if (this.mounted) {
					this.setState({isSubmitting: false});
				}
			},
			onError: error => {
				// this.handleRegisterError(
				//   'No Internet Connection!',
				//   'You need to be connected to the Internet to do that!',
				// );
				if (this.mounted) {
					this.setState({isSubmitting: false});
				}
			},
		});
		Keyboard.dismiss();
		await registerApi.submit();
	};

	onChange = gender => {
		this.setState({
			gender: gender,
		});
	};
	onChangeDays = days => {
		let oldError = this.state.error;
		delete oldError.birthday;
		this.setState({
			error: oldError,
			selectedDay: days.key,
		});
	};
	onChangeMonths = months => {
		let {selectedDay} = this.state;
		let test = moment(
			this.state.selectedYear + '-' + (months.key + 1) + '-' + selectedDay,
			'YYYY-M-D',
			true,
		);
		if (!test.isValid()) {
			selectedDay = 1;
		}
		let oldError = this.state.error;
		delete oldError.birthday;
		this.setState({
			error: oldError,
			selectedMonth: months.key,
			selectedDay: selectedDay,
		});
	};
	onChangeYears = years => {
		let {selectedDay} = this.state;
		let test = moment(
			years.key + '-' + (this.state.selectedMonth + 1) + '-' + selectedDay,
			'YYYY-M-D',
			true,
		);
		if (!test.isValid()) {
			selectedDay = 1;
		}
		let oldError = this.state.error;
		delete oldError.birthday;
		this.setState({
			error: oldError,
			selectedYear: years.key,
			selectedDay: selectedDay,
		});
	};
	onChangeProvince = province => {
		this.setState({
			province: province,
		});
	};
	onChangeCity = city => {
		this.setState({
			city: city,
		});
	};
	onChangeDistrict = district => {
		this.setState({
			district: district,
		});
	};

	changeEmail = email => {
		if (this.mounted) {
			this.setState({email: email});
		}
		if (email !== undefined && email !== '') {
			let oldError = this.state.error;
			delete oldError.email;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};
	changeMobile = mobile => {
		if (this.mounted) {
			this.setState({mobile: mobile});
		}
		if (mobile !== undefined && mobile !== '') {
			let oldError = this.state.error;
			delete oldError.mobile;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};
	changePassword = password => {
		if (this.mounted) {
			this.setState({password: password});
		}
		if (password !== undefined && password !== '') {
			let oldError = this.state.error;
			delete oldError.password;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};
	changePassword2 = password2 => {
		if (this.mounted) {
			this.setState({password2: password2});
		}
		if (password2 !== undefined && password2 !== '') {
			let oldError = this.state.error;
			delete oldError.password2;
			if (this.mounted) {
				this.setState({error: oldError});
			}
		}
	};
	render() {
		let {mobile, email, password, nickname, password2} = this.state;
		let days = [];
		for (let i = 1; i <= 31; i++) {
			if (i <= 28) {
				days.push({key: i, value: i});
			} else {
				let test = moment(
					this.state.selectedYear +
						'-' +
						(this.state.selectedMonth + 1) +
						'-' +
						i,
					'YYYY-M-D',
					true,
				);
				if (test.isValid()) {
					days.push({key: i, value: i});
				}
			}
		}
		// let district = [
		//   {key: 'Abenaho, Yalimo', value: 'Abenaho'},
		//   {key: 'Abepura, Jayapura', value: 'Abepura'},
		// ];
		return (
			<View style={[FreshStyles.flex1,FreshStyles.marginTop2]}>
				<View
					style={[
						FreshStyles.selfCenter,
						FreshStyles.width80,
						FreshStyles.marginVer7,
					]}>
					<FormInput
						placeholder="Email Address"
						onChangeText={this.changeEmail}
						error={this.state.error.email}
						value={this.state.email}
						keyboardType="email-address"
					/>
					<FormInput
						placeholder="Mobile Number"
						onChangeText={this.changeMobile}
						error={this.state.error.mobile}
						keyboardType="numeric"
						value={this.state.mobile}
					/>
					<FormInput
						placeholder="Password"
						onChangeText={this.changePassword}
						error={this.state.error.password}
						secureTextEntry={true}
						value={this.state.password}
					/>
					<FormInput
						placeholder="Confirm Password"
						onChangeText={this.changePassword2}
						error={this.state.error.password2}
						secureTextEntry={true}
						value={this.state.password2}
					/>
					<FormInput placeholder="Nickname" error={this.state.error.nickname} />

					<Text
						style={[
							FreshStyles.text12,
							FreshStyles.padTop2,
							{color: 'rgba(0, 0, 0, .38)'},
						]}>
						Gender
					</Text>
					<View style={[FreshStyles.flexRow, FreshStyles.contentSpaceBetween]}>
						<RadioButton
							text="Male"
							value={this.state.gender}
							onChange={this.onChange}
							myValue="1"
						/>
						<RadioButton
							text="Female"
							value={this.state.gender}
							onChange={this.onChange}
							myValue="2"
						/>
					</View>
					{this.state.error.sex != undefined &&
						this.state.error.sex != null &&
						this.state.error.sex != '' && <Text>{this.state.error.sex}</Text>}
					<View style={[FreshStyles.flexRow, FreshStyles.padVer2]}>
						{/* YEARS */}
						<DialogDropdown
							title="Years"
							data={this.state.yearsList}
							selectedValue={this.state.day}
							change={this.onChangeYears}
							containerStyle={[FreshStyles.margin0, FreshStyles.Flex08]}
						/>
						{/* MONTHS */}
						<DialogDropdown
							title="Months"
							data={this.state.monthList}
							selectedValue={this.state.day}
							change={this.onChangeMonths}
							containerStyle={[
								FreshStyles.margin0,
								FreshStyles.flex1,
								FreshStyles.padHor2,
							]}
						/>
						{/* DAYS */}
						<DialogDropdown
							title="Days"
							data={days}
							selectedValue={this.state.selectedDay}
							change={this.onChangeDays}
							containerStyle={[FreshStyles.margin0, FreshStyles.halfFlex]}
						/>
					</View>
					{this.state.error.birthday != undefined &&
						this.state.error.birthday != null &&
						this.state.error.birthday != '' && (
							<Text style={[FreshStyles.errorText]}>
								{this.state.error.birthday}
							</Text>
						)}
					{/* <DialogDropdown
						title="Province"
						data={province}
						selectedValue={this.state.province}
						change={this.onChangeProvince}
						containerStyle={[FreshStyles.margin0, FreshStyles.padVer2]}
					/>
					<DialogDropdown
						title="City"
						data={city}
						selectedValue={this.state.city}
						change={this.onChangeCity}
						containerStyle={[FreshStyles.margin0, FreshStyles.padVer2]}
					/>
					<DialogDropdown
						title="District"
						data={district}
						selectedValue={this.state.district}
						change={this.onChangeDistrict}
						containerStyle={[FreshStyles.margin0, FreshStyles.padVer2]}
					/> */}
					<Buttons
						disabled={this.state.isSubmitting ? true : false}
						title={this.state.isSubmitting ? 'Submitting' : 'Register'}
						onPress={() => this.submitRegister()}
					/>
				</View>
			</View>
		);
	}
}
