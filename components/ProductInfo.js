import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';

import {currencySymbol} from '../constants/Config';
import FreshStyles from '../constants/FreshStyles';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faHeart} from '@fortawesome/free-solid-svg-icons';

import ImageHelper from '../helper/ImageHelper';

export default class ProductInfo extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		this.state = {
			liked: false,
			imageOkay: true,
		};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	changeFontHeart = () => {
		if (this.mounted) {
			if (!this.state.liked) {
				this.setState({
					liked: true,
				});
			} else {
				this.setState({
					liked: false,
				});
			}
		}
	};
	imageError = e => {
		this.setState({
			imageOkay: false,
		});
	};

	render() {
		// Remove product Info Image when not found
		return (
			<View>
				<TouchableOpacity activeOpacity={0.9}>
					<Image
						style={{width: '100%', height: 300}}
						source={
							this.state.imageOkay
								? ImageHelper.returnSource(this.props.data.original_img)
								: ImageHelper.returnSource('')
						}
						onError={this.imageError}
						resizeMode="contain"
					/>
				</TouchableOpacity>
				<View
					style={[FreshStyles.productInfoWrapper, FreshStyles.centerColumn]}>
					<View style={[FreshStyles.flexRow, FreshStyles.contentSpaceBetween]}>
						<Text style={[FreshStyles.titleText, FreshStyles.width80]}>
							{this.props.data.goods_name}
						</Text>
						<TouchableOpacity
							style={FreshStyles.buttonLike}
							onPress={() => this.changeFontHeart()}>
							<FontAwesomeIcon
								icon={faHeart}
								style={
									this.state.liked ? FreshStyles.navLiked : FreshStyles.nav0
								}
							/>
						</TouchableOpacity>
					</View>
					<Text style={[FreshStyles.detail]}>
						1 pcs ({this.props.data.weight}g)
					</Text>
					<Text style={[FreshStyles.description]}>
						{this.props.data.goods_remark}
					</Text>
					<Text style={[FreshStyles.titleText]}>
						{currencySymbol} {this.props.data.market_price}
					</Text>
				</View>
			</View>

			// End Padding
		);
	}
}
