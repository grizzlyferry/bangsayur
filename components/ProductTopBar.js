import React, {Component} from 'react';
import {View, TouchableOpacity, Text, TextInput} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faAngleLeft, faShoppingCart} from '@fortawesome/free-solid-svg-icons';
import {faFilter} from '@fortawesome/free-solid-svg-icons';
import {faSearch} from '@fortawesome/free-solid-svg-icons';

export default class ProductTopBar extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		this.state = {};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	render() {
		let currRoute = this.props.navigation.state.routeName;
		return (
			<View style={[FreshStyles.flexColumn, FreshStyles.shadows]}>
				<View style={[FreshStyles.topBar2]}>
					<TouchableOpacity
						style={[FreshStyles.padRight2]}
						onPress={() => this.props.navigation.pop()}>
						<FontAwesomeIcon icon={faAngleLeft} size={27} />
					</TouchableOpacity>
					<View style={FreshStyles.searchBar}>
						<View style={[FreshStyles.flexRow, FreshStyles.alignCenter]}>
							{(currRoute == 'Category' || currRoute == 'ProductList') && (
								<TouchableOpacity style={[FreshStyles.padLeft2]}>
									<FontAwesomeIcon icon={faSearch} size={18} />
								</TouchableOpacity>
							)}
							<TextInput
								style={[
									FreshStyles.pad1,
									FreshStyles.padHor2,
									FreshStyles.flex3,
								]}
								placeholder="Search..."
							/>
						</View>
					</View>
					{currRoute != 'ProductList' && (
						<TouchableOpacity
							style={[FreshStyles.alignCenter, FreshStyles.marginHor5]}>
							<FontAwesomeIcon
								icon={faShoppingCart}
								style={FreshStyles.navOff}
								size={20}
							/>
						</TouchableOpacity>
					)}
				</View>
				{currRoute == 'ProductList' && <SubTopBar />}
			</View>
		);
	}
}
