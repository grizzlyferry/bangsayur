import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text} from 'react-native';

import FreshStyles from '../constants/FreshStyles';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faSort} from '@fortawesome/free-solid-svg-icons';

import Buttons from './Buttons';

export default class SubTopBar extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		this.state = {};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	render() {
		return (
			<View style={[FreshStyles.white]}>
				<View style={[FreshStyles.flexRow, FreshStyles.contentSpaceBetween]}>
					<TouchableOpacity style={[FreshStyles.subCategoryOff]}>
						<Text style={[FreshStyles.navTextOn]}>Newest</Text>
					</TouchableOpacity>
					<TouchableOpacity style={[FreshStyles.subCategoryOn]}>
						<Text style={[FreshStyles.navTextOn]}>Hot</Text>
					</TouchableOpacity>
					<TouchableOpacity style={[FreshStyles.subCategoryOff]}>
						<Text style={[FreshStyles.navTextOn]}>Promo</Text>
					</TouchableOpacity>
					<TouchableOpacity style={[FreshStyles.subCategoryOff]}>
						<Text style={[FreshStyles.navTextOn]}>
							Price <FontAwesomeIcon icon={faSort} size={12} />
						</Text>
					</TouchableOpacity>
				</View>
				<ScrollView
					horizontal={true}
					showsHorizontalScrollIndicator={false}
					style={[FreshStyles.pad2]}>
					<Buttons
						title="Kol"
						textStyle={[FreshStyles.cornerRad5, FreshStyles.padHor5]}
						style={[FreshStyles.padHor1]}
					/>
					<Buttons
						title="Kol"
						textStyle={[FreshStyles.cornerRad5, FreshStyles.padHor5]}
						style={[FreshStyles.padHor1]}
					/>
				</ScrollView>
			</View>
		);
	}
}
