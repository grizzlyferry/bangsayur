import React, {PureComponent} from 'react';
import {View, TouchableOpacity, Image, Text, ScrollView} from 'react-native';

import {currencySymbol} from '../constants/Config';
import FreshStyles from '../constants/FreshStyles';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCartPlus} from '@fortawesome/free-solid-svg-icons';

import ImageHelper from '../helper/ImageHelper';
export default class ProductBox extends PureComponent {
	static defaultProps = {};
	constructor(props) {
		super(props);
		this.state = {
			imageOkay: true,
		};
	}
	componentDidMount() {
		this.mounted = true;
	}
	componentWillUnmount() {
		this.mounted = false;
	}
	imageError = e => {
		this.setState({
			imageOkay: false,
		});
	};

	dataText = () => {
		return (
			<View style={[FreshStyles.flex1Column, FreshStyles.width20Percent]}>
				<Text style={[FreshStyles.text13, {height: 20}]}>
					{this.props.data.goods_name}
				</Text>
				<Text style={FreshStyles.textBold}>
					{currencySymbol}
					{this.props.data.market_price}
				</Text>
			</View>
		);
	};
	fontAwesome = () => {
		return (
			<TouchableOpacity style={[FreshStyles.selfEnd]}>
				<FontAwesomeIcon
					icon={faCartPlus}
					size={20}
					style={FreshStyles.navGreen}
				/>
			</TouchableOpacity>
		);
	};
	pills = () => {
		return (
			<View style={[FreshStyles.pillsContainer]}>
				{this.props.data.is_hot == 1 && (
					<Text style={[FreshStyles.pillsRed]}>HOT</Text>
				)}
			</View>
		);
	};
	render() {
		return (
			<View>
				{this.props.type == 'horizontal' && (
					<TouchableOpacity
						style={[
							FreshStyles.boxShadow,
							FreshStyles.width35Percent,
							{position: 'relative'},
						]}
						onPress={() =>
							this.props.navigation.push('Product', {
								product_id: this.props.data.goods_id,
							})
						}
						activeOpacity={0.9}>
						<Image
							style={[
								FreshStyles.width30PercentDown50,
								FreshStyles.height30PercentDown50,
							]}
							source={
								this.state.imageOkay
									? ImageHelper.returnSource(this.props.data.original_img)
									: ImageHelper.returnSource('')
							}
							onError={this.imageError}
							resizeMode="cover"
						/>
						{this.pills()}
						<View style={[FreshStyles.padTop1, FreshStyles.flexRow]}>
							{this.dataText()}
							{this.fontAwesome()}
						</View>
					</TouchableOpacity>
				)}
				{this.props.type == 'two-column' && (
					<TouchableOpacity
						style={[
							FreshStyles.productBoxWrapper2,
							FreshStyles.boxShadow,
							{position: 'relative'},
						]}
						onPress={() =>
							this.props.navigation.push('Product', {
								product_id: this.props.data.goods_id,
							})
						}
						activeOpacity={0.9}>
						<Image
							style={[FreshStyles.width100, FreshStyles.height40Percent]}
							source={
								this.state.imageOkay
									? ImageHelper.returnSource(this.props.data.original_img)
									: ImageHelper.returnSource('')
							}
							onError={this.imageError}
							resizeMode="cover"
						/>
						{this.pills()}
						<View style={[FreshStyles.padTop1, FreshStyles.flexRow]}>
							{this.dataText()}
							{this.fontAwesome()}
						</View>
					</TouchableOpacity>
				)}
				{this.props.type == 'three-column' && (
					<TouchableOpacity
						style={[this.props.style, {position: 'relative'}]}
						onPress={() =>
							this.props.navigation.push('Product', {
								product_id: this.props.data.goods_id,
							})
						}
						activeOpacity={0.9}>
						<Image
							style={[
								FreshStyles.width25Percent,
								FreshStyles.height25Percent,
								FreshStyles.justifyCenter,
							]}
							source={
								this.state.imageOkay
									? ImageHelper.returnSource(this.props.data.original_img)
									: ImageHelper.returnSource('')
							}
							onError={this.imageError}
							resizeMode="cover"
						/>
						{this.pills()}
						<View style={[FreshStyles.padTop1, FreshStyles.flexRow]}>
							{this.dataText()}
							{this.fontAwesome()}
						</View>
					</TouchableOpacity>
				)}
				{this.props.type == null && this.props.type == undefined && (
					<TouchableOpacity
						style={[this.props.style, {position: 'relative'}]}
						onPress={() =>
							this.props.navigation.push('Product', {
								product_id: this.props.data.goods_id,
							})
						}
						activeOpacity={0.9}>
						<Image
							style={[this.props.imageStyle]}
							source={
								this.state.imageOkay
									? ImageHelper.returnSource(this.props.data.original_img)
									: ImageHelper.returnSource('')
							}
							onError={this.imageError}
							resizeMode="cover"
						/>
						{this.pills()}
						<View style={[FreshStyles.padTop1, FreshStyles.flexRow]}>
							{this.dataText()}
							<TouchableOpacity style={[FreshStyles.selfEnd]}>
								<FontAwesomeIcon
									icon={faCartPlus}
									size={this.props.size}
									style={FreshStyles.navGreen}
								/>
							</TouchableOpacity>
						</View>
					</TouchableOpacity>
				)}
			</View>
		);
	}
}
