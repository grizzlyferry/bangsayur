import React, {Component} from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';

export default class FormInput extends Component {
	static defaultProps = {
		disabled: false,
		placeholder: '',
	};
	constructor(props) {
		super(props);
		this.state = {
			value: null
		};
	}
	render() {
		return (
			<View>
				{this.props.title != undefined &&
					this.props.title != '' &&
					this.props.title != null && (
						<Text style={Formstyles.textTitle}>{this.props.title}</Text>
					)}
				<TextInput
					style={[
						this.props.error != undefined &&
						this.props.error != '' &&
						this.props.error != null
							? Formstyles.formInputError
							: Formstyles.formInput,
						this.props.style,
					]}
					placeholder={this.props.placeholder}
					placeholderTextColor={
						this.props.error != undefined &&
						this.props.error != '' &&
						this.props.error != null
							? 'red'
							: 'grey'
					}
					onChangeText={this.props.onChangeText}
					value={this.state.value}
					maxLength={this.props.maxLength}
					keyboardType={this.props.keyboardType}
					secureTextEntry={this.props.secureTextEntry}
				/>
				<Text style={[Formstyles.errorText]}>{this.props.error}</Text>
			</View>
		);
	}
}
const Formstyles = StyleSheet.create({
	formInput: {
		paddingVertical: 0,
		paddingTop: 10,
		borderBottomColor: '#000000',
		borderBottomWidth: 0.5,
		width: '100%',
	},
	formInputError: {
		paddingVertical: 0,
		paddingTop: 10,
		borderBottomColor: '#FF0000',
		borderBottomWidth: 1.5,
		width: '100%',
	},
	textTitle: {
		fontSize: 14,
		fontWeight: 'bold',
	},
	errorText: {
		color: 'red',
		fontSize: 11,
		alignSelf: 'stretch',
		paddingTop: 3,
		paddingVertical: 0,
		paddingLeft: 5,
		marginVertical: 0,
		padding: 0,
		margin: 0,
	},
});
