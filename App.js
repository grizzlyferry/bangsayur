/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
// import {createAppContainer} from '@react-navigation/native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from './screen/HomeScreen';
import ProfileScreen from './screen/ProfileScreen';
import CheckoutScreen from './screen/CheckoutScreen';
import NewAddressScreen from './screen/NewAddressScreen';
import AddressListScreen from './screen/AddressListScreen';
import RegisterScreen from './screen/RegisterScreen';
import ErrorScreen from './screen/ErrorScreen';
import ValidateOTPScreen from './screen/ValidateOTPScreen';
import LoginScreen from './screen/LoginScreen';
import CategoryScreen from './screen/CategoryScreen';
import CategoryListScreen from './screen/CategoryListScreen';
import CartScreen from './screen/CartScreen';
import ForgetScreen from './screen/ForgetScreen';
import ProductScreen from './screen/ProductScreen';
import ProductListScreen from './screen/ProductListScreen';
import ResetPasswordScreen from './screen/ResetPasswordScreen';
import TestScreen from './screen/TestScreen';


const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login" screenOptions={{headerShown: false}}>        
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Profile" component={ProfileScreen} />
        <Stack.Screen name="Checkout" component={CheckoutScreen} />
        <Stack.Screen name="NewAddress" component={NewAddressScreen} />
        <Stack.Screen name="AddressList" component={AddressListScreen} />
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="Register" component={RegisterScreen} />
        <Stack.Screen name="Error" component={ErrorScreen} />
        <Stack.Screen name="ValidateOTP" component={ValidateOTPScreen} />
        <Stack.Screen name="Category" component={CategoryScreen} />
        <Stack.Screen name="CategoryList" component={CategoryListScreen} />
        <Stack.Screen name="Cart" component={CartScreen} />
        <Stack.Screen name="Forget" component={ForgetScreen} />
        <Stack.Screen name="Product" component={ProductScreen} />
        <Stack.Screen name="ProductList" component={ProductListScreen} />
        <Stack.Screen name="ResetPassword" component={ResetPasswordScreen} />
        <Stack.Screen name="Test" component={TestScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;


// const MainNavigator = createStackNavigator(
//   {
//     Home: {screen: HomeScreen},
//     Profile: {screen: ProfileScreen},
//     Checkout: {screen: CheckoutScreen},
//     NewAddress: {screen: NewAddressScreen},
//     AddressList: {screen: AddressListScreen},
//     Login: {screen: LoginScreen},
//     Register: {screen: RegisterScreen},
//     Error: {screen: ErrorScreen},
//     ValidateOTP: {screen: ValidateOTPScreen},
//     Category: {screen: CategoryScreen},
//     CategoryList: {screen: CategoryListScreen},
//     Cart: {screen: CartScreen},
//     Forget: {screen: ForgetScreen},
//     Product: {screen: ProductScreen},
//     ProductList: {screen: ProductListScreen},
//     ResetPassword: {screen: ResetPasswordScreen},
//     Test: {screen: TestScreen},
//   },
//   {
//     initialRouteName: 'Home',
//     defaultNavigationOptions: {headerShown: false},
//   },
// );

// const App = createAppContainer(MainNavigator);