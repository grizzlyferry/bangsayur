import {
	SafeAreaView,
	StyleSheet,
	ScrollView,
	View,
	Text,
	StatusBar,
	Dimensions,
} from 'react-native';

export const screenWidth1 = Math.floor(Dimensions.get('window').width);
export const screenHeight1 = Math.floor(Dimensions.get('window').height);

let ScreenHeight = Dimensions.get('window').height;
let temp = Math.floor((screenWidth1 * 0.3) / 50) * 50;
export const screen30PercDown50Min50 = temp < 50 ? 50 : temp;

export const textColor1 = '#303030';
export const textColor2 = '#6E6E6E';
export const textColor3 = '#ACACAC';
export const textColor4 = '#C0C0C0';
export const textColor5 = '#45AEFF';
export const textColor6 = '#FE8203';
export const textColor7 = '#828282';
export const textColor8 = 'white';
export const backgroundColor1 = '#fff';
export const backgroundColor2 = '#FE8203';
export const backgroundColor3 = '#FEF1E4';
export const backgroundColor4 = '#ECECEF';
export const statusBarHeight = StatusBar.currentHeight;

export default Styles = StyleSheet.create({
	// COLORS
	bgColor1: {
		backgroundColor: backgroundColor1,
	},
	bgColor2: {
		backgroundColor: backgroundColor2,
	},
	bgColor3: {
		backgroundColor: backgroundColor3,
	},
	bgColor4: {
		backgroundColor: backgroundColor4,
	},
	fgColor1: {
		color: textColor1,
	},
	fgColor2: {
		color: textColor2,
	},
	fgColor3: {
		color: textColor3,
	},
	fgColor4: {
		color: textColor4,
	},
	fgColor5: {
		color: textColor5,
	},
	fgColor8: {
		color: textColor8,
	},
	fgColorFormTitle: {
		color: 'rgba(0, 0, 0, 0.4)',
	},
	color1: {
		color: '#a0a0a0',
	},
	// SCREEN WIDTH AND HEIGHT
	screenTab: {
		width: screenWidth1 - 20,
	},
	maxScreenHeight: {
		maxHeight: screenHeight1 - StatusBar.currentHeight,
	},
	screenHeightMinBar: {
		maxHeight: screenHeight1 - (30 + StatusBar.currentHeight), // 30 for back bar height
	},
	popupWidth: {
		width: screenWidth1 - 20 - 20,
	},
	maxPopupHeight: {
		maxHeight: screenHeight1 - (StatusBar.currentHeight + 100),
	},
	screen: {
		height: screenHeight1,
		width: screenWidth1,
	},
	width15Percent: {
		width: Math.floor((screenWidth1 - 15) * 0.18),
	},
	height15Percent: {
		height: Math.floor((screenWidth1 - 15) * 0.18),
	},
	width20Percent: {
		width: Math.floor((screenWidth1 - 20) * 0.2),
	},
	height20Percent: {
		height: Math.floor((screenWidth1 - 20) * 0.2),
	},
	width225Percent: {
		width: Math.floor((screenWidth1 - 20) * 0.225),
	},
	width25Percent: {
		width: Math.floor((screenWidth1 - 20) * 0.25),
	},
	height25Percent: {
		height: Math.floor((screenWidth1 - 20) * 0.25),
	},
	width30Percent: {
		width: Math.floor((screenWidth1 - 20) * 0.3),
	},
	height30Percent: {
		height: Math.floor((screenWidth1 - 20) * 0.3),
	},
	height40Percent: {
		height: Math.floor((screenWidth1 - 20) * 0.4),
	},
	height30PercentPad5: {
		height: Math.floor((screenWidth1 - 20) * 0.3) + 10,
		padding: 5,
	},
	width35Percent: {
		width: Math.floor((screenWidth1 - 20) * 0.35) - 10,
	},
	width40Percent: {
		width: Math.floor((screenWidth1 - 20) * 0.4),
	},
	width50Percent: {
		width: Math.floor((screenWidth1 - 20) * 0.475),
	},
	height50Percent: {
		height: Math.floor((screenWidth1 - 20) * 0.5),
	},
	height60Percent: {
		height: Math.floor((screenWidth1 - 20) * 0.6),
	},
	height100Percent: {
		height: Math.floor(ScreenHeight) * 0.51,
	},
	width30PercentDown50: {
		width: screen30PercDown50Min50,
	},
	height30PercentDown50: {
		height: screen30PercDown50Min50,
	},
	// ICON SIZE
	iconSize10: {
		height: 10,
		width: 10,
	},
	iconSize20: {
		height: 20,
		width: 20,
	},
	iconSize30: {
		height: 30,
		width: 30,
	},
	iconSize40: {
		height: 40,
		width: 40,
	},
	iconSize50: {
		height: 50,
		width: 50,
	},
	iconSize60: {
		height: 60,
		width: 60,
	},
	iconSize70: {
		height: 70,
		width: 70,
	},
	iconSize80: {
		height: 80,
		width: 80,
	},
	iconSize90: {
		height: 90,
		width: 90,
	},
	iconSize100: {
		height: 100,
		width: 100,
	},
	hugeSize: {
		height: 200,
		width: 200,
	},
	// TEXT
	text10: {
		fontSize: 10,
	},
	text11: {
		fontSize: 11,
	},
	text12: {
		fontSize: 12,
	},
	text13: {
		fontSize: 13,
	},
	text14: {
		fontSize: 14,
	},
	text15: {
		fontSize: 15,
	},
	text16: {
		fontSize: 16,
	},
	text17: {
		fontSize: 17,
	},
	text18: {
		fontSize: 18,
	},
	text19: {
		fontSize: 19,
	},
	titleText: {
		fontSize: 15,
		fontWeight: 'bold',
	},
	titleText2: {
		fontSize: 15,
		fontWeight: 'bold',
		paddingHorizontal: 5,
		paddingBottom: 3,
	},
	// PADDING
	pad0: {
		padding: 0,
		paddingTop: 0,
		paddingBottom: 0,
		paddingVertical: 0,
		paddingHorizontal: 0,
		paddingStart: 0,
		paddingEnd: 0,
	},
	pad1: {
		padding: 5,
	},
	pad1half: {
		padding: 7.5,
	},
	pad2: {
		padding: 10,
	},
	pad2half: {
		padding: 12.5,
	},
	pad3: {
		padding: 15,
	},
	pad3half: {
		padding: 17.5,
	},
	pad4: {
		padding: 20,
	},
	pad4half: {
		padding: 22.5,
	},
	pad5: {
		padding: 25,
	},
	padTop0: {
		paddingTop: 0,
	},
	padTop1: {
		paddingTop: 5,
	},
	padTop2: {
		paddingTop: 10,
	},
	padTop3: {
		paddingTop: 15,
	},
	padTop4: {
		paddingTop: 20,
	},
	padBot0: {
		paddingBottom: 0,
	},
	padBot1: {
		paddingBottom: 5,
	},
	padBot2: {
		paddingBottom: 10,
	},
	padBot3: {
		paddingBottom: 15,
	},
	padBot4: {
		paddingBottom: 20,
	},
	padBot5: {
		paddingBottom: 25,
	},
	padBot6: {
		paddingBottom: 30,
	},
	padRight0: {
		paddingRight: 0,
	},
	padRight1: {
		paddingRight: 5,
	},
	padRight2: {
		paddingRight: 10,
	},
	padRight3: {
		paddingRight: 15,
	},
	padRight4: {
		paddingRight: 20,
	},
	padRight5: {
		paddingRight: 25,
	},
	padRight6: {
		paddingRight: 30,
	},
	padRight7: {
		paddingRight: 35,
	},
	padRight8: {
		paddingRight: 40,
	},
	padLeft0: {
		paddingLeft: 0,
	},
	padLeft1: {
		paddingLeft: 5,
	},
	padLeft2: {
		paddingLeft: 10,
	},
	padLeft3: {
		paddingLeft: 15,
	},
	padLeft4: {
		paddingLeft: 20,
	},
	padLeft5: {
		paddingLeft: 25,
	},
	padLeft6: {
		paddingLeft: 30,
	},
	padLeft7: {
		paddingLeft: 35,
	},
	padLeft8: {
		paddingLeft: 40,
	},
	padVer1: {
		paddingVertical: 5,
	},
	padVer2: {
		paddingVertical: 10,
	},
	padVer3: {
		paddingVertical: 15,
	},
	padVer4: {
		paddingVertical: 20,
	},
	padHor1: {
		paddingHorizontal: 5,
	},
	padHor2: {
		paddingHorizontal: 10,
	},
	padHor3: {
		paddingHorizontal: 15,
	},
	padHor4: {
		paddingHorizontal: 20,
	},
	padHor5: {
		paddingHorizontal: 25,
	},
	// MARGIN
	margin0: {
		margin: 0,
		marginHorizontal: 0,
		marginVertical: 0,
		marginTop: 0,
		marginBottom: 0,
		marginLeft: 0,
		marginRight: 0,
	},
	margin5: {
		margin: 5,
	},
	margin10: {
		margin: 10,
	},
	margin15: {
		margin: 15,
	},
	margin20: {
		margin: 20,
	},
	marginTop0: {
		marginTop: 0,
	},
	marginTop1: {
		marginTop: 5,
	},
	marginTop2: {
		marginTop: 10,
	},
	marginTop3: {
		marginTop: 15,
	},
	marginTop4: {
		marginTop: 20,
	},
	marginTop5: {
		marginTop: 25,
	},
	marginTop6: {
		marginTop: 30,
	},
	marginTop7: {
		marginTop: 35,
	},
	marginTop8: {
		marginTop: 40,
	},
	marginBot0: {
		marginBottom: 0,
	},
	marginBot1: {
		marginBottom: 5,
	},
	marginBot2: {
		marginBottom: 10,
	},
	marginBot3: {
		marginBottom: 15,
	},
	marginBot4: {
		marginBottom: 20,
	},
	marginBot5: {
		marginBottom: 25,
	},
	marginBot6: {
		marginBottom: 30,
	},
	marginBot7: {
		marginBottom: 35,
	},
	marginBot8: {
		marginBottom: 40,
	},
	marginLeft0: {
		marginLeft: 0,
	},
	marginLeft1: {
		marginLeft: 5,
	},
	marginLeft2: {
		marginLeft: 10,
	},
	marginLeft3: {
		marginLeft: 15,
	},
	marginLeft4: {
		marginLeft: 20,
	},
	marginRight0: {
		marginRight: 0,
	},
	marginRight1: {
		marginRight: 5,
	},
	marginRight2: {
		marginRight: 10,
	},
	marginRight3: {
		marginRight: 15,
	},
	marginRight4: {
		marginRight: 20,
	},
	marginRight5: {
		marginRight: 25,
	},
	marginRight6: {
		marginRight: 30,
	},
	marginVer1: {
		marginVertical: 2,
	},
	marginVer2: {
		marginVertical: 4,
	},
	marginVer3: {
		marginVertical: 6,
	},
	marginVer4: {
		marginVertical: 8,
	},
	marginVer5: {
		marginVertical: 10,
	},
	marginVer6: {
		marginVertical: 12,
	},
	marginVer7: {
		marginVertical: 14,
	},
	marginVer8: {
		marginVertical: 16,
	},
	marginVer9: {
		marginVertical: 18,
	},
	marginVer10: {
		marginVertical: 20,
	},
	marginVer11: {
		marginVertical: 22,
	},
	marginVer12: {
		marginVertical: 24,
	},
	marginVer13: {
		marginVertical: 26,
	},
	marginVer14: {
		marginVertical: 28,
	},
	marginVer15: {
		marginVertical: 30,
	},
	marginVer16: {
		marginVertical: 32,
	},
	marginVer17: {
		marginVertical: 34,
	},
	marginVer18: {
		marginVertical: 36,
	},
	marginVer19: {
		marginVertical: 38,
	},
	marginVer20: {
		marginVertical: 40,
	},
	marginHor1: {
		marginHorizontal: 2,
	},
	marginHor2: {
		marginHorizontal: 4,
	},
	marginHor3: {
		marginHorizontal: 6,
	},
	marginHor4: {
		marginHorizontal: 8,
	},
	marginHor5: {
		marginHorizontal: 10,
	},
	marginHor6: {
		marginHorizontal: 12,
	},
	marginHor7: {
		marginHorizontal: 14,
	},
	marginHor8: {
		marginHorizontal: 16,
	},
	marginHor9: {
		marginHorizontal: 18,
	},
	marginHor10: {
		marginHorizontal: 20,
	},
	marginHor11: {
		marginHorizontal: 22,
	},
	marginHor12: {
		marginHorizontal: 24,
	},
	marginHor13: {
		marginHorizontal: 26,
	},
	marginHor14: {
		marginHorizontal: 28,
	},
	marginHor15: {
		marginHorizontal: 30,
	},
	marginHor16: {
		marginHorizontal: 32,
	},
	marginHor17: {
		marginHorizontal: 34,
	},
	marginHor18: {
		marginHorizontal: 36,
	},
	marginHor19: {
		marginHorizontal: 38,
	},
	marginHor20: {
		marginHorizontal: 40,
	},
	// CONTAINER
	container1: {
		height: 150,
		flexDirection: 'row',
	},
	container1half: {
		height: 175,
		flexDirection: 'row',
	},
	container2: {
		height: 200,
		flexDirection: 'row',
	},
	container2half: {
		height: 225,
		flexDirection: 'row',
	},
	container3: {
		height: 250,
		flexDirection: 'row',
	},
	container3half: {
		height: 275,
		flexDirection: 'row',
	},
	container4: {
		height: 300,
		flexDirection: 'row',
	},
	container4half: {
		height: 325,
		flexDirection: 'row',
	},
	container5: {
		height: 350,
		flexDirection: 'row',
	},
	container6: {
		height: 400,
		flexDirection: 'row',
	},
	textLeft: {
		textAlign: 'left',
	},
	textCenter: {
		textAlign: 'center',
	},
	textRight: {
		textAlign: 'right',
	},
	textUnderline: {
		textDecorationLine: 'underline',
	},
	textBold: {
		fontWeight: 'bold',
	},
	textHalfOpacity: {
		opacity: 0.5,
	},
	errorText: {
		color: 'red',
		fontSize: 11,
		alignSelf: 'stretch',
		paddingTop: 3,
		paddingVertical: 0,
		paddingLeft: 5,
		marginVertical: 0,
		padding: 0,
		margin: 0,
	},
	// FLEXES
	flexRow: {
		flexDirection: 'row',
	},
	flexRowWrap: {
		flexDirection: 'row',
		flexWrap: 'wrap',
	},
	flexColumn: {
		flexDirection: 'column',
	},
	flexColumnWrap: {
		flexDirection: 'column',
		flexWrap: 'wrap',
	},
	flex1: {
		flex: 1,
	},
	flex2: {
		flex: 2,
	},
	flex3: {
		flex: 3,
	},
	flexWrap: {
		flexWrap: 'wrap',
	},
	flex1Row: {
		flex: 1,
		flexDirection: 'row',
	},
	flex1Column: {
		flex: 1,
		flexDirection: 'column',
	},
	flexGrow: {
		flexGrow: 1,
	},
	// JUSTIFY CONTENT
	justifyStart: {
		justifyContent: 'flex-start',
	},
	justifyEnd: {
		justifyContent: 'flex-end',
	},
	justifyBetween: {
		justifyContent: 'space-between',
	},
	justifyCenter: {
		justifyContent: 'center',
	},
	// SELF ALIGNS
	selfStretch: {
		alignSelf: 'stretch',
	},
	selfCenter: {
		alignSelf: 'center',
	},
	selfStart: {
		alignSelf: 'flex-start',
	},
	selfEnd: {
		alignSelf: 'flex-end',
	},
	// ALIGN ITEMS
	alignEnd: {
		alignItems: 'flex-end',
	},
	alignStart: {
		alignItems: 'flex-start',
	},
	alignCenter: {
		alignItems: 'center',
	},
	// ROUNDED CORNERS
	cornerRad0: {
		borderRadius: 0,
	},
	cornerRad5: {
		borderRadius: 5,
	},
	cornerRad10: {
		borderRadius: 10,
	},
	cornerRad15: {
		borderRadius: 15,
	},
	cornerRad20: {
		borderRadius: 20,
	},
	cornerTopRad5: {
		borderTopLeftRadius: 5,
		borderTopRightRadius: 5,
	},
	cornerBottomRad5: {
		borderBottomLeftRadius: 5,
		borderBottomRightRadius: 5,
	},
	// COLOURS
	white: {
		backgroundColor: 'white',
	},
	red: {
		backgroundColor: 'red',
	},
	black: {
		backgroundColor: 'black',
	},
	blue: {
		backgroundColor: 'blue',
	},
	// justify content
	justifyChildStart: {
		justifyContent: 'flex-start',
	},
	contentSpaceBetween: {
		justifyContent: 'space-between',
	},
	justifyChildCenter: {
		justifyContent: 'center',
	},
	// WIDTH
	width15: {
		width: '15%',
	},
	width20: {
		width: '20%',
	},
	width30: {
		width: '30%',
	},
	width40: {
		width: '40%',
	},
	width50: {
		width: '50%',
	},
	width60: {
		width: '60%',
	},
	width70: {
		width: '70%',
	},
	width80: {
		width: '80%',
	},
	width90: {
		width: '90%',
	},
	width100: {
		width: '100%',
	},
	end: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'flex-end',
	},
	center: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
	},
	start: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'flex-start',
	},
	// COMPONENTS STYLES
	bottomMenu: {
		backgroundColor: '#fff',
		width: '100%',
		height: 65,
		flexDirection: 'row',
		justifyContent: 'center',
		shadowColor: '#000',
		shadowOffset: {width: 0, height: 1},
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: 5,
	},
	subBottomMenu: {
		flex: 1,
		flexDirection: 'column',
		alignItems: 'center',
		paddingVertical: 8,
	},
	searchBar: {
		height: 40,
		backgroundColor: '#fff',
		padding: 5,
		flexDirection: 'row',
		flex: 1,
		alignItems: 'center',
		borderRadius: 60,
		borderWidth: 0.5,
		borderColor: '#dedede',
	},
	topBar: {
		backgroundColor: '#46CF54',
		padding: 10,
		height: 50,
		alignItems: 'center',
		width: '100%',
		flexDirection: 'row',
		alignSelf: 'stretch',
	},
	searchBar2: {
		height: 40,
		backgroundColor: 'rgba(200,200,200,0.5)',
		padding: 5,
		flexDirection: 'row',
		flex: 1,
		alignItems: 'center',
		borderRadius: 5,
	},
	topBar2: {
		backgroundColor: 'rgba(255, 255, 255, 1)',
		padding: 10,
		alignItems: 'center',
		width: '100%',
		flexDirection: 'row',
		alignSelf: 'stretch',
	},
	Background: {
		minHeight: ScreenHeight,
		backgroundColor: '#F5F6F8',
		flex: 1,
		flexDirection: 'column',
	},
	centerRow: {
		flexDirection: 'row',
		justifyContent: 'center',
	},
	centerColumn: {
		flexDirection: 'column',
		justifyContent: 'center',
	},
	allCenter: {
		justifyContent: 'center',
		alignItems: 'center',
		alignSelf: 'center',
	},
	formControl: {
		paddingVertical: 0,
		paddingTop: 10,
		borderBottomColor: '#000000',
		borderBottomWidth: 0.5,
		width: '100%',
	},
	formControlError: {
		paddingVertical: 0,
		paddingTop: 10,
		borderBottomColor: '#FF0000',
		borderBottomWidth: 1.5,
		width: '100%',
	},
	border1: {
		borderWidth: 1,
		borderColor: 'black',
	},
	topBarText: {
		fontSize: 15,
		marginLeft: 15,
		color: 'white',
	},
	nav0: {
		height: 50,
		width: 50,
		color: 'rgba(200,200,200,1)',
	},
	navLiked: {
		height: 50,
		width: 50,
		color: '#FE5C6C',
	},
	navGreen: {
		height: 50,
		width: 50,
		margin: 1,
		color: '#46CF54',
	},
	navOff: {
		height: 50,
		width: 50,
		color: 'rgba(0,0,0,0.5)',
	},
	navOn: {
		height: 50,
		width: 50,
		color: '#46CF54',
	},
	navTextGreen: {
		color: '#46CF54',
		fontSize: 15,
	},
	navTextWhite: {
		color: 'rgba(255, 255, 255, 1)',
		fontSize: 15,
	},
	navTextOff: {
		color: 'rgba(0,0,0,0.5)',
		fontSize: 15,
	},
	navTextOn: {
		color: 'rgba(0, 200, 0, 1)',
		fontSize: 15,
		fontWeight: 'bold',
	},
	detail: {
		paddingTop: 5,
		color: 'rgba(0,200,0,1)',
		fontSize: 13,
	},
	description: {
		paddingVertical: 5,
		fontSize: 12,
	},
	productListWrapper: {
		backgroundColor: 'white',
		flexDirection: 'row',
		flexWrap: 'wrap',
		padding: 10,
		margin: 15,
	},
	productList2Wrapper: {
		backgroundColor: 'white',
		padding: 10,
	},
	productBoxWrapper: {
		width: Math.floor(screenWidth1 * 0.25),
		alignItems: 'center',
	},
	productBoxWrapper2: {
		width: Math.floor(screenWidth1 * 0.5 - 10),
	},
	productBoxWrapper3: {
		width: Math.floor(screenWidth1 * 0.33 - 15),
	},
	categoryBoxWrapper: {
		width: Math.floor(screenWidth1 * 0.33 - 5),
		alignItems: 'center',
	},
	categoryBoxWrapper2: {
		width: Math.floor(screenWidth1 * 0.33 - 15),
		alignItems: 'center',
	},
	productInfoWrapper: {
		backgroundColor: 'white',
		flexDirection: 'column',
		flexWrap: 'wrap',
		padding: 15,
		width: '100%',
	},
	CatWrapper: {
		backgroundColor: 'white',
		flexDirection: 'row',
		flexWrap: 'wrap',
		padding: 10,
		alignItems: 'center',
	},
	menuListWrapper: {
		backgroundColor: 'white',
		flexDirection: 'row',
		flexWrap: 'wrap',
	},
	menuWrapper: {
		width: '100%',
		padding: 10,
		maxWidth: '20%',
	},
	buttonLike: {
		borderWidth: 1,
		borderColor: '#dedede',
		borderRadius: 5,
		padding: 10,
		alignSelf: 'flex-end',
	},
	buttonOutline: {
		borderWidth: 1,
		borderColor: '#dedede',
		borderRadius: 5,
	},
	greenButtonOutline: {
		borderWidth: 1,
		borderColor: 'rgba(0,200,0,1)',
		borderRadius: 5,
	},
	greenButton: {
		backgroundColor: 'rgba(0,200,0,1)',
		borderRadius: 5,
	},
	cartContainer: {
		padding: 40,
		paddingHorizontal: 60,
		backgroundColor: 'white',
		alignItems: 'center',
	},
	boxShadow: {
		backgroundColor: '#fff',
		justifyContent: 'center',
		shadowColor: '#000',
		shadowOffset: {width: 2, height: 0.5},
		shadowOpacity: 0.2,
		shadowRadius: 10,
		elevation: 5,
		margin: 5,
		padding: 10,
		paddingBottom: 15,
	},
	boxNoShadow: {
		backgroundColor: '#fff',
		justifyContent: 'center',
		margin: 5,
	},
	cartText: {
		flex: 1,
		flexDirection: 'column',
		paddingBottom: 5,
		paddingLeft: 20,
	},
	variation: {
		flexDirection: 'column',
		paddingBottom: 10,
		paddingHorizontal: 15,
	},
	subCategoryOff: {
		flex: 1,
		alignItems: 'center',
		paddingBottom: 5,
	},
	subCategoryOn: {
		flex: 1,
		borderBottomWidth: 2,
		borderBottomColor: 'rgba(0,200,0,1)',
		alignItems: 'center',
		paddingBottom: 5,
	},
	shadows: {
		backgroundColor: 'white',
		shadowColor: '#000',
		shadowOffset: {width: 0, height: 1},
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: 10,
	},
	pillsContainer: {
		flexDirection: 'row',
		position: 'absolute',
		top: 10,
		right: 10,
	},
	pillsHome: {
		backgroundColor: 'rgba(130,130,255,1)',
		borderRadius: 50,
		paddingHorizontal: 10,
		paddingVertical: 4,
		fontSize: 10,
		alignSelf: 'flex-end',
		marginHorizontal: 2,
	},
	pillsPick: {
		backgroundColor: 'rgba(150,255,100,1)',
		borderRadius: 50,
		paddingHorizontal: 10,
		paddingVertical: 4,
		fontSize: 10,
		alignSelf: 'flex-end',
		marginHorizontal: 2,
	},
	pillsRed: {
		backgroundColor: 'rgba(255,150,100,1)',
		borderRadius: 50,
		paddingHorizontal: 5,
		paddingVertical: 2,
		fontSize: 10,
		alignSelf: 'flex-end',
		marginHorizontal: 2,
	},
	pillsBlue: {
		backgroundColor: 'rgba(173,216,255,1)',
		borderRadius: 50,
		paddingHorizontal: 5,
		paddingVertical: 2,
		fontSize: 10,
		alignSelf: 'flex-end',
		marginHorizontal: 2,
	},
	pillsYellow: {
		backgroundColor: 'rgba(255,255,51,1)',
		borderRadius: 50,
		paddingHorizontal: 5,
		paddingVertical: 2,
		fontSize: 10,
		alignSelf: 'flex-end',
		marginHorizontal: 2,
	},
	cartBottom: {
		borderWidth: 1,
		borderColor: '#dedede',
		flexDirection: 'row',
		backgroundColor: 'white',
		padding: 10,
		justifyContent: 'space-between',
		alignItems: 'center',
	},
	address: {
		backgroundColor: 'white',
		marginTop: 15,
		paddingVertical: 20,
		paddingHorizontal: 20,
		shadowColor: '#000',
		shadowOffset: {width: 0, height: 1},
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: 5,
	},
	// Loading group's styling
	layoutLoading: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignSelf: 'stretch',
		minHeight: 150,
	},
	LBWidth50Perc: {
		width: Math.floor((screenWidth1 - 20) * 0.5),
	},
	LBWidth50PercInside: {
		width: Math.floor((screenWidth1 - 20) * 0.5) - 20,
	},
	LBHeight50PercInside: {
		height: Math.floor((screenWidth1 - 20) * 0.5) - 20,
	},
	LBWidth35Perc: {
		width: Math.floor(screenWidth1 * 0.35),
	},
	LBWidth35PercInside: {
		width: Math.floor(screenWidth1 * 0.35) - 20,
	},
	LBHeight35PercInside: {
		height: Math.floor(screenWidth1 * 0.35) - 20,
	},
	LBWidth3Col: {
		width: Math.floor((screenWidth1 - 20)/3),
	},
	LBWidth3ColInside: {
		width: Math.floor((screenWidth1 - 20)/3) - 20,
	},
	LBHeight3ColInside: {
		height: Math.floor((screenWidth1 - 20)/3) - 20,
	},
	LBWidth4Col: {
		width: Math.floor(screenWidth1 * 0.25),
	},
	LBWidth4ColInside: {
		width: Math.floor(screenWidth1 * 0.25) - 20,
	},
	LBHeight4ColInside: {
		height: Math.floor(screenWidth1 * 0.25) - 20,
	},
	/*
	LBwidth30Perc: {
		width: Math.floor(screenWidth1 * 0.3),
	},
	LBheight30Perc: {
		height: Math.floor(screenWidth1 * 0.3),
	},*/
	LBTextHeight: {
		height: 20,
	},
	LBBackground: {
		backgroundColor: '#C5C5C5',
	},
	LBMargin: {
		marginBottom: 5,
	},
	LBTextW3: {
		width: '75%',
	},
	LBTextW1: {
		width: '20%',
	},
});

export const screenWidth = screenWidth1;
export const screenHeight = screenHeight1;
